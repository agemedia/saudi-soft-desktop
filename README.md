# BoomPOS
A Modular Point of Sale C# Application With Modern UI Styling.

## How to get running
1. Install Visual Studio
2. In the installation select the windows development package.
3. Clone the repo on your local machine.
4. Open Visual studio
5. Click "Open a project or solution" button
6. Navigate to where you have the local copy of boom, and select BoomPOS.sln file
7. Now you are ready to edit.
8. In order to run press the button that looks like a play button.
9. that will run a debug supported version of BoomPOS.

Keep in mind it does check if BoomPOS was installed, so do install any recent verison of BoomPOS to your pc,
cause the software automatically checks if BoomPOS exists in your registry.

Alernatively, You could also override that by going to App.xaml.cs in BoomPOS.WinApp
and replace this function.

```
protected override void OnStartup(StartupEventArgs e)
{
    IServiceProvider provider = CreateServiceProvider();
    IValidateLicenceService licenceService = provider.GetRequiredService<IValidateLicenceService>();
    bool isSetupValid = licenceService.ValidateRegistry();
    if (!isSetupValid)
    {
        MessageBox.Show("This application was not installed correctly via the installer.", "BoomPOS | Invalid Installation", MessageBoxButton.OK, MessageBoxImage.Error);
        this.Shutdown();
    }
    else
    {
        MainWindow window = provider.GetRequiredService<MainWindow>();
        window.DataContext = provider.GetRequiredService<WindowViewModel>();
        window.Show();
        base.OnStartup(e);
    }
}
```

With
```
protected override void OnStartup(StartupEventArgs e)
{
    IServiceProvider provider = CreateServiceProvider();
    IValidateLicenceService licenceService = provider.GetRequiredService<IValidateLicenceService>();
    bool isSetupValid = True;
    if (!isSetupValid)
    {
        MessageBox.Show("This application was not installed correctly via the installer.", "BoomPOS | Invalid Installation", MessageBoxButton.OK, MessageBoxImage.Error);
        this.Shutdown();
    }
    else
    {
        MainWindow window = provider.GetRequiredService<MainWindow>();
        window.DataContext = provider.GetRequiredService<WindowViewModel>();
        window.Show();
        base.OnStartup(e);
    }
}
```

If you do it that way, please think about going to the WindowViewModel.cs in BoomPOS
and change what is in CheckForFirstTime() to navigate you the dashboard view.
This way you bypass the signup process. [Or You can have a working BoomPOS database file in AppData/Roaming/EngTechno/BoomPOS]
that way you can login normally.

Installing one of our recent releases on your machine is definitly the easier way here.
cause you would have to undo these changes when you want to release the software.