﻿namespace SaudiSoft.Core
{
    public interface IPrintItemReportService
    {
        void Print(StockItem selectedItem, ItemReport reportData);
    }
}