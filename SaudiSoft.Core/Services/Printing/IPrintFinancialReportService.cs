﻿namespace SaudiSoft.Core
{
    public interface IPrintFinancialReportService
    {
        void Print(FinancialReport reportData);
    }
}