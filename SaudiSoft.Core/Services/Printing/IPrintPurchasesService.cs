﻿using System.Collections.Generic;

namespace SaudiSoft.Core
{
    public interface IPrintPurchasesService
    {
        bool Print(PurchaseTicket receiptTicket, ICollection<PurchaseItem> receiptItems, string paymentName, string cashierName, string currencyValue);
    }
}