﻿using System.Collections.Generic;

namespace SaudiSoft.Core
{
    public interface IPrintSalesService
    {
        bool Print(SalesTicket receiptTicket, ICollection<SalesItem> receiptItems, string paymentName, string cashierName, string currencyValue);
    }
}