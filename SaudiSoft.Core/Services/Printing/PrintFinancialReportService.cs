﻿using System.Drawing;
using System.Drawing.Printing;

namespace SaudiSoft.Core
{
    public class PrintFinancialReportService : IPrintFinancialReportService
    {
        #region Private Members       
        private AppSettings _currentSettings;
        private FinancialReport _reportData;
        #endregion

        #region Public Class Methods
        /// <summary>
        /// Converts Data into Graphics and Prints the Result.
        /// </summary>
        public void Print(FinancialReport reportData)
        {
            _currentSettings = AppSettings.Load();
            _reportData = reportData;

            PrintDocument printDoc = new PrintDocument();
            if (printDoc.PrinterSettings.IsValid && _currentSettings.LanguageIndex == 1)
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPageArabic);
                printDoc.Print();
            }
            else if (printDoc.PrinterSettings.IsValid)
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPageEnglish);
                printDoc.Print();
            }
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Converts Data into An Arabic Invoice
        /// </summary>
        private void PrintPageArabic(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;

            Font titleFont = new Font(FontFamily.GenericSansSerif, 14.0f, FontStyle.Bold);
            Font boldFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Bold);
            Font regularFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Regular);

            var centerTopFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Near
            };
            var centerBottomFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Far
            };
            var rightFormat = new StringFormat
            {
                Alignment = StringAlignment.Far,
                LineAlignment = StringAlignment.Center
            };

            // Print Header
            graphics.DrawString(_currentSettings.CompanyName, titleFont, Brushes.Black, new Rectangle(10, 0, 270, 40), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyAddress, regularFont, Brushes.Black, new Rectangle(10, 40, 270, 30), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyPhone, regularFont, Brushes.Black, new Rectangle(10, 70, 270, 15), centerBottomFormat);
            graphics.DrawLine(Pens.Black, 10, 91, 280, 91);
            graphics.DrawLine(Pens.Black, 10, 93, 280, 93);

            // Print Report Content
            graphics.DrawString("تقرير مالي", boldFont, Brushes.Black, new Rectangle(10, 95, 270, 20), centerTopFormat);
            graphics.DrawString(":من", regularFont, Brushes.Black, new Rectangle(190, 115, 85, 15), rightFormat);
            graphics.DrawString(_reportData.FilterFromDate.ToString("dd/MM/yyyy"), regularFont, Brushes.Black, new Rectangle(10, 115, 180, 15), rightFormat);
            graphics.DrawString(":الي", regularFont, Brushes.Black, new Rectangle(190, 130, 85, 15), rightFormat);
            graphics.DrawString(_reportData.FilterToDate.ToString("dd/MM/yyyy"), regularFont, Brushes.Black, new Rectangle(10, 130, 180, 15), rightFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 112, 270, 35));

            graphics.DrawString("المبيعات", boldFont, Brushes.Black, new Rectangle(10, 150, 270, 20), centerTopFormat);
            graphics.DrawString(":المبلغ الاجمالي", regularFont, Brushes.Black, new Rectangle(190, 170, 85, 15), rightFormat);
            graphics.DrawString(_reportData.TotalSales.ToString("F"), regularFont, Brushes.Black, new Rectangle(10, 170, 180, 15), rightFormat);
            graphics.DrawString(":المبلغ المدوع", regularFont, Brushes.Black, new Rectangle(190, 185, 85, 15), rightFormat);
            graphics.DrawString(_reportData.TotalSalesPaid.ToString("F"), regularFont, Brushes.Black, new Rectangle(10, 185, 180, 15), rightFormat);
            graphics.DrawString(":المبلغ المستحق", regularFont, Brushes.Black, new Rectangle(190, 200, 85, 15), rightFormat);
            graphics.DrawString(_reportData.TotalSalesDue.ToString("F"), regularFont, Brushes.Black, new Rectangle(10, 200, 180, 15), rightFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 167, 270, 50));


            graphics.DrawString("المشتريات", boldFont, Brushes.Black, new Rectangle(10, 220, 270, 20), centerTopFormat);
            graphics.DrawString(":المبلغ الاجمالي", regularFont, Brushes.Black, new Rectangle(190, 240, 85, 15), rightFormat);
            graphics.DrawString(_reportData.TotalPurchases.ToString("F"), regularFont, Brushes.Black, new Rectangle(10, 240, 180, 15), rightFormat);
            graphics.DrawString(":المبلغ المدوع", regularFont, Brushes.Black, new Rectangle(190, 255, 85, 15), rightFormat);
            graphics.DrawString(_reportData.TotalPurchasesPaid.ToString("F"), regularFont, Brushes.Black, new Rectangle(10, 255, 180, 15), rightFormat);
            graphics.DrawString(":المبلغ المستحق", regularFont, Brushes.Black, new Rectangle(190, 270, 85, 15), rightFormat);
            graphics.DrawString(_reportData.TotalPurchasesDue.ToString("F"), regularFont, Brushes.Black, new Rectangle(10, 270, 180, 15), rightFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 237, 270, 50));


            graphics.DrawString("المصاريف", boldFont, Brushes.Black, new Rectangle(10, 290, 270, 20), centerTopFormat);
            graphics.DrawString(":المبلغ الاجمالي", regularFont, Brushes.Black, new Rectangle(190, 310, 85, 15), rightFormat);
            graphics.DrawString(_reportData.TotalExpenses.ToString("F"), regularFont, Brushes.Black, new Rectangle(10, 310, 180, 15), rightFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 307, 270, 20));


            graphics.DrawString("الربح/الخسارة", boldFont, Brushes.Black, new Rectangle(10, 330, 270, 20), centerTopFormat);
            graphics.DrawString(":المجموع العام", regularFont, Brushes.Black, new Rectangle(190, 350, 85, 15), rightFormat);
            graphics.DrawString(_reportData.TotalGrossProfit.ToString("F"), regularFont, Brushes.Black, new Rectangle(10, 350, 180, 15), rightFormat);
            graphics.DrawString(":المجموع الصافي", regularFont, Brushes.Black, new Rectangle(185, 365, 90, 15), rightFormat);
            graphics.DrawString(_reportData.TotalNetProfit.ToString("F"), regularFont, Brushes.Black, new Rectangle(10, 365, 180, 15), rightFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 347, 270, 35));

            // Print Footer
            graphics.DrawLine(Pens.Black, 10, 390, 280, 390);
            graphics.DrawLine(Pens.Black, 10, 392, 280, 392);
            graphics.DrawString(_currentSettings.FooterMessage, boldFont, Brushes.Black, new Rectangle(10, 395, 270, 40), centerTopFormat);

            titleFont.Dispose();
            boldFont.Dispose();
            regularFont.Dispose();
            centerTopFormat.Dispose();
            centerBottomFormat.Dispose();
            rightFormat.Dispose();

            (sender as PrintDocument).PrintPage -= PrintPageArabic;
        }

        /// <summary>
        /// Converts Data into An English Invoice
        /// </summary>
        private void PrintPageEnglish(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;

            Font titleFont = new Font(FontFamily.GenericSansSerif, 14.0f, FontStyle.Bold);
            Font boldFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Bold);
            Font regularFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Regular);

            var centerTopFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Near
            };
            var centerBottomFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Far
            };
            var leftFormat = new StringFormat
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Center
            };

            // Print Header
            graphics.DrawString(_currentSettings.CompanyName, titleFont, Brushes.Black, new Rectangle(10, 0, 270, 40), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyAddress, regularFont, Brushes.Black, new Rectangle(10, 40, 270, 30), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyPhone, regularFont, Brushes.Black, new Rectangle(10, 70, 270, 15), centerBottomFormat);
            graphics.DrawLine(Pens.Black, 10, 91, 280, 91);
            graphics.DrawLine(Pens.Black, 10, 93, 280, 93);


            // Print Report Content
            graphics.DrawString("Financial Report", boldFont, Brushes.Black, new Rectangle(10, 95, 270, 20), centerTopFormat);
            graphics.DrawString("From:", regularFont, Brushes.Black, new Rectangle(15, 115, 95, 15), leftFormat);
            graphics.DrawString(_reportData.FilterFromDate.ToString("dd/MM/yyyy"), regularFont, Brushes.Black, new Rectangle(110, 115, 160, 15), leftFormat);
            graphics.DrawString("To:", regularFont, Brushes.Black, new Rectangle(15, 130, 95, 15), leftFormat);
            graphics.DrawString(_reportData.FilterToDate.ToString("dd/MM/yyyy"), regularFont, Brushes.Black, new Rectangle(110, 130, 160, 15), leftFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 112, 270, 35));

            graphics.DrawString("Sales", boldFont, Brushes.Black, new Rectangle(10, 150, 270, 20), centerTopFormat);
            graphics.DrawString("Total Amount:", regularFont, Brushes.Black, new Rectangle(15, 170, 95, 15), leftFormat);
            graphics.DrawString(_reportData.TotalSales.ToString("F"), regularFont, Brushes.Black, new Rectangle(110, 170, 160, 15), leftFormat);
            graphics.DrawString("Total Paid:", regularFont, Brushes.Black, new Rectangle(15, 185, 95, 15), leftFormat);
            graphics.DrawString(_reportData.TotalSalesPaid.ToString("F"), regularFont, Brushes.Black, new Rectangle(110, 185, 160, 15), leftFormat);
            graphics.DrawString("Total Due:", regularFont, Brushes.Black, new Rectangle(15, 200, 95, 15), leftFormat);
            graphics.DrawString(_reportData.TotalSalesDue.ToString("F"), regularFont, Brushes.Black, new Rectangle(110, 200, 160, 15), leftFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 167, 270, 50));


            graphics.DrawString("Purchases", boldFont, Brushes.Black, new Rectangle(10, 220, 270, 20), centerTopFormat);
            graphics.DrawString("Total Amount:", regularFont, Brushes.Black, new Rectangle(15, 240, 95, 15), leftFormat);
            graphics.DrawString(_reportData.TotalPurchases.ToString("F"), regularFont, Brushes.Black, new Rectangle(110, 240, 160, 15), leftFormat);
            graphics.DrawString("Total Paid:", regularFont, Brushes.Black, new Rectangle(15, 255, 95, 15), leftFormat);
            graphics.DrawString(_reportData.TotalPurchasesPaid.ToString("F"), regularFont, Brushes.Black, new Rectangle(110, 255, 160, 15), leftFormat);
            graphics.DrawString("Total Due:", regularFont, Brushes.Black, new Rectangle(15, 270, 95, 15), leftFormat);
            graphics.DrawString(_reportData.TotalPurchasesDue.ToString("F"), regularFont, Brushes.Black, new Rectangle(110, 270, 160, 15), leftFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 237, 270, 50));


            graphics.DrawString("Expenses", boldFont, Brushes.Black, new Rectangle(10, 290, 270, 20), centerTopFormat);
            graphics.DrawString("Total Amount:", regularFont, Brushes.Black, new Rectangle(15, 310, 95, 15), leftFormat);
            graphics.DrawString(_reportData.TotalExpenses.ToString("F"), regularFont, Brushes.Black, new Rectangle(110, 310, 160, 15), leftFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 307, 270, 20));


            graphics.DrawString("Profit/Loss", boldFont, Brushes.Black, new Rectangle(10, 330, 270, 20), centerTopFormat);
            graphics.DrawString("Gross Amount:", regularFont, Brushes.Black, new Rectangle(15, 350, 100, 15), leftFormat);
            graphics.DrawString(_reportData.TotalGrossProfit.ToString("F"), regularFont, Brushes.Black, new Rectangle(110, 350, 160, 15), leftFormat);
            graphics.DrawString("Net Amount:", regularFont, Brushes.Black, new Rectangle(15, 365, 95, 15), leftFormat);
            graphics.DrawString(_reportData.TotalNetProfit.ToString("F"), regularFont, Brushes.Black, new Rectangle(110, 365, 160, 15), leftFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 347, 270, 35));

            // Print Footer
            graphics.DrawLine(Pens.Black, 10, 390, 280, 390);
            graphics.DrawLine(Pens.Black, 10, 392, 280, 392);
            graphics.DrawString(_currentSettings.FooterMessage, boldFont, Brushes.Black, new Rectangle(10, 395, 270, 40), centerTopFormat);

            titleFont.Dispose();
            boldFont.Dispose();
            regularFont.Dispose();
            centerTopFormat.Dispose();
            centerBottomFormat.Dispose();
            leftFormat.Dispose();

            (sender as PrintDocument).PrintPage -= PrintPageEnglish;
        }
        #endregion
    }
}
