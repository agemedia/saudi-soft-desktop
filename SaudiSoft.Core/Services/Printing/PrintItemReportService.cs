﻿using System.Drawing;
using System.Drawing.Printing;

namespace SaudiSoft.Core
{
    public class PrintItemReportService : IPrintItemReportService
    {
        #region Private Members       
        private AppSettings _currentSettings;
        private StockItem _selectedItem;
        private ItemReport _reportData;
        #endregion

        #region Public Class Methods
        /// <summary>
        /// Converts Data into Graphics and Prints the Result.
        /// </summary>
        public void Print(StockItem selectedItem, ItemReport reportData)
        {
            _currentSettings = AppSettings.Load();
            _selectedItem = selectedItem;
            _reportData = reportData;

            PrintDocument printDoc = new PrintDocument();
            if (printDoc.PrinterSettings.IsValid && _currentSettings.LanguageIndex == 1)
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPageArabic);
                printDoc.Print();
            }
            else if (printDoc.PrinterSettings.IsValid)
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPageEnglish);
                printDoc.Print();
            }
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Converts Data into An Arabic Invoice
        /// </summary>
        private void PrintPageArabic(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;

            Font titleFont = new Font(FontFamily.GenericSansSerif, 14.0f, FontStyle.Bold);
            Font boldFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Bold);
            Font regularFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Regular);

            var centerTopFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Near
            };
            var centerBottomFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Far
            };
            var rightFormat = new StringFormat
            {
                Alignment = StringAlignment.Far,
                LineAlignment = StringAlignment.Center
            };

            // Print Header
            graphics.DrawString(_currentSettings.CompanyName, titleFont, Brushes.Black, new Rectangle(10, 0, 270, 40), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyAddress, regularFont, Brushes.Black, new Rectangle(10, 40, 270, 30), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyPhone, regularFont, Brushes.Black, new Rectangle(10, 70, 270, 15), centerBottomFormat);
            graphics.DrawLine(Pens.Black, 10, 91, 280, 91);
            graphics.DrawLine(Pens.Black, 10, 93, 280, 93);

            // Print Receipt Main Information
            graphics.DrawString("تقرير السلعة", boldFont, Brushes.Black, new Rectangle(10, 95, 270, 20), centerTopFormat);
            graphics.DrawString(":رقم السلعة", regularFont, Brushes.Black, new Rectangle(175, 115, 100, 15), rightFormat);
            graphics.DrawString(_selectedItem.ID.ToString(), regularFont, Brushes.Black, new Rectangle(10, 115, 195, 15), rightFormat);
            graphics.DrawString(":اسم السلعة", regularFont, Brushes.Black, new Rectangle(175, 130, 100, 15), rightFormat);
            graphics.DrawString(_selectedItem.Name, regularFont, Brushes.Black, new Rectangle(10, 130, 195, 15), rightFormat);
            graphics.DrawString(":الكمية الحالية", regularFont, Brushes.Black, new Rectangle(175, 145, 100, 15), rightFormat);
            graphics.DrawString(_selectedItem.Quantity.ToString(), regularFont, Brushes.Black, new Rectangle(10, 145, 195, 15), rightFormat);
            graphics.DrawString(":السعر المورد", regularFont, Brushes.Black, new Rectangle(175, 160, 100, 15), rightFormat);
            graphics.DrawString(_selectedItem.SupplierPrice.ToString(), regularFont, Brushes.Black, new Rectangle(10, 160, 195, 15), rightFormat);
            graphics.DrawString(":السعر البيع", regularFont, Brushes.Black, new Rectangle(175, 175, 100, 15), rightFormat);
            graphics.DrawString(_selectedItem.SalesPrice.ToString(), regularFont, Brushes.Black, new Rectangle(10, 175, 195, 15), rightFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 112, 270, 80));

            // Print Receipt Data
            int yOffset = 200;
            graphics.DrawString("تاريخ البيع", boldFont, Brushes.Black, new Rectangle(10, yOffset, 270, 20), centerTopFormat);
            yOffset += 20;
            int boxOffset = yOffset - 3;
            graphics.DrawString("التاريخ", regularFont, Brushes.Black, new Rectangle(155, yOffset, 115, 15), rightFormat);
            graphics.DrawString("سعر", regularFont, Brushes.Black, new Rectangle(105, yOffset, 50, 15), rightFormat);
            graphics.DrawString("كمية", regularFont, Brushes.Black, new Rectangle(65, yOffset, 40, 15), rightFormat);
            graphics.DrawString("اجمالي", regularFont, Brushes.Black, new Rectangle(15, yOffset, 50, 15), rightFormat);
            yOffset += 22;
            graphics.DrawLine(Pens.Black, 15, yOffset - 4, 275, yOffset - 4);
            graphics.DrawLine(Pens.Black, 15, yOffset - 2, 275, yOffset - 2);

            int boxLength = 30;
            foreach (var item in _reportData.SalesTransactions)
            {
                graphics.DrawString(item.Date.ToString("dd/MM/yyyy HH:mm"), regularFont, Brushes.Black, new Rectangle(150, yOffset, 120, 15), rightFormat);
                graphics.DrawString(item.Price.ToString("F"), regularFont, Brushes.Black, new Rectangle(105, yOffset, 50, 15), rightFormat);
                graphics.DrawString(item.Quantity.ToString(), regularFont, Brushes.Black, new Rectangle(65, yOffset, 40, 15), rightFormat);
                graphics.DrawString((item.Price * item.Quantity).ToString("F"), regularFont, Brushes.Black, new Rectangle(15, yOffset, 50, 15), rightFormat);
                yOffset += 15;
                boxLength += 15;
            }
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, boxOffset, 270, boxLength));

            // Print Receipt Data
            yOffset += 15;
            graphics.DrawString("تاريخ الشراء", boldFont, Brushes.Black, new Rectangle(10, yOffset, 270, 20), centerTopFormat);
            yOffset += 20;
            boxOffset = yOffset - 3;
            graphics.DrawString("التاريخ", regularFont, Brushes.Black, new Rectangle(155, yOffset, 115, 15), rightFormat);
            graphics.DrawString("سعر", regularFont, Brushes.Black, new Rectangle(105, yOffset, 50, 15), rightFormat);
            graphics.DrawString("كمية", regularFont, Brushes.Black, new Rectangle(65, yOffset, 40, 15), rightFormat);
            graphics.DrawString("اجمالي", regularFont, Brushes.Black, new Rectangle(15, yOffset, 50, 15), rightFormat);
            yOffset += 22;
            graphics.DrawLine(Pens.Black, 15, yOffset - 4, 275, yOffset - 4);
            graphics.DrawLine(Pens.Black, 15, yOffset - 2, 275, yOffset - 2);

            boxLength = 30;
            foreach (var item in _reportData.PurchaseTransactions)
            {
                graphics.DrawString(item.Date.ToString("dd/MM/yyyy HH:mm"), regularFont, Brushes.Black, new Rectangle(150, yOffset, 120, 15), rightFormat);
                graphics.DrawString(item.Price.ToString("F"), regularFont, Brushes.Black, new Rectangle(105, yOffset, 50, 15), rightFormat);
                graphics.DrawString(item.Quantity.ToString(), regularFont, Brushes.Black, new Rectangle(65, yOffset, 40, 15), rightFormat);
                graphics.DrawString((item.Price * item.Quantity).ToString("F"), regularFont, Brushes.Black, new Rectangle(15, yOffset, 50, 15), rightFormat);
                yOffset += 15;
                boxLength += 15;
            }
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, boxOffset, 270, boxLength));

            // Print Footer
            yOffset += 25;
            graphics.DrawLine(Pens.Black, 10, yOffset, 280, yOffset);
            graphics.DrawLine(Pens.Black, 10, yOffset + 2, 280, yOffset + 2);
            yOffset += 5;
            graphics.DrawString(_currentSettings.FooterMessage, boldFont, Brushes.Black, new Rectangle(10, yOffset, 270, 40), centerTopFormat);

            titleFont.Dispose();
            boldFont.Dispose();
            regularFont.Dispose();
            centerTopFormat.Dispose();
            centerBottomFormat.Dispose();
            rightFormat.Dispose();

            (sender as PrintDocument).PrintPage -= PrintPageArabic;
        }

        /// <summary>
        /// Converts Data into An English Invoice
        /// </summary>
        private void PrintPageEnglish(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;

            Font titleFont = new Font(FontFamily.GenericSansSerif, 14.0f, FontStyle.Bold);
            Font boldFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Bold);
            Font regularFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Regular);

            var centerTopFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Near
            };
            var centerBottomFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Far
            };
            var leftFormat = new StringFormat
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Center
            };

            // Print Header
            graphics.DrawString(_currentSettings.CompanyName, titleFont, Brushes.Black, new Rectangle(10, 0, 270, 40), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyAddress, regularFont, Brushes.Black, new Rectangle(10, 40, 270, 30), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyPhone, regularFont, Brushes.Black, new Rectangle(10, 70, 270, 15), centerBottomFormat);
            graphics.DrawLine(Pens.Black, 10, 91, 280, 91);
            graphics.DrawLine(Pens.Black, 10, 93, 280, 93);

            // Print Receipt Main Information
            graphics.DrawString("Item Report", boldFont, Brushes.Black, new Rectangle(10, 95, 270, 20), centerTopFormat);
            graphics.DrawString("Item ID:", regularFont, Brushes.Black, new Rectangle(15, 115, 100, 15), leftFormat);
            graphics.DrawString(_selectedItem.ID.ToString(), regularFont, Brushes.Black, new Rectangle(110, 115, 160, 15), leftFormat);
            graphics.DrawString("Item Name:", regularFont, Brushes.Black, new Rectangle(15, 130, 100, 15), leftFormat);
            graphics.DrawString(_selectedItem.Name, regularFont, Brushes.Black, new Rectangle(110, 130, 160, 15), leftFormat);
            graphics.DrawString("Current Qty:", regularFont, Brushes.Black, new Rectangle(15, 145, 100, 15), leftFormat);
            graphics.DrawString(_selectedItem.Quantity.ToString(), regularFont, Brushes.Black, new Rectangle(110, 145, 160, 15), leftFormat);
            graphics.DrawString("Supplier Price:", regularFont, Brushes.Black, new Rectangle(15, 160, 100, 15), leftFormat);
            graphics.DrawString(_selectedItem.SupplierPrice.ToString(), regularFont, Brushes.Black, new Rectangle(110, 160, 160, 15), leftFormat);
            graphics.DrawString("Sales Price:", regularFont, Brushes.Black, new Rectangle(15, 175, 100, 15), leftFormat);
            graphics.DrawString(_selectedItem.SalesPrice.ToString(), regularFont, Brushes.Black, new Rectangle(110, 175, 160, 15), leftFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 112, 270, 80));

            // Print Receipt Data
            int yOffset = 200;
            graphics.DrawString("Sale History", boldFont, Brushes.Black, new Rectangle(10, yOffset, 270, 20), centerTopFormat);
            yOffset += 20;
            int boxOffset = yOffset - 3;
            graphics.DrawString("Date", regularFont, Brushes.Black, new Rectangle(15, yOffset, 115, 15), leftFormat);
            graphics.DrawString("Price", regularFont, Brushes.Black, new Rectangle(130, yOffset, 50, 15), leftFormat);
            graphics.DrawString("Qty", regularFont, Brushes.Black, new Rectangle(180, yOffset, 40, 15), leftFormat);
            graphics.DrawString("Total", regularFont, Brushes.Black, new Rectangle(220, yOffset, 50, 15), leftFormat);
            yOffset += 22;
            graphics.DrawLine(Pens.Black, 15, yOffset - 4, 275, yOffset - 4);
            graphics.DrawLine(Pens.Black, 15, yOffset - 2, 275, yOffset - 2);

            int boxLength = 30;
            foreach (var item in _reportData.SalesTransactions)
            {
                graphics.DrawString(item.Date.ToString("dd/MM/yyyy HH:mm"), regularFont, Brushes.Black, new Rectangle(15, yOffset, 120, 15), leftFormat);
                graphics.DrawString(item.Price.ToString("F"), regularFont, Brushes.Black, new Rectangle(130, yOffset, 50, 15), leftFormat);
                graphics.DrawString(item.Quantity.ToString(), regularFont, Brushes.Black, new Rectangle(180, yOffset, 40, 15), leftFormat);
                graphics.DrawString((item.Price * item.Quantity).ToString("F"), regularFont, Brushes.Black, new Rectangle(220, yOffset, 50, 15), leftFormat);
                yOffset += 15;
                boxLength += 15;
            }
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, boxOffset, 270, boxLength));

            // Print Receipt Data
            yOffset += 15;
            graphics.DrawString("Purchase History", boldFont, Brushes.Black, new Rectangle(10, yOffset, 270, 20), centerTopFormat);
            yOffset += 20;
            boxOffset = yOffset - 3;
            graphics.DrawString("Date", regularFont, Brushes.Black, new Rectangle(15, yOffset, 140, 15), leftFormat);
            graphics.DrawString("Price", regularFont, Brushes.Black, new Rectangle(130, yOffset, 50, 15), leftFormat);
            graphics.DrawString("Qty", regularFont, Brushes.Black, new Rectangle(180, yOffset, 40, 15), leftFormat);
            graphics.DrawString("Total", regularFont, Brushes.Black, new Rectangle(220, yOffset, 50, 15), leftFormat);
            yOffset += 22;
            graphics.DrawLine(Pens.Black, 15, yOffset - 4, 275, yOffset - 4);
            graphics.DrawLine(Pens.Black, 15, yOffset - 2, 275, yOffset - 2);

            boxLength = 30;
            foreach (var item in _reportData.PurchaseTransactions)
            {
                graphics.DrawString(item.Date.ToString("dd/MM/yyyy HH:mm"), regularFont, Brushes.Black, new Rectangle(15, yOffset, 120, 15), leftFormat);
                graphics.DrawString(item.Price.ToString("F"), regularFont, Brushes.Black, new Rectangle(130, yOffset, 50, 15), leftFormat);
                graphics.DrawString(item.Quantity.ToString(), regularFont, Brushes.Black, new Rectangle(180, yOffset, 40, 15), leftFormat);
                graphics.DrawString((item.Price * item.Quantity).ToString("F"), regularFont, Brushes.Black, new Rectangle(220, yOffset, 50, 15), leftFormat);
                yOffset += 15;
                boxLength += 15;
            }
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, boxOffset, 270, boxLength));

            // Print Footer
            yOffset += 25;
            graphics.DrawLine(Pens.Black, 10, yOffset, 280, yOffset);
            graphics.DrawLine(Pens.Black, 10, yOffset + 2, 280, yOffset + 2);
            yOffset += 5;
            graphics.DrawString(_currentSettings.FooterMessage, boldFont, Brushes.Black, new Rectangle(10, yOffset, 270, 40), centerTopFormat);

            titleFont.Dispose();
            boldFont.Dispose();
            regularFont.Dispose();
            centerTopFormat.Dispose();
            centerBottomFormat.Dispose();
            leftFormat.Dispose();

            (sender as PrintDocument).PrintPage -= PrintPageEnglish;
        }
        #endregion
    }
}
