﻿using QRCodeEncoderLibrary;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace SaudiSoft.Core
{
    public class PrintSalesService : IPrintSalesService
    {
        #region Private Members
        private SalesTicket _receiptTicket;
        private ICollection<SalesItem> _receiptItems;
        private AppSettings _currentSettings;
        private string _cashierName;
        private string _paymentName;
        private string _currencyValue;
        #endregion

        #region Public Class Methods
        /// <summary>
        /// Converts Data into Graphics and Prints the Result.
        /// </summary>
        public bool Print(SalesTicket receiptTicket, ICollection<SalesItem> receiptItems, string paymentName, string cashierName,string currencyValue)
        {
            PrintDocument printDoc = new PrintDocument();
            _receiptTicket = receiptTicket;
            _receiptItems = receiptItems;
            _currentSettings = AppSettings.Load();
            _paymentName = paymentName;
            _currencyValue = currencyValue;
            _cashierName = cashierName;
            var IsValid = printDoc.PrinterSettings.IsValid;
            if (printDoc.PrinterSettings.PrinterName.ToLower()!= "Microsoft Print To PDF".ToLower())
            {

            if (printDoc.PrinterSettings.IsValid && _currentSettings.LanguageIndex == 1)
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPageArabic);
                printDoc.Print();
            }
            else if (printDoc.PrinterSettings.IsValid)
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPageEnglish);
                printDoc.Print();
            }
            }
            else
            {
                IsValid = false;
            }
            return IsValid;
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Converts Data into An Arabic Receipt
        /// </summary>
        private void PrintPageArabic(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;

            Font titleFont = new Font(FontFamily.GenericSansSerif, 14.0f, FontStyle.Bold);
            Font boldFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Bold);
            Font regularFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Regular);

            var centerTopFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Near
            };
            var centerBottomFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Far
            };
            var rightFormat = new StringFormat
            {
                Alignment = StringAlignment.Far,
                LineAlignment = StringAlignment.Center
            };

            // Print Header
            graphics.DrawString(_currentSettings.CompanyName, titleFont, Brushes.Black, new Rectangle(10, 0, 270, 40), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyAddress, regularFont, Brushes.Black, new Rectangle(10, 40, 270, 30), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyPhone, regularFont, Brushes.Black, new Rectangle(10, 70, 270, 15), centerBottomFormat);
            graphics.DrawLine(Pens.Black, 10, 91, 280, 91);
            graphics.DrawLine(Pens.Black, 10, 93, 280, 93);

            // Print Receipt Main Information
            graphics.DrawString("فاتورة البيع", boldFont, Brushes.Black, new Rectangle(10, 95, 270, 20), centerTopFormat); // here
            graphics.DrawString(":رقم الفاتورة", regularFont, Brushes.Black, new Rectangle(200, 115, 75, 15), rightFormat);
            graphics.DrawString(_receiptTicket.ID.ToString(), regularFont, Brushes.Black, new Rectangle(10, 115, 195, 15), rightFormat);
            graphics.DrawString(":التاريخ", regularFont, Brushes.Black, new Rectangle(200, 130, 75, 15), rightFormat);
            graphics.DrawString(_receiptTicket.SaleDate.ToString("dd/MM/yyyy hh:mm"), regularFont, Brushes.Black, new Rectangle(10, 130, 195, 15), rightFormat);
            graphics.DrawString(":طريقة الدفع", regularFont, Brushes.Black, new Rectangle(200, 145, 75, 15), rightFormat);
            graphics.DrawString(_paymentName, regularFont, Brushes.Black, new Rectangle(10, 145, 195, 15), rightFormat);
            graphics.DrawString(":الكاشير", regularFont, Brushes.Black, new Rectangle(200, 160, 75, 15), rightFormat);
            graphics.DrawString(_cashierName, regularFont, Brushes.Black, new Rectangle(10, 160, 195, 15), rightFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 112, 270, 65));

            // Print Receipt Data
            graphics.DrawString("السلع المباعة", boldFont, Brushes.Black, new Rectangle(10, 180, 270, 20), centerTopFormat);
            graphics.DrawString("اسم", regularFont, Brushes.Black, new Rectangle(155, 200, 115, 15), rightFormat);
            graphics.DrawString("سعر", regularFont, Brushes.Black, new Rectangle(105, 200, 50, 15), rightFormat);
            graphics.DrawString("كمية", regularFont, Brushes.Black, new Rectangle(65, 200, 40, 15), rightFormat);
            graphics.DrawString("اجمالي", regularFont, Brushes.Black, new Rectangle(15, 200, 50, 15), rightFormat);
            graphics.DrawLine(Pens.Black, 15, 218, 275, 218);
            graphics.DrawLine(Pens.Black, 15, 220, 275, 220);

            int yOffset = 222;
            int boxLength = 30;
            double Tax = 0;
            foreach (var item in _receiptItems)
            {
                graphics.DrawString(item.StockItemName, regularFont, Brushes.Black, new Rectangle(155, yOffset, 115, 15), rightFormat);
                graphics.DrawString(item.Price.ToString("F"), regularFont, Brushes.Black, new Rectangle(105, yOffset, 50, 15), rightFormat);
                graphics.DrawString(item.Quantity.ToString(), regularFont, Brushes.Black, new Rectangle(65, yOffset, 40, 15), rightFormat);
                graphics.DrawString((item.Price * item.Quantity).ToString("F"), regularFont, Brushes.Black, new Rectangle(15, yOffset, 50, 15), rightFormat);
                Tax += item.Tax;
                yOffset += 15;
                boxLength += 15;
            }

            // Print Receipt Total Information
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, yOffset - 3, 280, 65));
            if (_receiptTicket.Discount > 0)
            {

                graphics.DrawString(":الخصم", regularFont, Brushes.Black, new Rectangle(200, yOffset, 75, 15), rightFormat);
                graphics.DrawString(_receiptTicket.Discount.ToString("F") + "%", regularFont, Brushes.Black, new Rectangle(10, yOffset, 195, 15), rightFormat);
                yOffset += 15;
            }
            var discountedTotal = _receiptTicket.CalculatedTotalPrice;
            var subTotal = _receiptTicket.SubTotal;
            graphics.DrawString(":المجموع", regularFont, Brushes.Black, new Rectangle(200, yOffset, 75, 15), rightFormat);
            graphics.DrawString((subTotal + ((_receiptTicket.Tax * subTotal) / 100)).ToString("F"), regularFont, Brushes.Black, new Rectangle(10, yOffset, 195, 15), rightFormat);
            yOffset += 15;
            graphics.DrawString(":المدفوع", regularFont, Brushes.Black, new Rectangle(200, yOffset, 75, 15), rightFormat);
            graphics.DrawString(_receiptTicket.TotalPaid.ToString("F"), regularFont, Brushes.Black, new Rectangle(10, yOffset, 195, 15), rightFormat);
            yOffset += 15;
            graphics.DrawString(":الباقي", regularFont, Brushes.Black, new Rectangle(200, yOffset, 75, 15), rightFormat);
            graphics.DrawString((_receiptTicket.TotalPaid - discountedTotal).ToString("F"), regularFont, Brushes.Black, new Rectangle(10, yOffset, 195, 15), rightFormat);

            // Print Footer
            yOffset += 25;
            graphics.DrawLine(Pens.Black, 10, yOffset, 280, yOffset);
            graphics.DrawLine(Pens.Black, 10, yOffset + 2, 280, yOffset + 2);
            yOffset += 5;
            graphics.DrawString(_currentSettings.FooterMessage, boldFont, Brushes.Black, new Rectangle(10, yOffset, 270, 40), centerTopFormat);
            graphics.DrawString("الرقم الضريبي: " + _currentSettings.TaxNo, boldFont, Brushes.Black, new Rectangle(10, yOffset + 20, 270, 40), centerTopFormat);
            graphics.DrawString("قيمة الضريبة: " + (_receiptTicket.DiscountedTotal * _receiptTicket.Tax / 100).ToString("F") +" "+ _currencyValue, boldFont, Brushes.Black, new Rectangle(10, yOffset + 40, 270, 40), centerTopFormat);
            TLVCls tlv = new TLVCls(_currentSettings.CompanyName, _currentSettings.TaxNo, System.DateTime.Now, discountedTotal, _receiptTicket.DiscountedTotal * _receiptTicket.Tax / 100);
            var qrCodeWriter = new BarcodeWriter<MemoryStream>()
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions
                {
                    Width = 100,
                    Height = 100,
                }
            };
            using (var img = (Bitmap)CreateQrCode(tlv.ToBase64()).Clone())
            {
                graphics.DrawImage(img, 70, yOffset + 60);
            }
            titleFont.Dispose();
            boldFont.Dispose();
            regularFont.Dispose();
            centerTopFormat.Dispose();
            centerBottomFormat.Dispose();
            rightFormat.Dispose();

            (sender as PrintDocument).PrintPage -= PrintPageArabic;
        }

        /// <summary>
        /// Converts Data into An English Receipt
        /// </summary>
        private void PrintPageEnglish(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;

            Font titleFont = new Font(FontFamily.GenericSansSerif, 14.0f, FontStyle.Bold);
            Font boldFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Bold);
            Font regularFont = new Font(FontFamily.GenericSansSerif, 10.0f, FontStyle.Regular);

            var centerTopFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Near
            };
            var centerBottomFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Far
            };
            var leftFormat = new StringFormat
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Center
            };

            // Print Header
            graphics.DrawString(_currentSettings.CompanyName, titleFont, Brushes.Black, new Rectangle(10, 0, 270, 40), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyAddress, regularFont, Brushes.Black, new Rectangle(10, 40, 270, 30), centerBottomFormat);
            graphics.DrawString(_currentSettings.CompanyPhone, regularFont, Brushes.Black, new Rectangle(10, 70, 270, 15), centerBottomFormat);
            graphics.DrawLine(Pens.Black, 10, 91, 280, 91);
            graphics.DrawLine(Pens.Black, 10, 93, 280, 93);

            // Print Receipt Main Information
            graphics.DrawString("Sale Receipt", boldFont, Brushes.Black, new Rectangle(10, 95, 270, 20), centerTopFormat);
            graphics.DrawString("Receipt ID:", regularFont, Brushes.Black, new Rectangle(15, 115, 85, 15), leftFormat);
            graphics.DrawString(_receiptTicket.ID.ToString(), regularFont, Brushes.Black, new Rectangle(95, 115, 175, 15), leftFormat);
            graphics.DrawString("Date:", regularFont, Brushes.Black, new Rectangle(15, 130, 85, 15), leftFormat);
            graphics.DrawString(_receiptTicket.SaleDate.ToString("dd/MM/yyyy hh:mm"), regularFont, Brushes.Black, new Rectangle(95, 130, 175, 15), leftFormat);
            graphics.DrawString("PayMethod:", regularFont, Brushes.Black, new Rectangle(15, 145, 85, 15), leftFormat);
            graphics.DrawString(_paymentName, regularFont, Brushes.Black, new Rectangle(95, 145, 175, 15), leftFormat);
            graphics.DrawString("Cashier:", regularFont, Brushes.Black, new Rectangle(15, 160, 85, 15), leftFormat);
            graphics.DrawString(_cashierName, regularFont, Brushes.Black, new Rectangle(95, 160, 175, 15), leftFormat);
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 112, 270, 65));

            // Print Receipt Data
            graphics.DrawString("Items Sold", boldFont, Brushes.Black, new Rectangle(10, 180, 270, 20), centerTopFormat);
            graphics.DrawString("Name", regularFont, Brushes.Black, new Rectangle(15, 200, 115, 15), leftFormat);
            graphics.DrawString("Price", regularFont, Brushes.Black, new Rectangle(130, 200, 50, 15), leftFormat);
            graphics.DrawString("Qty", regularFont, Brushes.Black, new Rectangle(180, 200, 40, 15), leftFormat);
            graphics.DrawString("Total", regularFont, Brushes.Black, new Rectangle(220, 200, 50, 15), leftFormat);
            graphics.DrawLine(Pens.Black, 15, 218, 275, 218);
            graphics.DrawLine(Pens.Black, 15, 220, 275, 220);

            int yOffset = 222;
            int boxLength = 30;
            double Tax = 0;
            foreach (var item in _receiptItems)
            {
                graphics.DrawString(item.StockItemName, regularFont, Brushes.Black, new Rectangle(15, yOffset, 115, 15), leftFormat);
                graphics.DrawString(item.Price.ToString("F"), regularFont, Brushes.Black, new Rectangle(130, yOffset, 50, 15), leftFormat);
                graphics.DrawString(item.Quantity.ToString(), regularFont, Brushes.Black, new Rectangle(180, yOffset, 40, 15), leftFormat);
                graphics.DrawString((item.Price * item.Quantity).ToString("F"), regularFont, Brushes.Black, new Rectangle(220, yOffset, 50, 15), leftFormat);
                yOffset += 15;
                boxLength += 15;
                Tax += item.Tax;
            }
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, 197, 270, boxLength));

            // Print Receipt Total Information
            yOffset += 15;
            graphics.DrawRectangle(Pens.Black, new Rectangle(10, yOffset - 3, 280, 65));
            if (_receiptTicket.Discount > 0)
            {

                graphics.DrawString("Discount:", regularFont, Brushes.Black, new Rectangle(15, yOffset, 85, 15), leftFormat);
                graphics.DrawString(_receiptTicket.Discount.ToString("F") + "%", regularFont, Brushes.Black, new Rectangle(95, yOffset, 85, 15), leftFormat);
            yOffset += 15;
            }
            var discountedTotal = _receiptTicket.CalculatedTotalPrice;
            var subTotal = _receiptTicket.SubTotal;
            graphics.DrawString("Total:", regularFont, Brushes.Black, new Rectangle(15, yOffset, 85, 15), leftFormat);
            graphics.DrawString((subTotal+((_receiptTicket.Tax*subTotal)/100)).ToString("F"), regularFont, Brushes.Black, new Rectangle(95, yOffset, 175, 15), leftFormat);
            yOffset += 15;
            graphics.DrawString("Paid:", regularFont, Brushes.Black, new Rectangle(15, yOffset, 85, 15), leftFormat);
            graphics.DrawString(_receiptTicket.TotalPaid.ToString("F"), regularFont, Brushes.Black, new Rectangle(95, yOffset, 175, 15), leftFormat);
            yOffset += 15;
            graphics.DrawString("Remaining:", regularFont, Brushes.Black, new Rectangle(15, yOffset, 85, 15), leftFormat);
            graphics.DrawString((_receiptTicket.TotalPaid - discountedTotal).ToString("F"), regularFont, Brushes.Black, new Rectangle(95, yOffset, 175, 15), leftFormat);

            // Print Footer
            yOffset += 25;
            graphics.DrawLine(Pens.Black, 10, yOffset, 280, yOffset);
            graphics.DrawLine(Pens.Black, 10, yOffset + 2, 280, yOffset + 2);
            yOffset += 5;
            graphics.DrawString(_currentSettings.FooterMessage, boldFont, Brushes.Black, new Rectangle(10, yOffset, 270, 40), centerTopFormat);
            graphics.DrawString("TaxNo: "+_currentSettings.TaxNo, boldFont, Brushes.Black, new Rectangle(10, yOffset+20, 270, 40), centerTopFormat);
            graphics.DrawString("TaxValue: " + (_receiptTicket.DiscountedTotal * _receiptTicket.Tax / 100).ToString("F") + " "+ _currencyValue, boldFont, Brushes.Black, new Rectangle(10, yOffset+40, 270, 40), centerTopFormat);
            TLVCls tlv = new TLVCls(_currentSettings.CompanyName, _currentSettings.TaxNo, System.DateTime.Now, discountedTotal, _receiptTicket.DiscountedTotal * _receiptTicket.Tax / 100);
            var qrCodeWriter = new BarcodeWriter<MemoryStream>()
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions
                {
                    Width = 20,
                    Height = 20,
                }
            };
            using (var img = (Bitmap)CreateQrCode(tlv.ToBase64()).Clone())
            {
                graphics.DrawImage(img, 70, yOffset + 60);
            }
            titleFont.Dispose();
            boldFont.Dispose();
            regularFont.Dispose();
            centerTopFormat.Dispose();
            centerBottomFormat.Dispose();
            leftFormat.Dispose();

            (sender as PrintDocument).PrintPage -= PrintPageEnglish;
        }
        public Bitmap CreateQrCode(string content)
        {

            var qrWriter = new ZXing.BarcodeWriter<System.Drawing.Bitmap>
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new ZXing.Common.EncodingOptions { Height = 150, Width = 150, Margin = 0 },
                Renderer = new ZXing.CoreCompat.Rendering.BitmapRenderer()
            };

            return qrWriter.Write(content);
        }
        #endregion
    }
}
