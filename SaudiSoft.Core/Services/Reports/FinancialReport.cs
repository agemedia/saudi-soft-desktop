﻿using System;
using System.Collections.ObjectModel;

namespace SaudiSoft.Core
{
    public class FinancialReport
    {
        #region Public Member
        /// <summary>
        /// Holds Report Min Date Range.
        /// </summary>
        public DateTime FilterFromDate { get; set; } = DateTime.Today;

        /// <summary>
        /// Holds Report Max Date Range.
        /// </summary>
        public DateTime FilterToDate { get; set; } = DateTime.Today;

        /// <summary>
        /// Holds Filter User for Data on Display.
        /// </summary>
        public SystemUser FilterUser { get; set; }

        /// <summary>
        /// Holds Total Sales Value.
        /// </summary>
        public double TotalSales { get; set; }

        /// <summary>
        /// Holds Paid Sales Value.
        /// </summary>
        public double TotalSalesPaid { get; set; }

        /// <summary>
        /// Holds Unpaid Sales Value.
        /// </summary>
        public double TotalSalesDue { get; set; }

        /// <summary>
        /// Holds Total Purchases Value.
        /// </summary>
        public double TotalPurchases { get; set; }

        /// <summary>
        /// Holds Paid Purchases Value.
        /// </summary>
        public double TotalPurchasesPaid { get; set; }

        /// <summary>
        /// Holds Unpaid Purchases Value.
        /// </summary>
        public double TotalPurchasesDue { get; set; }

        /// <summary>
        /// Holds Total Expenses Value.
        /// </summary>
        public double TotalExpenses { get; set; }

        /// <summary>
        /// Holds Gross Profit Value.
        /// </summary>
        public double TotalGrossProfit { get; set; }

        /// <summary>
        /// Holds Net Profit Value.
        /// </summary>
        public double TotalNetProfit { get; set; }

        /// <summary>
        /// Holds the Sold Items Data.
        /// </summary>
        public ObservableCollection<SalesItem> SalesItems { get; set; }

        /// <summary>
        /// Holds the Purchased Items Data.
        /// </summary>
        public ObservableCollection<PurchaseItem> PurchaseItems { get; set; }

        /// <summary>
        /// Holds the Store Expenses Data.
        /// </summary>
        public ObservableCollection<StoreExpense> StoreExpenses { get; set; }
        #endregion
    }
}
