﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public class FinancialReportService : IFinancialReportService
    {
        #region Private Services
        private readonly IDataService<SalesTicket> _salesDataService;
        private readonly IDataService<SalesItem> _salesItemDataService;
        private readonly IDataService<PurchaseTicket> _purchaseDataService;
        private readonly IDataService<PurchaseItem> _purchaseItemDataService;
        private readonly IDataService<StoreExpense> _expenseDataService;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor with passed instances of data services.
        /// </summary>
        public FinancialReportService(IDataService<SalesTicket> salesDataService, IDataService<SalesItem> salesItemDataService, IDataService<PurchaseTicket> purchaseDataService, IDataService<PurchaseItem> purchaseItemDataService, IDataService<StoreExpense> expenseDataService)
        {
            _salesDataService = salesDataService;
            _salesItemDataService = salesItemDataService;
            _purchaseDataService = purchaseDataService;
            _purchaseItemDataService = purchaseItemDataService;
            _expenseDataService = expenseDataService;
        }
        #endregion

        #region Private Class Methods
        private FinancialReport CalculateSales(FinancialReport report, List<SalesTicket> salesTickets, List<SalesItem> salesItems)
        {
            report.TotalSales = 0;
            report.TotalSalesPaid = 0;
            report.SalesItems = new ObservableCollection<SalesItem>();

            foreach (SalesTicket sale in salesTickets)
            {
                double saleTotalPrice = sale.CalculatedTotalPrice;
                double paymentFraction = sale.TotalPaid / saleTotalPrice;

                report.TotalSales += saleTotalPrice;
                report.TotalSalesPaid += sale.TotalPaid;

                IEnumerable<SalesItem> items = salesItems.Where(x => x.SalesTicketID == sale.ID);
                foreach (SalesItem item in items)
                {
                    SalesItem existingItem = report.SalesItems.FirstOrDefault(x => x.StockItemID == item.StockItemID);
                    double totalAmount = item.Price * item.Quantity * (1 - (sale.Discount / 100));
                    if (double.IsNaN(paymentFraction)) paymentFraction = 0;
                    double totalPaid = totalAmount * paymentFraction;

                    item.Price = totalAmount;
                    item.PaidPrice = totalPaid;

                    if (existingItem == null) report.SalesItems.Add(item);
                    else
                    {
                        existingItem.Price += item.Price;
                        existingItem.PaidPrice += item.PaidPrice;
                        existingItem.Quantity += item.Quantity;
                    }
                }
            }
            report.TotalSalesDue = report.TotalSales - report.TotalSalesPaid;

            return report;
        }


        private FinancialReport CalculatePurchases(FinancialReport report, List<PurchaseTicket> purchaseTickets, List<PurchaseItem> purchaseItems)
        {
            report.TotalPurchases = 0;
            report.TotalPurchasesPaid = 0;
            report.PurchaseItems = new ObservableCollection<PurchaseItem>();

            foreach (PurchaseTicket purchase in purchaseTickets)
            {
                double purchaseTotalPrice = purchase.CalculatedTotalPrice;
                double paymentFraction = purchase.TotalPaid / purchaseTotalPrice;

                report.TotalPurchases += purchase.CalculatedTotalPrice;
                report.TotalPurchasesPaid += purchase.TotalPaid;

                IEnumerable<PurchaseItem> ticketItems = purchaseItems.Where(x => x.PurchaseTicketID == purchase.ID);
                foreach (PurchaseItem item in ticketItems)
                {
                    PurchaseItem existingItem = report.PurchaseItems.FirstOrDefault(x => x.StockItemID == item.StockItemID);
                    double totalAmount = item.Price * item.Quantity * (1 - (purchase.Discount / 100));
                    if (double.IsNaN(paymentFraction)) paymentFraction = 0;
                    double totalPaid = totalAmount * paymentFraction;

                    item.Price = totalAmount;
                    item.PaidPrice = totalPaid;

                    if (existingItem == null) report.PurchaseItems.Add(item);
                    else
                    {
                        existingItem.Price += item.Price;
                        existingItem.PaidPrice += item.PaidPrice;
                        existingItem.Quantity += item.Quantity;
                    }
                }
            }
            report.TotalPurchasesDue = report.TotalPurchases - report.TotalPurchasesPaid;

            return report;
        }

        private FinancialReport CalculateExpenses(FinancialReport report, List<StoreExpense> expenses)
        {
            report.TotalExpenses = 0;

            foreach (StoreExpense expense in expenses)
            {
                report.TotalExpenses += expense.Amount;
            }
            report.StoreExpenses = new ObservableCollection<StoreExpense>(expenses);

            return report;
        }

        private FinancialReport CalculateProfit(FinancialReport report)
        {
            report.TotalGrossProfit = report.TotalSales - report.TotalPurchases - report.TotalExpenses;
            report.TotalNetProfit = report.TotalSalesPaid - report.TotalPurchasesPaid - report.TotalExpenses;

            return report;
        }
        #endregion

        #region Public Class Methods
        /// <summary>
        /// Populates Reports with Relevant Data.   
        /// </summary>
        public async Task<FinancialReport> LoadReportData(DateTime fromDate, DateTime toDate, SystemUser filterUser)
        {
            toDate = toDate.AddDays(1);

            FinancialReport report = new FinancialReport
            {
                FilterFromDate = fromDate,
                FilterToDate = toDate,
                FilterUser = filterUser
            };

            List<SalesTicket> salesTickets;
            if (filterUser == null || filterUser.ID == 0)
            {
                salesTickets = await _salesDataService.GetAll(x => x.SaleDate >= fromDate && x.SaleDate <= toDate);
            }
            else
            {
                salesTickets = await _salesDataService.GetAll(x => x.SaleDate >= fromDate && x.SaleDate <= toDate && x.SoldBy == filterUser.ID);
            }

            List<SalesItem> salesItems = new List<SalesItem>();
            foreach (SalesTicket salesTicket in salesTickets)
            {
                List<SalesItem> ticketItems = await _salesItemDataService.GetAll(x => x.SalesTicketID == salesTicket.ID);
                salesItems.AddRange(ticketItems);
            }

            CalculateSales(report, salesTickets, salesItems);

            List<PurchaseTicket> purchaseTickets;
            if (filterUser == null || filterUser.ID == 0)
            {
                purchaseTickets = await _purchaseDataService.GetAll(x => x.PurchaseDate >= fromDate && x.PurchaseDate <= toDate);
            }
            else
            {
                purchaseTickets = await _purchaseDataService.GetAll(x => x.PurchaseDate >= fromDate && x.PurchaseDate <= toDate && x.BoughtBy == filterUser.ID);
            }

            List<PurchaseItem> purchaseItems = new List<PurchaseItem>();
            foreach (PurchaseTicket purchaseTicket in purchaseTickets)
            {
                List<PurchaseItem> ticketItems = await _purchaseItemDataService.GetAll(x => x.PurchaseTicketID == purchaseTicket.ID);
                purchaseItems.AddRange(ticketItems);
            }

            CalculatePurchases(report, purchaseTickets, purchaseItems);

            List<StoreExpense> expenses;
            if (filterUser == null || filterUser.ID == 0)
            {
                expenses = await _expenseDataService.GetAll(x => x.ExpenseDate >= fromDate && x.ExpenseDate <= toDate);
            }
            else
            {
                expenses = await _expenseDataService.GetAll(x => x.ExpenseDate >= fromDate && x.ExpenseDate <= toDate && x.CreatedBy == filterUser.ID);
            }

            CalculateExpenses(report, expenses);
            CalculateProfit(report);

            return report;
        }
        #endregion
    }
}
