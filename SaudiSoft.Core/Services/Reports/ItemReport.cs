﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SaudiSoft.Core
{
    public class ItemReport
    {
        #region Public Member
        /// <summary>
        /// Holds Report Min Date Range.
        /// </summary>
        public DateTime FilterFromDate { get; set; } = DateTime.Today.AddMonths(-1);

        /// <summary>
        /// Holds Report Max Date Range.
        /// </summary>
        public DateTime FilterToDate { get; set; } = DateTime.Today;

        /// <summary>
        /// Name of the StockItem Being Analysed.
        /// </summary>
        public string StockItemName{ get; set; } = "";

        /// <summary>
        /// List of Sales for given item.
        /// </summary>
        public ObservableCollection<ItemReportTransaction> SalesTransactions { get; set; }

        /// <summary>
        /// List of Purchases for given item.
        /// </summary>
        public ObservableCollection<ItemReportTransaction> PurchaseTransactions { get; set; }

        /// <summary>
        /// Holds the X Axis Date Data for a Chart.
        /// </summary>
        public List<string> DateChartData { get; set; }

        /// <summary>
        /// Holds the Total Amount of Sales Per Day.
        /// </summary>
        public List<double> SalesChartData { get; set; }

        /// <summary>
        /// Holds the Total Amount of Purchases Per Day.
        /// </summary>
        public List<double> PurchasesChartData { get; set; }
        #endregion
    }
}
