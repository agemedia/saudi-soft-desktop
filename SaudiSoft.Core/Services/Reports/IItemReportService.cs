﻿using System;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public interface IItemReportService
    {
        Task<ItemReport> LoadReportData(int stockItemID, DateTime fromDate, DateTime toDate);
    }
}