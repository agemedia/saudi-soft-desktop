﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public class ItemReportService : IItemReportService
    {
        #region Private Services
        private readonly IDataService<SalesTicket> _salesDataService;
        private readonly IDataService<SalesItem> _salesItemDataService;
        private readonly IDataService<PurchaseTicket> _purchaseDataService;
        private readonly IDataService<PurchaseItem> _purchaseItemDataService;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor with passed instances of data services.
        /// </summary>
        public ItemReportService(IDataService<SalesTicket> salesDataService, IDataService<SalesItem> salesItemDataService, IDataService<PurchaseTicket> purchaseDataService, IDataService<PurchaseItem> purchaseItemDataService)
        {
            _salesDataService = salesDataService;
            _salesItemDataService = salesItemDataService;
            _purchaseDataService = purchaseDataService;
            _purchaseItemDataService = purchaseItemDataService;
        }
        #endregion

        #region Private Class Methods
        /// <summary>
        /// Calculates Values to be displayed on a Bar Chart.
        /// </summary>
        private ItemReport CalculateValues(ItemReport report)
        {
            // Initializes and Resets Values that will be used.
            DateTime fromDate, toDate;
            report.DateChartData = new List<string>();
            report.SalesChartData = new List<double>();
            report.PurchasesChartData = new List<double>();

            // Cycles through every day in the filter range.
            for (fromDate = report.FilterFromDate; fromDate <= report.FilterToDate; fromDate = fromDate.AddDays(1))
            {
                // Sets end of day as the toDate. (therefore we would be observing 24hrs)
                toDate = fromDate.AddDays(1);

                // Inializes variables to hold the sales and  totals.
                double salesDayTotal = 0;
                double purchasesDayTotal = 0;

                // Gets the total sales within the selected day.
                foreach (var sale in report.SalesTransactions)
                {
                    if (sale.Date >= fromDate && sale.Date <= toDate)
                    {
                        salesDayTotal += sale.TotalPrice;
                    }
                }

                // Gets the total purchases within the selected day.
                foreach (var purchase in report.PurchaseTransactions)
                {
                    if (purchase.Date >= fromDate && purchase.Date <= toDate)
                    {
                        purchasesDayTotal += purchase.TotalPrice;
                    }
                }

                // Saves the date and data if the there exists any sales or purchases on selected day.
                if (salesDayTotal != 0 || purchasesDayTotal != 0)
                {
                    report.DateChartData.Add(fromDate.ToShortDateString());
                    report.SalesChartData.Add(salesDayTotal);
                    report.PurchasesChartData.Add(purchasesDayTotal);
                }
            }

            return report;
        }
        #endregion

        #region Public Class Methods
        /// <summary>
        /// Populates Reports with Relevant Data.   
        /// </summary>
        public async Task<ItemReport> LoadReportData(int stockItemID, DateTime fromDate, DateTime toDate)
        {
            // Initializes and Resets Values that will be used. (toDate was adjusted to include the entirety of the selected date)
            toDate = toDate.AddDays(1);

            ItemReport report = new ItemReport
            {
                FilterFromDate = fromDate,
                FilterToDate = toDate,
                SalesTransactions = new ObservableCollection<ItemReportTransaction>(),
                PurchaseTransactions = new ObservableCollection<ItemReportTransaction>()
            };

            // Gets All SalesTickets in the given range of dates.
            List<SalesTicket> salesTicketData = await _salesDataService.GetAll(x => x.SaleDate >= fromDate && x.SaleDate <= toDate);

            // Gets all SalesItems that are related to the selected item and SalesTickets previously collected.
            foreach (var saleTicket in salesTicketData)
            {
                SalesItem saleItem = await _salesItemDataService.Get(x => x.SalesTicketID == saleTicket.ID && x.StockItemID == stockItemID);
                // If the SalesItem exists it saves it as a Sale Transaction.
                if (saleItem != null)
                {
                    ItemReportTransaction saleTransaction = new ItemReportTransaction
                    {
                        TransactionType = TransactionType.Sale,
                        Date = saleTicket.SaleDate,
                        TicketID = saleTicket.ID,
                        Quantity = saleItem.Quantity,
                        Price = saleItem.Price * (1 - (saleTicket.Discount / 100)),
                        TotalPrice = saleItem.Price * saleItem.Quantity * (1 - (saleTicket.Discount / 100)),
                        RecipientID = saleTicket.CustomerID
                    };
                    report.SalesTransactions.Add(saleTransaction);
                }
            }

            // Gets All PurchaseTickets in the given range of dates.
            List<PurchaseTicket> purchasesTicketData = await _purchaseDataService.GetAll(x => x.PurchaseDate >= fromDate && x.PurchaseDate <= toDate);

            // Gets all PurchaseItems that are related to the selected item and PurchaseTickets previously collected.
            foreach (var purchaseTicket in purchasesTicketData)
            {
                PurchaseItem purchaseItem = await _purchaseItemDataService.Get(x => x.PurchaseTicketID == purchaseTicket.ID && x.StockItemID == stockItemID);
                // If the PurchaseItem exists it saves it as a Purchase Transaction.
                if (purchaseItem != null)
                {
                    ItemReportTransaction purchaseTransaction = new ItemReportTransaction
                    {
                        TransactionType = TransactionType.Purchase,
                        Date = purchaseTicket.PurchaseDate,
                        TicketID = purchaseTicket.ID,
                        Quantity = purchaseItem.Quantity,
                        Price = purchaseItem.Price * (1 - (purchaseTicket.Discount / 100)),
                        TotalPrice = purchaseItem.Price * purchaseItem.Quantity * (1 - (purchaseTicket.Discount / 100)),
                        RecipientID = purchaseTicket.StockSupplierID
                    };
                    report.PurchaseTransactions.Add(purchaseTransaction);
                }
            }

            CalculateValues(report);

            return report;
        }
        #endregion
    }
}
