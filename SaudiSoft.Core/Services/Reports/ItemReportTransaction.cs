﻿using System;

namespace SaudiSoft.Core
{
    public class ItemReportTransaction
    {
        /// <summary>
        /// The date of the history entry.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Type of the item transaction.
        /// </summary>
        public TransactionType TransactionType { get; set; }

        /// <summary>
        /// The amount of given Item being Sold.
        /// </summary>
        public double Quantity { get; set; }

        /// <summary>
        /// The Price of given Item at the time of the transaction.
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// The Total of the transaction.
        /// </summary>
        public double TotalPrice { get; set; }

        /// <summary>
        /// ID of the Ticket that item transaction occured in.
        /// </summary>
        public int TicketID { get; set; }

        /// <summary>
        /// Client/Supplier ID for the item transcation.
        /// </summary>
        public int RecipientID { get; set; }
    }

    public enum TransactionType
    {
        Sale,
        Purchase
    }
}
