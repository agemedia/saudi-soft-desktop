﻿using System;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public interface IFinancialReportService
    {
        Task<FinancialReport> LoadReportData(DateTime fromDate, DateTime toDate, SystemUser filterUser);
    }
}