﻿using System.Collections.Generic;

namespace SaudiSoft.Core
{
    public class DashboardServiceResult
    {
        /// <summary>
        /// Holds Total Number of SalesTicket in the Database.
        /// </summary>
        public int TotalSales { get; set; }

        /// <summary>
        /// Holds Total Number of PurchaseTicket in the Database.
        /// </summary>
        public int TotalPurchases { get; set; }

        /// <summary>
        /// Holds Total Number of Registered Customers of the POS.
        /// </summary>
        public int TotalCustomers { get; set; }

        /// <summary>
        /// Holds Total Number of StockItem Entries in the Database.
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// Holds PaidTotal of Sales Over the course of the week.
        /// </summary>
        public double[] SalesValues { get; set; }

        /// <summary>
        /// Holds String Representation of Days of the Week.
        /// </summary>
        public string[] DateLabels { get; set; }

        /// <summary>
        /// Holds List of Low Stock Items (Below Alert Quantity).
        /// </summary>
        public List<StockItem> LowStockItems { get; set; }

        /// <summary>
        /// Holds List of Items About to Expire Items (Below Alert Quantity).
        /// </summary>
        public List<StockItem> ExpiryStockItems { get; set; }
    }
}
