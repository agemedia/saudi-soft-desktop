﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public class DashboardService : IDashboardService
    {
        #region Private Members
        private readonly IDataService<StockItem> _itemDataService;
        private readonly IDataService<SalesTicket> _salesDataService;
        private readonly IDataService<PurchaseTicket> _purchaseDataService;
        private readonly IDataService<StoreCustomer> _customerDataService;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor with passed instances of data services.
        /// </summary>
        public DashboardService(IDataService<StockItem> itemDataService, IDataService<SalesTicket> salesDataService, IDataService<PurchaseTicket> purchaseDataService, IDataService<StoreCustomer> customerDataService)
        {
            _itemDataService = itemDataService;
            _salesDataService = salesDataService;
            _purchaseDataService = purchaseDataService;
            _customerDataService = customerDataService;
        }
        #endregion

        #region Class Methods/Functions
        /// <summary>
        /// Populates Object's Properites From Database.
        /// </summary>
        public async Task<DashboardServiceResult> PopulateAsync()
        {
            DashboardServiceResult result = new DashboardServiceResult();

            // Get Values For Various Entry Counters.
            result.TotalSales = await _salesDataService.CountEntries();
            result.TotalPurchases = await _purchaseDataService.CountEntries();
            result.TotalItems = await _itemDataService.CountEntries();
            result.TotalCustomers = await _customerDataService.CountEntries();

            // Gets Expiry and LowStock List From Database.
            DateTime todayDate = DateTime.Today;
            DateTime monthAheadDate = todayDate.AddMonths(1);
            result.LowStockItems = await _itemDataService.GetAll(x => x.CanAlertQuantity == true && x.Quantity <= x.AlertQuantity);
            result.ExpiryStockItems = await _itemDataService.GetAll(x => x.CanExpire == true && x.ExpiryDate <= monthAheadDate);
            result.DateLabels = new string[7];
            result.SalesValues = new double[7];

            // Gets and Calculates 7 Days Worth of Sales Values.
            for (int i = 0; i <= 6; i++)
            {
                int arrIndex = 6 - i;
                DateTime ToDate = DateTime.Today.AddDays(-i + 1);
                DateTime FromDate = DateTime.Today.AddDays(-i);
                List<SalesTicket> daySales = await _salesDataService.GetAll(x => x.SaleDate >= FromDate && x.SaleDate <= ToDate);

                result.DateLabels[arrIndex] = FromDate.ToShortDateString();
                result.SalesValues[arrIndex] = 0;

                foreach (var sale in daySales)
                {
                    result.SalesValues[arrIndex] += sale.TotalPaid;
                }
            }
            return result;
        }
        #endregion
    }
}
