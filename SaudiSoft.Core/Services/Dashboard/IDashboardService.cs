﻿using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public interface IDashboardService
    {
        Task<DashboardServiceResult> PopulateAsync();
    }
}