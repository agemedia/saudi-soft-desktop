﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public class SupplierPaymentService : ISupplierPaymentService
    {
        #region Private Services
        private readonly IDataService<StockSupplier> _supplierDataService;
        private readonly IDataService<PurchaseTicket> _ticketsDataService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor with passed instances of data services.
        /// </summary>
        public SupplierPaymentService(IDataService<StockSupplier> supplierDataService, IDataService<PurchaseTicket> ticketsDataService)
        {
            _supplierDataService = supplierDataService;
            _ticketsDataService = ticketsDataService;
        }
        #endregion

        #region Class Members
        /// <summary>
        /// Adds a payment of a supplier to their unpaid tickets.
        /// </summary>
        /// <param name="supplier">Supplier that affects the Payment</param>
        /// <param name="payment">Payment amount</param>
        /// <returns>Modified supplier with updated remaining amount</returns>
        public async Task<StockSupplier> AddPayment(StockSupplier supplier, double payment, SystemUser user)
        {
            // Gets all customer tickets
            List<PurchaseTicket> supplierTickets = await _ticketsDataService.GetAll(predicate: x => x.StockSupplierID == supplier.ID);

            // Only selects ones that unpaid fully.
            supplierTickets = supplierTickets.Where(x => x.CalculatedTotalPrice > x.TotalPaid).ToList();

            foreach (PurchaseTicket ticket in supplierTickets)
            {
                if (payment == 0) break;
                else if (payment >= -ticket.Remaining)
                {
                    payment += ticket.Remaining;
                    supplier.PaidPurchaseDue += -ticket.Remaining;
                    ticket.TotalPaid += -ticket.Remaining;
                }
                else
                {
                    supplier.PaidPurchaseDue += payment;
                    ticket.TotalPaid += payment;
                    payment = 0;
                }
                ticket.Editedby = user.ID;
                await _ticketsDataService.UpdateEntry(ticket);
            }
            await _supplierDataService.UpdateEntry(supplier);
            return supplier;
        }
        #endregion
    }
}
