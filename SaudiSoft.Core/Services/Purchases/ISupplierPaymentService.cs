﻿using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public interface ISupplierPaymentService
    {
        Task<StockSupplier> AddPayment(StockSupplier supplier, double payment, SystemUser user);
    }
}