﻿using SQLite;
using System.IO;

namespace SaudiSoft.Core
{
    public class DataConnection : IDataConnection
    {
        #region Private Connection
        private SQLiteAsyncConnection _connection;
        #endregion

        #region Public Properties
        /// <summary>
        /// Connection property (Can be cast into the correct type later in its users)
        /// </summary>
        public object Connection => _connection;
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public DataConnection()
        {
            Initialize();
        }
        #endregion

        #region Class Methods
        /// <summary>
        /// Initialize the database and create its tables (if they dont exist)
        /// </summary>
        private void Initialize()
        {
            // Creates Directory for program data if it does not exist.
            if (!Directory.Exists(DataPath.programAppData))
            {
                Directory.CreateDirectory(DataPath.programAppData);
            }
            //Initalizes localDB Connection.
            SQLiteConnection connection = new SQLiteConnection(DataPath.localDBFile);
            // Creates each table for the localDB.
            connection.CreateTable<StockCategory>();
            connection.CreateTable<StockBrand>();
            connection.CreateTable<StockItem>();
            connection.CreateTable<StockSupplier>();
            connection.CreateTable<StoreCustomer>();
            connection.CreateTable<StoreExpense>();
            connection.CreateTable<StoreShift>();
            connection.CreateTable<SystemRole>();
            connection.CreateTable<SystemUser>();
            connection.CreateTable<SystemPassword>();
            connection.CreateTable<SalesItem>();
            connection.CreateTable<SalesTicket>();
            connection.CreateTable<PurchaseItem>();
            connection.CreateTable<PurchaseTicket>();

            _connection = new SQLiteAsyncConnection(DataPath.localDBFile);
        }
        #endregion
    }
}
