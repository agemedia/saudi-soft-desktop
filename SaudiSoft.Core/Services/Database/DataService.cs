﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    /// <summary>
    /// Generic Data Service with CRUD Operations on the Database Tables. (Singleton)
    /// </summary>
    /// <typeparam name="T">The Data Table</typeparam>
    public class DataService<T> : IDataService<T> where T : DatabaseModel, new()
    {
        #region Private Members
        /// <summary>
        /// Connection the database.
        /// </summary>
        private readonly SQLiteAsyncConnection _connection;
        #endregion

        #region Constructor
        public DataService(IDataConnection dataConnection)
        {
            _connection = (SQLiteAsyncConnection)dataConnection.Connection;
        }
        #endregion

        #region Class Methods
        /// <summary>
        /// Creates/Inserts an entry into the database.
        /// </summary>
        /// <param name="entity">Entry with data to be created</param>
        /// <returns>ID of the entity after creation</returns>
        public async Task<int> CreateEntry(T entity)
        {
            await _connection.InsertAsync(entity);
            int createdID = await _connection.ExecuteScalarAsync<int>("SELECT last_insert_rowid()");
            return createdID;
        }

        /// <summary>
        /// Updates an entry into the database.
        /// </summary>
        /// <param name="entity">Entry with data to be updated</param>
        /// <returns>Returns true if update was successful</returns>
        public async Task<bool> UpdateEntry(T entity)
        {
            int affectedEntries = await _connection.UpdateAsync(entity);
            return (affectedEntries > 0);
        }

        /// <summary>
        /// Deletes an entry from the database.
        /// </summary>
        /// <param name="entity">Entry with data to be deleted</param>
        /// <returns>Returns true if deletion was successful</returns>
        public async Task<bool> DeleteEntry(T entity)
        {
            int affectedEntries = await _connection.DeleteAsync(entity);
            return (affectedEntries > 0);
        }

        /// <summary>
        /// Gets the number of entries in the database table.
        /// </summary>
        /// <returns>Number of entries</returns>
        public async Task<int> CountEntries()
        {
            int savedEntries = await _connection.ExecuteScalarAsync<int>("SELECT Count(ID) FROM " + typeof(T).Name);
            return savedEntries;
        }

        /// <summary>
        /// Gets an entry from the database.
        /// </summary>
        /// <param name="id">ID of the Entry in the Database.</param>
        /// <returns>Return a data entry from database</returns>
        public async Task<T> Get(int id)
        {
            T entry = await _connection.FindAsync<T>(id);
            return entry;
        }

        /// <summary>
        /// Gets an entry from the database.
        /// </summary>
        /// <param name="id">Linq Query/Condition for the entry.</param>
        /// <returns>Return a data entry from database</returns>
        public async Task<T> Get(Expression<Func<T, bool>> predicate)
        {
            T entry = await _connection.FindAsync<T>(predicate);
            return entry;
        }

        /// <summary>
        /// Gets a list of entries from database.
        /// </summary>
        /// <returns>Returns all entries in a database table</returns>
        public async Task<List<T>> GetAll()
        {
            List<T> entries = await _connection.Table<T>().ToListAsync();
            return entries;
        }

        /// <summary>
        /// Gets a list of entries from database.
        /// </summary>
        /// <param name="predicate">Linq Query/Condition for the entry.</param>
        /// <returns>Return all entries that comply with the query from the database</returns>
        public async Task<List<T>> GetAll(Expression<Func<T, bool>> predicate)
        {
            List<T> entries = await _connection.Table<T>().Where(predicate).ToListAsync();
            return entries;
        }

        /// <summary>
        /// Gets a list of entries from database. (Order them according to a property in descending order)
        /// </summary>
        /// <param name="predicate">Linq Query/Condition for the entry.</param>
        /// <returns>Return all entries that comply with the query from the database</returns>
        public async Task<List<T>> GetAll<TValue>(Expression<Func<T, bool>> predicate, Expression<Func<T, TValue>> orderBy)
        {
            List<T> entries = await _connection.Table<T>().Where(predicate).OrderByDescending(orderBy).ToListAsync();
            return entries;
        }
        #endregion
    }
}
