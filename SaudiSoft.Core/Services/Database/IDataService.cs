﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public interface IDataService<T> where T : DatabaseModel, new()
    {
        Task<int> CountEntries();
        Task<int> CreateEntry(T entity);
        Task<bool> DeleteEntry(T entity);
        Task<T> Get(Expression<Func<T, bool>> predicate);
        Task<T> Get(int id);
        Task<List<T>> GetAll();
        Task<List<T>> GetAll(Expression<Func<T, bool>> predicate);
        Task<List<T>> GetAll<TValue>(Expression<Func<T, bool>> predicate, Expression<Func<T, TValue>> orderBy);
        Task<bool> UpdateEntry(T entity);
    }
}