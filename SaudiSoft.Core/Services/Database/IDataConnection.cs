﻿namespace SaudiSoft.Core
{
    public interface IDataConnection
    {
        object Connection { get; }
    }
}