﻿using Microsoft.Win32;

namespace SaudiSoft.Core
{
    public class ValidateLicenceService : IValidateLicenceService
    {
        public bool ValidateRegistry()
        {
            // Access the registry as 32 - Bit Application
            RegistryKey key32 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
            // Find Application Registry Keys (Added by the Installer) & Checks for its existance.
            RegistryKey key = key32.OpenSubKey(@"Software\Eng Techno\SaudiSoft", false);
            // Checking if there is an registry entry of the software.
            if (key == null)
            {
                return false;
            }
            key.Close();
            return true;
        }

        /// <summary>
        /// Checks if the serial input in the user is the asme as the one set by the setup file.
        /// </summary>
        /// <param name="serialKey">the serial key to be valided </param>
        /// <returns>Returns true if the serial key is valid</returns>
        public bool ValidateSerial(string serialKey)
        {
            //RegistryKey key32 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
            //RegistryKey key = key32.OpenSubKey(@"Software\Eng Techno\SaudiSoft", false);
            //string savedSerial = (string)key.GetValue("Serial");
            //key.Close();

            //if (serialKey != savedSerial)
            //{
            //    return false;
            //}
            return true;
        }
    }
}
