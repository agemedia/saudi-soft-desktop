﻿namespace SaudiSoft.Core
{
    public interface IValidateLicenceService
    {
        bool ValidateRegistry();
        bool ValidateSerial(string serialKey);
    }
}
