﻿using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public interface ICustomerPaymentService
    {
        Task<StoreCustomer> AddPayment(StoreCustomer customer, double payment, SystemUser user);
    }
}