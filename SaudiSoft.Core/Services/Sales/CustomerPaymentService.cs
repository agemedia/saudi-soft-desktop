﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public class CustomerPaymentService : ICustomerPaymentService
    {
        #region Private Services
        private readonly IDataService<StoreCustomer> _customerDataService;
        private readonly IDataService<SalesTicket> _ticketsDataService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor with passed instances of data services.
        /// </summary>
        public CustomerPaymentService(IDataService<StoreCustomer> customerDataService, IDataService<SalesTicket> ticketsDataService)
        {
            _customerDataService = customerDataService;
            _ticketsDataService = ticketsDataService;
        }
        #endregion

        #region Class Members
        /// <summary>
        /// Adds a payment of a customer to their unpaid tickets.
        /// </summary>
        /// <param name="customer">Customer that affects the Payment</param>
        /// <param name="payment">Payment amount</param>
        /// <returns>Modified customer with updated remaining amount</returns>
        public async Task<StoreCustomer> AddPayment(StoreCustomer customer, double payment, SystemUser user)
        {
            // Gets all customer tickets
            List<SalesTicket> customerTickets = await _ticketsDataService.GetAll(predicate: x => x.CustomerID == customer.ID);

            // Only selects ones that unpaid fully.
            customerTickets = customerTickets.Where(x => x.CalculatedTotalPrice > x.TotalPaid).ToList();

            foreach (SalesTicket ticket in customerTickets)
            {
                if (payment == 0) break;
                else if (payment >= -ticket.Remaining)
                {
                    payment += ticket.Remaining;
                    customer.PaidPurchaseDue += -ticket.Remaining;
                    ticket.TotalPaid += -ticket.Remaining;
                }
                else
                {
                    customer.PaidPurchaseDue += payment;
                    ticket.TotalPaid += payment;
                    payment = 0;
                }
                ticket.Editedby = user.ID;
                await _ticketsDataService.UpdateEntry(ticket);
            }
            await _customerDataService.UpdateEntry(customer);
            return customer;
        }
        #endregion
    }
}
