﻿namespace SaudiSoft.Core
{
    public interface IHasherService
    {
        SystemPassword EncryptPassword(string password);
        bool VerifyPassword(string enteredPassword, SystemPassword storedPassword);
    }
}