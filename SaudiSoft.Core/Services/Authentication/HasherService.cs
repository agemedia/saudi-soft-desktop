﻿using System;
using System.Security.Cryptography;

namespace SaudiSoft.Core
{
    public class HasherService : IHasherService
    {
        /// <summary>
        /// Tasks a Password string encrypts it into a Hash & Salt returned as A SystemPassword.
        /// </summary>
        /// <param name="password">String To Be Encrypted</param>
        /// <returns>A SystemPassword Entry with a Hash & Salt strings within it.</returns>
        public SystemPassword EncryptPassword(string password)
        {
            // Creates the Salt
            var saltBytes = new byte[64];
            var provider = new RNGCryptoServiceProvider();
            provider.GetNonZeroBytes(saltBytes);
            var salt = Convert.ToBase64String(saltBytes);

            // Encrypts the Password
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, saltBytes, 10000);
            var hashPassword = Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256));

            // Returns the Hash & Salt
            return new SystemPassword { Hash = hashPassword, Salt = salt };
        }

        /// <summary>
        /// Takes a Password string and verify it with a given SystemPassword
        /// </summary>
        /// <param name="enteredPassword"></param>
        /// <param name="storedPassword"></param>
        /// <returns>A Bool Verifying if the </returns>
        public bool VerifyPassword(string enteredPassword, SystemPassword storedPassword)
        {
            // Converts Salt back to Bytes.
            var saltBytes = Convert.FromBase64String(storedPassword.Salt);

            // Encrypts given Password Using the salt bytes.
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(enteredPassword, saltBytes, 10000);

            // Compares has to the output hash to the saved hash. (Returning a Bool).
            return Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256)) == storedPassword.Hash;
        }
    }
}
