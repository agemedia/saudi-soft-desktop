﻿using Microsoft.Win32;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{

    public class ResultFromActivation
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class AuthService : IAuthService
    {
        #region Private Members
        private readonly IDataService<SystemUser> _userDataService;
        private readonly IDataService<SystemPassword> _passwordDataService;
        private readonly IDataService<SystemRole> _roleDataService;
        private readonly IHasherService _hasherService;
        private readonly IValidateLicenceService _validateLicence;
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        //public AuthService() : this(new DataService<SystemUser>(), new DataService<SystemPassword>(), new DataService<SystemRole>()) { }

        /// <summary>
        /// Constructor with passed instances of data services.
        /// </summary>
        public AuthService(IDataService<SystemUser> userDataService, IDataService<SystemPassword> passwordDataService, IDataService<SystemRole> roleDataService, IHasherService hasherService, IValidateLicenceService validateLicence)
        {
            _userDataService = userDataService;
            _passwordDataService = passwordDataService;
            _roleDataService = roleDataService;
            _hasherService = hasherService;
            _validateLicence = validateLicence;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Checks the database for the number of users in its database.
        /// </summary>
        /// <returns>Number of users in the database</returns>
        public async Task<int> CheckForSavedUsers()
        {
            int savedUsers = await _userDataService.CountEntries();
            return savedUsers;
        }

        /// <summary>
        /// Registers an adminstractor user when the system has no users.
        /// </summary>
        /// <param name="serialKey">Serial Key of the Installer</param>
        /// <param name="signupFullName">User Full Name</param>
        /// <param name="signupUsername">Username (To be longed in with)</param>
        /// <param name="signupPassword">Password in string format</param>
        /// <returns>AuthServiceResult with created user and admin role</returns>
        public async Task<AuthServiceResult> FirstTimeRegister(string serialKey, string signupFullName, string signupUsername, string signupPassword)
        {
            AuthServiceResult result = new AuthServiceResult();

            // If the Input is not Filled in, Informs back with a message.
            if (signupFullName == "" || signupUsername == "")
            {
                result.ResultMessage = AuthMessage.MissingName;
                return result;
            }
            else if (signupPassword == "")
            {
                result.ResultMessage = AuthMessage.MissingPassword;
                return result;
            }

            // Creates User and Role to Log in.
            SystemRole newRole = new SystemRole { Name = "Admin", Description = "Full Access To Features" };
            newRole.ID = await _roleDataService.CreateEntry(newRole).ConfigureAwait(false);

            SystemUser newUser = new SystemUser { Name = signupFullName, Username = signupUsername, RoleID = newRole.ID };
            newUser.ID = await _userDataService.CreateEntry(newUser).ConfigureAwait(false);

            SystemPassword newUserPassword = await Task.Run(() => _hasherService.EncryptPassword(signupPassword)).ConfigureAwait(false);
            newUserPassword.UserID = newUser.ID;
            newUserPassword.ID = await _passwordDataService.CreateEntry(newUserPassword);

            result.ResultMessage = AuthMessage.RegisterSuccessful;
            result.ResultUser = newUser;
            result.ResultRole = newRole;

            return result;
        }

        /// <summary>
        /// Login a user by verifying username and password.
        /// </summary>
        /// <param name="loginUsername">Given Username</param>
        /// <param name="loginPassword">Given Password</param>
        /// <returns>AuthServiceResult With all Operation Data and Status</returns>
        public async Task<AuthServiceResult> Login(string loginUsername, string loginPassword)
        {
            AuthServiceResult result = new AuthServiceResult();

            // If the Input is not Filled in, Informs back with a message.
            if (loginUsername == "")
            {
                result.ResultMessage = AuthMessage.MissingName;
                return result;
            }
            else if (loginPassword == "")
            {
                result.ResultMessage = AuthMessage.MissingPassword;
                return result;
            }

            // Gets user data from database, returns InvalidUsername if not found.
            SystemUser savedUser = await _userDataService.Get(x => x.Username == loginUsername).ConfigureAwait(false);

            if (savedUser == null)
            {
                result.ResultMessage = AuthMessage.InvalidUsername;
                return result;
            }

            // Gets password data from database, returns InvalidPassword if not found.
            SystemPassword savedPassword = await _passwordDataService.Get(x => x.UserID == savedUser.ID).ConfigureAwait(false);
            bool passwordsMatch = await Task.Run(() => _hasherService.VerifyPassword(loginPassword, savedPassword)).ConfigureAwait(false);

            if (!passwordsMatch)
            {
                result.ResultMessage = AuthMessage.InvalidPassword;
                return result;
            }

            // If auth is essentially successful, gets the role of the user and return all this data.
            SystemRole savedRole = await _roleDataService.Get(savedUser.RoleID).ConfigureAwait(false);

            result.ResultMessage = AuthMessage.LoginSuccessful;
            result.ResultUser = savedUser;
            result.ResultRole = savedRole;

            return result;
        }
        /// <summary>
        /// Login a user by verifying username and password.
        /// </summary>
        /// <param name="loginUsername">Given Username</param>
        /// <param name="loginPassword">Given Password</param>
        /// <returns>AuthServiceResult With all Operation Data and Status</returns>
        public async Task<Tuple<AuthServiceResult,string>> RegisterKey(string Key)
        {
            AuthServiceResult result = new AuthServiceResult();
            // If the Input is not Filled in, Informs back with a message.
            var Msg = "";
            if (string.IsNullOrEmpty(Key))
            {
                result.ResultMessage = AuthMessage.MissingSerial;
                return new Tuple<AuthServiceResult, string>(result, Msg);
            }

            // Gets user data from Web Service, returns Key if not found.
            var resultFromApi = await GetResponse<ResultFromActivation>(Key);
            if(resultFromApi.status== "not_valid")
            {
                result.ResultMessage = AuthMessage.MissingSerial;
                Msg = resultFromApi.message;
                return new Tuple<AuthServiceResult, string>(result, Msg);
            }
            result.ResultMessage = AuthMessage.LoginSuccessful;
            return new Tuple<AuthServiceResult, string>(result, Msg);
        }
        public async Task<T> GetResponse<T>(string key) where T : new()
        {
            try
            {
                //_logService.LogCrash($"Starting Sync for endpoint {EndPoint}");
                //_logService.LogCrash($"data for this endpoint {EndPoint}, {bodyRequest}");
                var client = new RestClient("https://agemedia.me/saudi_soft_activation/");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Cookie", "ci_session=e814bf2191eaf20572a56781160ce99320129a72");
                request.AlwaysMultipartFormData = true;
                request.AddParameter("api_key", key);
                request.AddParameter("hardware", MotherboardInfo.SerialNumber);
                IRestResponse response =await client.ExecuteAsync(request);
                Console.WriteLine(response.Content);
                return JsonConvert.DeserializeObject<T>(response.Content);
                
            }
            catch (Exception ex)
            {
                return new T();
            }

        }
        public HttpRequestMessage CreateRequestMessage(HttpContent body, string EndPoint)
        {
            var request = new HttpRequestMessage();

            request.Method = HttpMethod.Post;
            request.RequestUri = new Uri($"https://agemedia.me/{EndPoint}/");
            request.Content = body;
            return request;

        }

        #endregion
    }
}
