﻿using System;
using System.Threading.Tasks;

namespace SaudiSoft.Core
{
    public interface IAuthService
    {
        Task<int> CheckForSavedUsers();
        Task<AuthServiceResult> FirstTimeRegister(string serialKey, string signupFullName, string signupUsername, string signupPassword);
        Task<AuthServiceResult> Login(string loginUsername, string loginPassword);
        Task<Tuple<AuthServiceResult, string>> RegisterKey(string Key);
    }
}