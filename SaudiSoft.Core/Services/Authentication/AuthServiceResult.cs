﻿namespace SaudiSoft.Core
{
    public class AuthServiceResult
    {
        public AuthMessage ResultMessage { get; set; }

        public SystemUser ResultUser { get; set; }

        public SystemRole ResultRole { get; set; }
    }

    public enum AuthMessage
    {
        LoginSuccessful,
        RegisterSuccessful,
        InvalidSerial,
        InvalidUsername,
        InvalidPassword,
        InvalidConfirmPassword,
        MissingSerial,
        MissingName,
        MissingRole,
        MissingPassword,
    }
}
