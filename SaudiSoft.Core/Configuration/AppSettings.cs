﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace SaudiSoft.Core
{
    public class AppSettings
    {
        #region Public Properties
        /// <summary>
        /// Holds the Index of Application Language Dictionary.
        /// </summary>
        public int LanguageIndex { get; set; } = 0;
        public string Key { get; set; }
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Holds the Index of Currency Label used in the Application.
        /// </summary>
        public int CurrencyIndex { get; set; } = 0;
       
        /// <summary>
        /// Holds the Index of Item Display Option
        /// </summary>
        public bool ItemDisplayOption { get; set; } = true;

        /// <summary>
        /// Holds Company Name that will be displayed on Receipt.
        /// </summary>
        public string CompanyName { get; set; } = "SaudiSoft";

        /// <summary>
        /// Holds Company Address that will be displayed on Receipt.
        /// </summary>
        public string CompanyAddress { get; set; } = "Egypt";

        /// <summary>
        /// Holds Company Phone that will be displayed on Receipt.
        /// </summary>
        public string CompanyPhone { get; set; } = "01092485755";
        /// <summary>
        /// Tax No.
        /// </summary>
        public string TaxNo { get; set; } = "123456789123456";
        /// <summary>
        /// Tax Value.
        /// </summary>
        public double TaxValue { get; set; } = 15;

        /// <summary>
        /// Holds Footer Message that will be displayed on Receipt.
        /// </summary>
        public string FooterMessage { get; set; } = "Eng Techno";

        /// <summary>
        /// Holds whether or not a print dialog is shown on checkout.
        /// </summary>
        public bool ShowPrintDialog { get; set; } = true;
        #endregion

        #region Class Methods
        /// <summary>
        /// Saves AppSettings into local Setting Config File.
        /// </summary>
        public void Save()
        {
            
            using (var sw = new StreamWriter(DataPath.localSettingsFile))
            {
                var xmls = new XmlSerializer(typeof(AppSettings));
                xmls.Serialize(sw, this);
            }
        }

        /// <summary>
        /// Loads AppSettings from local Setting Config File, (Creates One if it Does not Exist).
        /// </summary>
        public static AppSettings Load()
        {
            // Creates Directory for program data if it does not exist.
            if (!Directory.Exists(DataPath.programAppData))
            {
                Directory.CreateDirectory(DataPath.programAppData);
            }

            // Creates a setting file if it does not exist.
            if (!File.Exists(DataPath.localSettingsFile))
            {
                var defaultSettings = new AppSettings();
                defaultSettings.Save();
            }

            using (var sw = new StreamReader(DataPath.localSettingsFile))
            {
                var xmls = new XmlSerializer(typeof(AppSettings));
                return xmls.Deserialize(sw) as AppSettings;
            }
        }
        #endregion
    }
}
