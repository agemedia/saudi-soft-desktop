﻿using System;
using System.IO;

namespace SaudiSoft.Core
{
    public class DataPath
    {
        public static string localAppData =Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

        public static string programAppData = Path.Combine(localAppData, "SaudiSoftDb");

        public static string localDBFile = Path.Combine(programAppData, "POS_Data.db");

        public static string localSettingsFile = Path.Combine(programAppData, "POS_Settings.config");
    }
}
