﻿namespace SaudiSoft.Core
{
    public class StockSupplier : DatabaseModel
    {
        /// <summary>
        /// Name for the Supplier. (Visible On the UI).
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Email for the Supplier.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Mobile of the Supplier.
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// Phone of the Supplier.
        /// </summary>
        public string Phone { get; set; }
        
        /// <summary>
        /// GST Number of the Supplier.
        /// </summary>
        public string GSTNumber { get; set; }
        
        /// <summary>
        /// TAX Number of the Supplier.
        /// </summary>
        public string TAXNumber { get; set; }

        /// <summary>
        /// Country of what the Supplier Will Hold.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// State of what the Supplier Will Hold.
        /// </summary>
        public string State { get; set; }
        
        /// <summary>
        /// State of what the Supplier Will Hold.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Address of what the Supplier Will Hold.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// The value of the items purchased from this supplier.
        /// </summary>
        public double PurchaseDue { get; set; }

        /// <summary>
        /// The value of money payed back to the supplier.
        /// </summary>
        public double PaidPurchaseDue { get; set; }

        /// <summary>
        /// Remaining Unpaid amount to the supplier
        /// </summary>
        public double Remaining => PurchaseDue - PaidPurchaseDue;

        /// <summary>
        /// Creates a shallow copy of the current object.
        /// </summary>
        /// <returns></returns>
        public StockSupplier ShallowCopy()
        {
            return (StockSupplier)this.MemberwiseClone();
        }
    }
}
