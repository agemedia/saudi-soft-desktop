﻿using System;

namespace SaudiSoft.Core
{
    public class PurchaseTicket : DatabaseModel
    {
        /// <summary>
        /// Number of different SalesItems Entries associated with Ticket.
        /// </summary>
        public int Entries { get; set; }

        /// <summary>
        /// Total Price For Payment.
        /// </summary>
        public double SubTotal { get; set; }
        
        /// <summary>
        /// Total Tax.
        /// </summary>
        public double Tax { get; set; }

        /// <summary>
        /// General Discount On Entire Ticket Total.
        /// </summary>
        public double Discount { get; set; }

        /// <summary>
        /// Calculated Total Price With the discount and tax
        /// </summary>
        public double CalculatedTotalPrice => (SubTotal * (1 - (Discount / 100))) + (SubTotal * (Tax / 100));
        public double DiscountedTotal => (SubTotal * (1 - (Discount / 100)));

        /// <summary>
        /// Total Amount Paid By the Store.
        /// </summary>
        public double TotalPaid { get; set; }

        /// <summary>
        /// Remaining Unpaid amount of the total
        /// </summary>
        public double Remaining => TotalPaid - CalculatedTotalPrice;

        /// <summary>
        /// ID of the Supplier items being bought from.
        /// </summary>
        public int StockSupplierID { get; set; }

        /// <summary>
        /// Payment Method Used By  the Store.
        /// </summary>
        public PaymentMethod PayMethod { get; set; } = 0;

        /// <summary>
        /// The date of the purchase.
        /// </summary>
        public DateTime PurchaseDate { get; set; } = DateTime.Today;

        /// <summary>
        /// Holds User ID of person who completed the sale.
        /// </summary>
        public int BoughtBy { get; set; }

        /// <summary>
        /// Holds User ID of person who edited the purchase last.
        /// </summary>
        public int Editedby { get; set; }

        /// <summary>
        /// Creates a shallow copy of the current object.
        /// </summary>
        public PurchaseTicket ShallowCopy()
        {
            return (PurchaseTicket)this.MemberwiseClone();
        }
    }
}
