﻿using SQLite;

namespace SaudiSoft.Core
{
    public class PurchaseItem : DatabaseModel
    {
        /// <summary>
        /// ID of the PurchaseTicket that item was sold in.
        /// </summary>
        public int PurchaseTicketID { get; set; }

        /// <summary>
        /// ID of the StockItem Being Sold.
        /// </summary>
        public int StockItemID { get; set; }

        /// <summary>
        /// The amount of given Item being Sold.
        /// </summary>
        public double Quantity { get; set; }
        public double Tax { get; set; }

        /// <summary>
        /// The PurchasePrice of given Item at the time it was being Purchased.
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// The Paid Price of given Item at the time it was being Purchased/
        /// </summary>
        [Ignore]
        public double PaidPrice { get; set; }

        /// <summary>
        /// Name of the StockItem For Printed Receipt. (Not Included In the Database)
        /// </summary>
        [Ignore]
        public string StockItemName { get; set; }

        /// <summary>
        /// Creates a shallow copy of the current object.
        /// </summary>
        /// <returns></returns>
        public PurchaseItem ShallowCopy()
        {
            return (PurchaseItem)this.MemberwiseClone();
        }
    }
}
