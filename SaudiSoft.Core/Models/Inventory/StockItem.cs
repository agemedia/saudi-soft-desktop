﻿using SQLite;
using System;

namespace SaudiSoft.Core
{
    public class StockItem : DatabaseModel
    {
        /// <summary>
        /// The name of the item.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Barcode Number of the Item.
        /// </summary>
        public string Barcode { get; set; }

        /// <summary>
        ///  ID of the brand items belongs to.
        /// </summary>
        public int BrandID { get; set; }

        /// <summary>
        /// ID of the category items belongs to.
        /// </summary>
        public int CategoryID { get; set; }

        /// <summary>
        /// The amount of the item in stock.
        /// </summary>
        public double Quantity { get; set; }

        /// <summary>
        /// The Units of the item.
        /// </summary>
        public int Unit { get; set; } = 0;

        /// <summary>
        /// Alert on Dashboard if value is below alert quantity.
        /// </summary>
        public bool CanAlertQuantity { get; set; } = false;

        /// <summary>
        /// The min amount of item required, before informing the user of low stock.
        /// </summary>
        public double AlertQuantity { get; set; }

        /// <summary>
        /// Does the item expire or has an expiration date.
        /// </summary>
        public bool CanExpire { get; set; } = false;

        /// <summary>
        /// The Expiry Date of the item.
        /// </summary>
        public DateTime ExpiryDate { get; set; } = DateTime.Today;

        /// <summary>
        /// Supplier Price of the Item.
        /// </summary>
        public double SupplierPrice { get; set; }

        /// <summary>
        /// Tax on the item.
        /// </summary>
        public double Tax { get; set; }

        /// <summary>
        /// Desired Profit Margin for the item.
        /// </summary>
        public double SalesPrice { get; set; }

        /// <summary>
        /// Possible Image of the item.
        /// </summary>
        public byte[] Image { get; set; }

        /// <summary>
        /// Equals Override for Generic Lists.
        /// </summary>
        /// <returns>Bool of Equality</returns>
        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            if (ReferenceEquals(this, other))
                return true;
            if (GetType() != other.GetType())
                return false;

            DatabaseModel databaseEntity = (DatabaseModel)other;
            if (ID != databaseEntity.ID)
                return false;
            return true;
        }

        /// <summary>
        /// GetHashCode Override
        /// </summary>
        /// <returns>Returns the base implementation</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Creates a shallow copy of the current object.
        /// </summary>
        /// <returns></returns>
        public StockItem ShallowCopy()
        {
            return (StockItem)this.MemberwiseClone();
        }
    }
}
