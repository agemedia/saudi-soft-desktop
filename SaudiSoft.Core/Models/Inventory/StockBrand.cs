﻿using SQLite;

namespace SaudiSoft.Core
{
    public class StockBrand : DatabaseModel
    {
        /// <summary>
        /// Name for the Brand. (Visible On the UI).
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Internal Code for the Brand for the Store.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Description of what the Brand Will Hold.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Equals Override for Generic Lists.
        /// </summary>
        /// <returns>Bool of Equality</returns>
        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            if (ReferenceEquals(this, other))
                return true;
            if (GetType() != other.GetType())
                return false;

            DatabaseModel databaseEntity = (DatabaseModel)other;
            if (ID != databaseEntity.ID)
                return false;
            return true;
        }

        /// <summary>
        /// GetHashCode Override
        /// </summary>
        /// <returns>Returns the base implementation</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Creates a shallow copy of the current object.
        /// </summary>
        /// <returns></returns>
        public StockBrand ShallowCopy()
        {
            return (StockBrand)this.MemberwiseClone();
        }
    }
}
