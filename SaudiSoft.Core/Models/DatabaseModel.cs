﻿using SQLite;

namespace SaudiSoft.Core
{
    /// <summary>
    /// Base class for any model for database tables.
    /// </summary>
    public class DatabaseModel
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int ID { get; set; }
    }
}
