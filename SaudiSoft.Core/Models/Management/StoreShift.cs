﻿using System;

namespace SaudiSoft.Core
{
    public class StoreShift : DatabaseModel
    {
        /// <summary>
        /// User ID for the Shift User.
        /// </summary>
        public int UserID { get; set; } = 0;

        /// <summary>
        /// Start time & date of the shift.
        /// </summary>/
        public DateTime StartDate { get; set; } = DateTime.Now;

        /// <summary>
        /// End time & date of the shift.
        /// </summary>
        public DateTime EndDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Start of Shift Register Cash In
        /// </summary>
        public double CashIn { get; set; }

        /// <summary>
        /// End of Shift Register Cash Out
        /// </summary>
        public double CashOut { get; set; }

        /// <summary>
        /// Additional Notes (Optional).
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Starts the Shift by saving it in the Database.
        /// </summary>
        /// <param name="userID">ID of the user Starting the Shift</param>
        public void StartShift(int userID)
        {
            this.UserID = userID;
            this.StartDate = this.EndDate = DateTime.Now;
            this.CashOut = this.CashIn;
        }

        /// <summary>
        /// Ends the shift by update its data in the Database.
        /// </summary>
        public void EndShift()
        {
            this.EndDate = DateTime.Now;
        }
    }
}
