﻿using SQLite;
using System;

namespace SaudiSoft.Core
{
    public class StoreExpense : DatabaseModel
    {
        /// <summary>
        /// Name for the Expense. (Visible On the UI).
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description of what Expense is For.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Date of the Expense.
        /// </summary>
        public DateTime ExpenseDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Amount of the Expense.
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// Holds User ID for that created the expense.
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Holds User ID of person who edited the expense last.
        /// </summary>
        public int Editedby { get; set; }

        /// <summary>
        /// Creates a shallow copy of the current object.
        /// </summary>
        /// <returns></returns>
        public StoreExpense ShallowCopy()
        {
            return (StoreExpense)this.MemberwiseClone();
        }
    }
}
