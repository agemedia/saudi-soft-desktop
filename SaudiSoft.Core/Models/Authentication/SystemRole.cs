﻿using SQLite;

namespace SaudiSoft.Core
{
    public class SystemRole : DatabaseModel
    {
        #region Public Properties (Identification)
        /// <summary>
        /// Name for the Role Group.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Small Description of what the role gives.
        /// </summary>
        public string Description { get; set; }
        #endregion

        #region Public Properties (Permissions)
        public bool OpenPOSTab { get; set; } = true;

        public bool OpenSalesListTab { get; set; } = true;

        public bool EditSalesEntries { get; set; } = true;

        public bool DeleteSalesEntries { get; set; } = true;

        public bool OpenCustomersTab { get; set; } = true;

        public bool AddCustomersEntries { get; set; } = true;

        public bool EditCustomersEntries { get; set; } = true;

        public bool DeleteCustomersEntries { get; set; } = true;

        public bool OpenPurchasesListTab { get; set; } = true;

        public bool AddPurchasesEntries { get; set; } = true;

        public bool EditPurchasesEntries { get; set; } = true;

        public bool DeletePurchasesEntries { get; set; } = true;

        public bool OpenSuppliersTab { get; set; } = true;

        public bool AddSuppliersEntries { get; set; } = true;

        public bool EditSuppliersEntries { get; set; } = true;

        public bool DeleteSuppliersEntries { get; set; } = true;

        public bool OpenItemsListTab { get; set; } = true;

        public bool AddItemsEntries { get; set; } = true;

        public bool EditItemsEntries { get; set; } = true;

        public bool DeleteItemsEntries { get; set; } = true;

        public bool OpenBrandsListTab { get; set; } = true;

        public bool AddBrandsEntries { get; set; } = true;

        public bool EditBrandsEntries { get; set; } = true;

        public bool DeleteBrandsEntries { get; set; } = true;

        public bool OpenCategoriesListTab { get; set; } = true;

        public bool AddCategoriesEntries { get; set; } = true;

        public bool EditCategoriesEntries { get; set; } = true;

        public bool DeleteCategoriesEntries { get; set; } = true;

        public bool OpenExpensesPage { get; set; } = true;

        public bool AddExpensesEntries { get; set; } = true;

        public bool EditExpensesEntries { get; set; } = true;

        public bool DeleteExpensesEntries { get; set; } = true;

        public bool OpenFinancialReport { get; set; } = true;

        public bool OpenItemReport { get; set; } = true;

        public bool OpenUsersListTab { get; set; } = true;

        public bool AddUsersEntries { get; set; } = true;

        public bool EditUsersEntries { get; set; } = true;

        public bool DeleteUsersEntries { get; set; } = true;

        public bool OpenRolesTab { get; set; } = true;

        public bool OpenCurrentShiftTab { get; set; } = true;

        public bool OpenShiftsListTab { get; set; } = true;

        public bool DeleteShiftsEntries { get; set; } = true;

        public bool OpenSettingsPage { get; set; } = true;
        #endregion

        #region Derived Public Properties
        [Ignore]
        public bool OpenSalesPage { get { return (OpenPOSTab || OpenSalesListTab || OpenCustomersTab); } }

        [Ignore]
        public bool ViewCustomersAction { get { return (EditCustomersEntries || DeleteCustomersEntries); } }

        [Ignore]
        public bool OpenPurchasesPage { get { return (OpenPurchasesListTab || OpenSuppliersTab); } }

        [Ignore]
        public bool ViewSupplierAction { get { return (EditSuppliersEntries || DeleteSuppliersEntries); } }

        [Ignore]
        public bool OpenItemsPage { get { return (OpenItemsListTab || OpenBrandsListTab || OpenCategoriesListTab); } }
        
        [Ignore]
        public bool ViewItemsAction { get { return (EditItemsEntries || DeleteItemsEntries || OpenItemReport); } }

        [Ignore]
        public bool ViewBrandsAction { get { return (EditBrandsEntries || DeleteBrandsEntries); } }

        [Ignore]
        public bool ViewCategoriesAction { get { return (EditCategoriesEntries || DeleteCategoriesEntries); } }

        [Ignore]
        public bool OpenReportsPage { get { return (OpenFinancialReport || OpenItemReport); } }

        [Ignore]
        public bool OpenExpensesAction { get { return (EditExpensesEntries || DeleteExpensesEntries); } }

        [Ignore]
        public bool OpenUsersPage { get { return (OpenUsersListTab || OpenRolesTab); } }

        [Ignore]
        public bool ViewUsersAction { get { return (EditUsersEntries || DeleteUsersEntries); } }

        [Ignore]
        public bool OpenShiftsPage { get { return (OpenCurrentShiftTab || OpenShiftsListTab); } }
        #endregion

        #region Public Methods
        /// <summary>
        /// Creates a shallow copy of the current object.
        /// </summary>
        public SystemRole ShallowCopy()
        {
            return (SystemRole)this.MemberwiseClone();
        }
        #endregion
    }
}
