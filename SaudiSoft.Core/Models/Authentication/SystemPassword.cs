﻿using SQLite;

namespace SaudiSoft.Core
{
    public class SystemPassword : DatabaseModel
    {
        /// <summary>
        /// ID of SystemUser that owns the Password.
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// Hash of the Password.
        /// </summary>
        public string Hash { get; set; }

        /// <summary>
        /// Salt of the Password.
        /// </summary>
        public string Salt { get; set; }
    }
}
