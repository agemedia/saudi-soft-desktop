﻿using SQLite;

namespace SaudiSoft.Core
{
    public class SystemUser : DatabaseModel
    {
        /// <summary>
        /// Real Name of the User.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Login User Name of the User.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Mobile of the User.
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// Email of the User.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// ID of the brand items belongs to.
        /// </summary>
        public int RoleID { get; set; }

        /// <summary>
        /// Possible Pfofile Picture of the User.
        /// </summary>
        public byte[] Image { get; set; }

        /// <summary>
        /// Creates a shallow copy of the current object.
        /// </summary>
        /// <returns></returns>
        public SystemUser ShallowCopy()
        {
            return (SystemUser)this.MemberwiseClone();
        }
    }
}
