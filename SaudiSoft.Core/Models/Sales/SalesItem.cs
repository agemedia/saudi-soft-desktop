﻿using SQLite;

namespace SaudiSoft.Core
{
    public class SalesItem : DatabaseModel
    {
        /// <summary>
        /// ID of the SalesTicket that item was sold in.
        /// </summary>
        public int SalesTicketID { get; set; }

        /// <summary>
        /// ID of the StockItem Being Sold.
        /// </summary>
        public int StockItemID { get; set; }

        /// <summary>
        /// The amount of given Item being Sold.
        /// </summary>
        public double Quantity { get; set; }
        public double Tax { get; set; }

        /// <summary>
        /// The SalesPrice of given Item at the time it was being Sold.
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// The Paid Price of given Item at the time it was being Sold.
        /// </summary>
        [Ignore]
        public double PaidPrice { get; set; }

        /// <summary>
        /// Name of the StockItem For Printed Receipt. (Not Included In the Database)
        /// </summary>
        [Ignore]
        public string StockItemName { get; set; }

        /// <summary>
        /// Creates a shallow copy of the current object.
        /// </summary>
        /// <returns></returns>
        public SalesItem ShallowCopy()
        {
            return (SalesItem)this.MemberwiseClone();
        }
    }
}
