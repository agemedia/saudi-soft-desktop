﻿namespace SaudiSoft.Core
{
    public class StoreCustomer : DatabaseModel
    {
        /// <summary>
        /// Name for the Customer. (Visible On the UI).
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Email for the Customer.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Mobile of the Customer.
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// Phone of the Customer.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Country of the Customer.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// State of the Customer.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// State of the Customer.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Address of the Customer.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// The value of the items purchased by the Customer.
        /// </summary>
        public double PurchaseDue { get; set; }

        /// <summary>
        /// The amount of money paid by the Customer.
        /// </summary>
        public double PaidPurchaseDue { get; set; }

        /// <summary>
        /// Remaining Unpaid amount by the customer
        /// </summary>
        public double Remaining => PurchaseDue - PaidPurchaseDue;

        /// <summary>
        /// Creates a shallow copy of the current object.
        /// </summary>
        public StoreCustomer ShallowCopy()
        {
            return (StoreCustomer)this.MemberwiseClone();
        }
    }
}
