﻿namespace SaudiSoft.Core
{
    public enum PaymentMethod
    {
        Cash,
        CreditCard,
        PhonePay,
        GiftCard
    }
}
