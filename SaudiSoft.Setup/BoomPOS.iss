#define MyAppName "BoomPOS"
#define MyAppVersion "1.8.1"
#define MyAppPublisher "Eng Techno"
#define MyAppURL "http://engtechnos.com/"
#define MyAppExeName "BoomPOS.exe"
#define MyAppSetupName = "BoomPOS_Setup"
#define MyAppSerialKey = "93bc-95dd-41b7-bb66-b2e0"

[Setup]
; NOTE: The value of AppId uniquely identifies this application. Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{ECC03655-7A9F-4A46-8A95-739D7692BD2B}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={autopf}\{#MyAppName}
DisableProgramGroupPage=yes
PrivilegesRequired=admin
VersionInfoCompany={#MyAppPublisher}
VersionInfoVersion= {#MyAppVersion}
;PrivilegesRequiredOverridesAllowed=dialog
OutputDir=.\bin\Publish\
OutputBaseFilename={#MyAppSetupName}
Compression=lzma
SolidCompression=yes
WizardStyle=modern
UninstallDisplayIcon={app}\BoomPOS.exe
WizardImageFile=.\Assets\installation_banner.bmp
WizardSmallImageFile=.\Assets\installation_small.bmp
DisableWelcomePage=no
UserInfoPage=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Messages]
WelcomeLabel2=This will install [name/ver] on your computer.%n%nIt is recommended that you read every step in the installation process before continuing.

[Files]
Source: "..\BoomPOS.WinApp\bin\Publish\netcoreapp3.1\BoomPOS.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\BoomPOS.WinApp\bin\Publish\netcoreapp3.1\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Registry]
Root: HKA; Subkey: "Software\Eng Techno"; Flags: uninsdeletekeyifempty
Root: HKA; Subkey: "Software\Eng Techno\BoomPOS"; Flags: uninsdeletekey; ValueType: string; ValueName: "Serial"; ValueData: {#MyAppSerialKey}

[Code]
function CheckSerial(Serial: String): Boolean;
begin
Serial := Trim(Serial);
if Serial = '{#MyAppSerialKey}' then
  result := true;
end;

[Icons]
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

