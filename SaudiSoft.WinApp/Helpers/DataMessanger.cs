﻿using SaudiSoft.Core;
using System;

namespace SaudiSoft.WinApp
{
    /// <summary>
    /// Data messanger between ViewModels Using Custom Event Aggregator.
    /// </summary>
    public static class DataMessanger
    {
        /// <summary>
        /// Broadcasts a Message.
        /// </summary>
        /// <param name="message">A string message to be sent on broadcast.</param>
        /// <param name="payload">An optional payload object to be sent on broadcast.</param>
        public static void BroadCast(string message, object payload = null)
        {
            OnMessageTransmitted?.Invoke(message, payload);
        }

        /// <summary>
        /// OnMessageTransmission Event.
        /// </summary>
        public static Action<string,object> OnMessageTransmitted;

        /// <summary>
        /// Broadcasts a Message.
        /// </summary>
        /// <param name="message">A string message to be sent on broadcast.</param>
        /// <param name="payload">An optional payload object to be sent on broadcast.</param>
        public static void UpdateShift(double value)
        {
            OnShiftDataTransmitted?.Invoke(value);
        }

        /// <summary>
        /// OnMessageTransmission Event.
        /// </summary>
        public static Action<double> OnShiftDataTransmitted;

        /// <summary>
        /// Broadcasts language change event to multiple parts of the application.
        /// </summary>
        /// <param name="language">Optional field that contains the name of the language.</param>
        public static void LanguageSwitch(string language)
        {
            OnLanguageSwitched?.Invoke(language);
        }

        /// <summary>
        /// OnLanguageSwitched Event.
        /// </summary>
        public static Action<string> OnLanguageSwitched;

        /// <summary>
        /// Broadcasts a user change event on login/logout.
        /// </summary>
        /// <param name="systemUser">Hold User Data Payload.</param>
        /// <param name="systemRole">Hold Role Data Payload.</param>
        public static void UserSwitch(string message, SystemUser systemUser, SystemRole systemRole)
        {
            OnUserSwitched?.Invoke(message, systemUser, systemRole);
        }

        /// <summary>
        /// OnUserSwitched Event.
        /// </summary>
        public static Action<string, SystemUser, SystemRole> OnUserSwitched;
    }
}
