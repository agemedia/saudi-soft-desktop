﻿using SaudiSoft.Core;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace SaudiSoft.WinApp
{
    public static class ExcelExporter
    {
        #region Private Static Methods
        /// <summary>
        /// Setups a SaveFileDialog to get a location from the user to export to.
        /// </summary>
        /// <param name="fileName">Default Export File Name</param>
        /// <returns>A SaveFileDialog Ready for Excel Export</returns>
        private static Microsoft.Win32.SaveFileDialog SetupExportDialog(string fileName)
        { 
            var dialog = new Microsoft.Win32.SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "Excel File (*.xlsx, *.xls) | *.xlsx; *.xls",
                FileName = fileName,
            };
            return dialog;
        }
        #endregion

        #region Public Static Export Methods
        /// <summary>
        /// Exports List of SalesTickets Into An Excel File.
        /// </summary>
        /// <param name="salesTickets">Sales Tickets Being Exported.</param>
        /// <param name="storeCustomers">All Store Customers</param>
        /// <param name="systemUsers">All System Users</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportSales(ICollection<SalesTicket> salesTickets, ICollection<StoreCustomer> storeCustomers, ICollection<SystemUser> systemUsers)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_Sales_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 2;
                var paymethods = (string[])Application.Current.FindResource("SalesPaymentMethod");

                // Sets up excel table headers.
                worksheet.Cell(1, 1).Value = "#";
                worksheet.Cell(1, 2).Value = (string)Application.Current.FindResource("DBAttributeDate");
                worksheet.Cell(1, 3).Value = (string)Application.Current.FindResource("DBAttributeDiscount");
                worksheet.Cell(1, 4).Value = (string)Application.Current.FindResource("DBAttributeAmount");
                worksheet.Cell(1, 5).Value = (string)Application.Current.FindResource("DBAttributePaidAmount");
                worksheet.Cell(1, 6).Value = (string)Application.Current.FindResource("DBAttributeRemaining");
                worksheet.Cell(1, 7).Value = (string)Application.Current.FindResource("SalesPaymentTitle12");
                worksheet.Cell(1, 8).Value = (string)Application.Current.FindResource("DBAttributeCustomer");
                worksheet.Cell(1, 9).Value = (string)Application.Current.FindResource("DBAttributeCashier");
                // Loads up sales entries into the table.
                foreach (var entry in salesTickets)
                {
                    worksheet.Cell(i, 1).Value = entry.ID;
                    worksheet.Cell(i, 2).Value = entry.SaleDate.ToString();
                    worksheet.Cell(i, 3).Value = entry.Discount;
                    worksheet.Cell(i, 4).Value = entry.CalculatedTotalPrice;
                    worksheet.Cell(i, 5).Value = entry.TotalPaid;
                    worksheet.Cell(i, 6).Value = entry.TotalPaid - entry.CalculatedTotalPrice;
                    worksheet.Cell(i, 7).Value = paymethods[(int)entry.PayMethod];
                    var entryCustomer = storeCustomers.FirstOrDefault(x => x.ID == entry.CustomerID) ?? new StoreCustomer { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 8).Value = entryCustomer.Name;
                    var entryUser = systemUsers.FirstOrDefault(x => x.ID == entry.SoldBy) ?? new SystemUser { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 9).Value = entryUser.Name;
                    i++;
                }

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }

        /// <summary>
        /// Exports List of StoreCustomers Into An Excel File.
        /// </summary>
        /// <param name="storeCustomers">Store Customers Being Exported.</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportCustomers(ICollection<StoreCustomer> storeCustomers)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_Customers_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 2;

                // Sets up excel table headers.
                worksheet.Cell(1, 1).Value = "#";
                worksheet.Cell(1, 2).Value = (string)Application.Current.FindResource("DBAttributeName");
                worksheet.Cell(1, 3).Value = (string)Application.Current.FindResource("DBAttributeEmail");
                worksheet.Cell(1, 4).Value = (string)Application.Current.FindResource("DBAttributeMobile");
                worksheet.Cell(1, 5).Value = (string)Application.Current.FindResource("DBAttributePhone");
                worksheet.Cell(1, 6).Value = (string)Application.Current.FindResource("DBAttributeCountry");
                worksheet.Cell(1, 7).Value = (string)Application.Current.FindResource("DBAttributeState");
                worksheet.Cell(1, 8).Value = (string)Application.Current.FindResource("DBAttributeCity");
                worksheet.Cell(1, 9).Value = (string)Application.Current.FindResource("DBAttributeAddress");
                worksheet.Cell(1, 10).Value = (string)Application.Current.FindResource("DBAttributeAmountDue");
                worksheet.Cell(1, 11).Value = (string)Application.Current.FindResource("DBAttributePaidAmount");
                worksheet.Cell(1, 12).Value = (string)Application.Current.FindResource("DBAttributeRemaining");

                // Loads up customer entries into the table.
                foreach (var entry in storeCustomers)
                {
                    if (entry.ID != 0)
                    {
                        worksheet.Cell(i, 1).Value = entry.ID;
                        worksheet.Cell(i, 2).Value = entry.Name;
                        worksheet.Cell(i, 3).Value = entry.Email;
                        worksheet.Cell(i, 4).Value = entry.Mobile;
                        worksheet.Cell(i, 5).Value = entry.Phone;
                        worksheet.Cell(i, 6).Value = entry.Country;
                        worksheet.Cell(i, 7).Value = entry.State;
                        worksheet.Cell(i, 8).Value = entry.City;
                        worksheet.Cell(i, 9).Value = entry.Address;
                        worksheet.Cell(i, 10).Value = entry.PurchaseDue;
                        worksheet.Cell(i, 11).Value = entry.PaidPurchaseDue;
                        worksheet.Cell(i, 12).Value = entry.PaidPurchaseDue - entry.PurchaseDue;
                        i++;
                    }
                }

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }

        /// <summary>
        /// Exports List of PurchasesTickets Into An Excel File.
        /// </summary>
        /// <param name="purchaseTickets">Purchases Tickets Being Exported.</param>
        /// <param name="stockSuppliers">All Stock Suppliers</param>
        /// <param name="systemUsers">All System Users</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportPurchases(ICollection<PurchaseTicket> purchaseTickets, ICollection<StockSupplier> stockSuppliers, ICollection<SystemUser> systemUsers)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_Purchases_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 2;
                var paymethods = (string[])Application.Current.FindResource("SalesPaymentMethod");

                // Sets up excel table headers.
                worksheet.Cell(1, 1).Value = "#";
                worksheet.Cell(1, 2).Value = (string)Application.Current.FindResource("DBAttributeDate");
                worksheet.Cell(1, 3).Value = (string)Application.Current.FindResource("DBAttributeDiscount");
                worksheet.Cell(1, 4).Value = (string)Application.Current.FindResource("DBAttributeAmount");
                worksheet.Cell(1, 5).Value = (string)Application.Current.FindResource("DBAttributePaidAmount");
                worksheet.Cell(1, 6).Value = (string)Application.Current.FindResource("DBAttributeRemaining");
                worksheet.Cell(1, 7).Value = (string)Application.Current.FindResource("SalesPaymentTitle12");
                worksheet.Cell(1, 8).Value = (string)Application.Current.FindResource("DBAttributeSupplier");
                worksheet.Cell(1, 9).Value = (string)Application.Current.FindResource("DBAttributeCashier");
                // Loads up purchase entries into the table.
                foreach (var entry in purchaseTickets)
                { 
                    worksheet.Cell(i, 1).Value = entry.ID;
                    worksheet.Cell(i, 2).Value = entry.PurchaseDate.ToString();
                    worksheet.Cell(i, 3).Value = entry.Discount;
                    worksheet.Cell(i, 4).Value = entry.CalculatedTotalPrice;
                    worksheet.Cell(i, 5).Value = entry.TotalPaid;
                    worksheet.Cell(i, 6).Value = entry.TotalPaid - entry.CalculatedTotalPrice;
                    worksheet.Cell(i, 7).Value = paymethods[(int)entry.PayMethod];
                    var entryCustomer = stockSuppliers.FirstOrDefault(x => x.ID == entry.StockSupplierID) ?? new StockSupplier { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 8).Value = entryCustomer.Name;
                    var entryUser = systemUsers.FirstOrDefault(x => x.ID == entry.BoughtBy) ?? new SystemUser { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 9).Value = entryUser.Name;
                    i++;
                }

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }

        /// <summary>
        /// Exports List of StoreSuppliers Into An Excel File.
        /// </summary>
        /// <param name="stockSuppliers">Stock Supplier Being Exported.</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportSuppliers(ICollection<StockSupplier> stockSuppliers)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_Suppliers_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 2;

                // Sets up excel table headers.
                worksheet.Cell(1, 1).Value = "#";
                worksheet.Cell(1, 2).Value = (string)Application.Current.FindResource("DBAttributeName");
                worksheet.Cell(1, 3).Value = (string)Application.Current.FindResource("DBAttributeEmail");
                worksheet.Cell(1, 4).Value = (string)Application.Current.FindResource("DBAttributeMobile");
                worksheet.Cell(1, 5).Value = (string)Application.Current.FindResource("DBAttributePhone");
                worksheet.Cell(1, 6).Value = (string)Application.Current.FindResource("DBAttributeGSTNumber");
                worksheet.Cell(1, 7).Value = (string)Application.Current.FindResource("DBAttributeTAXNumber");
                worksheet.Cell(1, 8).Value = (string)Application.Current.FindResource("DBAttributeCountry");
                worksheet.Cell(1, 9).Value = (string)Application.Current.FindResource("DBAttributeState");
                worksheet.Cell(1, 10).Value = (string)Application.Current.FindResource("DBAttributeCity");
                worksheet.Cell(1, 11).Value = (string)Application.Current.FindResource("DBAttributeAddress");
                worksheet.Cell(1, 12).Value = (string)Application.Current.FindResource("DBAttributeAmountDue");
                worksheet.Cell(1, 13).Value = (string)Application.Current.FindResource("DBAttributePaidAmount");
                worksheet.Cell(1, 14).Value = (string)Application.Current.FindResource("DBAttributeRemaining");
                // Loads up supplier entries into the table.
                foreach (var entry in stockSuppliers)
                {
                    if (entry.ID != 0)
                    {
                        worksheet.Cell(i, 1).Value = entry.ID;
                        worksheet.Cell(i, 2).Value = entry.Name;
                        worksheet.Cell(i, 3).Value = entry.Email;
                        worksheet.Cell(i, 4).Value = entry.Mobile;
                        worksheet.Cell(i, 5).Value = entry.Phone;
                        worksheet.Cell(i, 6).Value = entry.GSTNumber;
                        worksheet.Cell(i, 7).Value = entry.TAXNumber;
                        worksheet.Cell(i, 8).Value = entry.Country;
                        worksheet.Cell(i, 9).Value = entry.State;
                        worksheet.Cell(i, 10).Value = entry.City;
                        worksheet.Cell(i, 11).Value = entry.Address;
                        worksheet.Cell(i, 12).Value = entry.PurchaseDue;
                        worksheet.Cell(i, 13).Value = entry.PaidPurchaseDue;
                        worksheet.Cell(i, 14).Value = entry.PaidPurchaseDue - entry.PurchaseDue;
                        i++;
                    }
                }

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }

        /// <summary>
        /// Exports List of StockItems Into An Excel File.
        /// </summary>
        /// <param name="stockItems">Stock Items Being Exported.</param>
        /// <param name="stockCategories">All Stock Categories</param>
        /// <param name="stockBrands">All Stock Brands</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportItems(ICollection<StockItem> stockItems, ICollection<StockCategory> stockCategories, ICollection<StockBrand> stockBrands)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_Items_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 2;
                var units = (string[])Application.Current.FindResource("ItemsUnitList");

                // Sets up excel table headers.
                worksheet.Cell(1, 1).Value = "#";
                worksheet.Cell(1, 2).Value = (string)Application.Current.FindResource("DBAttributeName");
                worksheet.Cell(1, 3).Value = (string)Application.Current.FindResource("DBAttributeBarcode");
                worksheet.Cell(1, 4).Value = (string)Application.Current.FindResource("DBAttributeBrand");
                worksheet.Cell(1, 5).Value = (string)Application.Current.FindResource("DBAttributeCategory");
                worksheet.Cell(1, 6).Value = (string)Application.Current.FindResource("DBAttributeQuantity");
                worksheet.Cell(1, 7).Value = (string)Application.Current.FindResource("DBAttributeUnit");
                worksheet.Cell(1, 8).Value = (string)Application.Current.FindResource("DBAttributeAlertQuantity");
                worksheet.Cell(1, 9).Value = (string)Application.Current.FindResource("DBAttributeExpiryDate");
                worksheet.Cell(1, 10).Value = (string)Application.Current.FindResource("DBAttributeSupplierPrice");
                worksheet.Cell(1, 11).Value = (string)Application.Current.FindResource("DBAttributeTax");
                worksheet.Cell(1, 12).Value = (string)Application.Current.FindResource("DBAttributeSalesPrice");
                // Loads up purchase entries into the table.
                foreach (var entry in stockItems)
                {
                    worksheet.Cell(i, 1).Value = entry.ID;
                    worksheet.Cell(i, 2).Value = entry.Name;
                    worksheet.Cell(i, 3).Value = entry.Barcode;
                    var entryBrand = stockBrands.FirstOrDefault(x => x.ID == entry.BrandID) ?? new StockBrand { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 4).Value = entryBrand.Name;
                    var entryCategory = stockCategories.FirstOrDefault(x => x.ID == entry.BrandID) ?? new StockCategory { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 5).Value = entryCategory.Name;
                    worksheet.Cell(i, 6).Value = entry.Quantity;
                    worksheet.Cell(i, 7).Value = units[entry.Unit];
                    worksheet.Cell(i, 8).Value = entry.AlertQuantity;
                    worksheet.Cell(i, 9).Value = entry.ExpiryDate;
                    worksheet.Cell(i, 10).Value = entry.SupplierPrice;
                    worksheet.Cell(i, 11).Value = entry.SupplierPrice * (1 + (entry.Tax / 100));
                    worksheet.Cell(i, 12).Value = entry.SalesPrice;
                    i++;
                }

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }

        /// <summary>
        /// Exports List of StoreBrands Into An Excel File.
        /// </summary>
        /// <param name="stockBrands">Stock Brands Being Exported.</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportBrands(ICollection<StockBrand> stockBrands)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_Brands_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 2;

                // Sets up excel table headers.
                worksheet.Cell(1, 1).Value = "#";
                worksheet.Cell(1, 2).Value = (string)Application.Current.FindResource("DBAttributeName");
                worksheet.Cell(1, 3).Value = (string)Application.Current.FindResource("DBAttributeCode");
                worksheet.Cell(1, 4).Value = (string)Application.Current.FindResource("DBAttributeDescription");
                // Loads up supplier entries into the table.
                foreach (var entry in stockBrands)
                {
                    if (entry.ID != 0)
                    {
                        worksheet.Cell(i, 1).Value = entry.ID;
                        worksheet.Cell(i, 2).Value = entry.Name;
                        worksheet.Cell(i, 3).Value = entry.Code;
                        worksheet.Cell(i, 4).Value = entry.Description;
                        i++;
                    }
                }

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }

        /// <summary>
        /// Exports List of StoreCategories Into An Excel File.
        /// </summary>
        /// <param name="stockCategories">Stock Categories Being Exported.</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportCategories(ICollection<StockCategory> stockCategories)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_Categories_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 2;

                // Sets up excel table headers.
                worksheet.Cell(1, 1).Value = "#";
                worksheet.Cell(1, 2).Value = (string)Application.Current.FindResource("DBAttributeName");
                worksheet.Cell(1, 3).Value = (string)Application.Current.FindResource("DBAttributeCode");
                worksheet.Cell(1, 4).Value = (string)Application.Current.FindResource("DBAttributeDescription");
                // Loads up supplier entries into the table.
                foreach (var entry in stockCategories)
                {
                    if (entry.ID != 0)
                    {
                        worksheet.Cell(i, 1).Value = entry.ID;
                        worksheet.Cell(i, 2).Value = entry.Name;
                        worksheet.Cell(i, 3).Value = entry.Code;
                        worksheet.Cell(i, 4).Value = entry.Description;
                        i++;
                    }
                }

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }


        /// <summary>
        /// Exports List of StoreExpenses Into An Excel File.
        /// </summary>
        /// <param name="storeExpenses">Stock Expenses Being Exported.</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportExpenses(ICollection<StoreExpense> storeExpenses)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_Expenses_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 2;

                // Sets up excel table headers.
                worksheet.Cell(1, 1).Value = "#";
                worksheet.Cell(1, 2).Value = (string)Application.Current.FindResource("DBAttributeDate");
                worksheet.Cell(1, 3).Value = (string)Application.Current.FindResource("DBAttributeName");
                worksheet.Cell(1, 4).Value = (string)Application.Current.FindResource("DBAttributeDescription");
                worksheet.Cell(1, 5).Value = (string)Application.Current.FindResource("DBAttributeAmount");
                // Loads up supplier entries into the table.
                foreach (var entry in storeExpenses)
                {
                    if (entry.ID != 0)
                    {
                        worksheet.Cell(i, 1).Value = entry.ID;
                        worksheet.Cell(i, 2).Value = entry.ExpenseDate;
                        worksheet.Cell(i, 3).Value = entry.Name;
                        worksheet.Cell(i, 4).Value = entry.Description;
                        worksheet.Cell(i, 5).Value = entry.Amount;
                        i++;
                    }
                }

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }

        /// <summary>
        /// Exports A Financial Report Into An Excel File.
        /// </summary>
        /// <param name="financialReport">Financial Report Being Exported.</param>
        /// <param name="stockItems">Stock Item Names To Use For Exporting.</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportFinancialReport(FinancialReport financialReport, ICollection<StockItem> stockItems)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_Financial_Report_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 3;

                // Sets up Table Title
                worksheet.Cell(1, 1).Value = (string)Application.Current.FindResource("ProfitReportsTitle1");
                worksheet.Cell(1, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 5)).Merge();
                // Sets up excel table headers.
                worksheet.Cell(2, 1).Value = (string)Application.Current.FindResource("DBAttributeName");
                worksheet.Cell(2, 2).Value = (string)Application.Current.FindResource("DBAttributeQuantity");
                worksheet.Cell(2, 3).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle1");
                worksheet.Cell(2, 4).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle2");
                worksheet.Cell(2, 5).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle3");
                // Loads up supplier entries into the table.
                foreach (var entry in financialReport.SalesItems)
                {
                    var entryItem = stockItems.FirstOrDefault(x => x.ID == entry.StockItemID) ?? new StockItem { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 1).Value = entryItem.Name;
                    worksheet.Cell(i, 2).Value = entry.Quantity;
                    worksheet.Cell(i, 3).Value = entry.Price;
                    worksheet.Cell(i, 4).Value = entry.PaidPrice;
                    worksheet.Cell(i, 5).Value = entry.Price - entry.PaidPrice;
                    i++;
                }

                i = 3;
                // Sets up Table Title
                worksheet.Cell(1, 7).Value = (string)Application.Current.FindResource("ProfitReportsTitle2");
                worksheet.Cell(1, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(1, 7), worksheet.Cell(1, 11)).Merge();
                // Sets up excel table headers.
                worksheet.Cell(2, 7).Value = (string)Application.Current.FindResource("DBAttributeName");
                worksheet.Cell(2, 8).Value = (string)Application.Current.FindResource("DBAttributeQuantity");
                worksheet.Cell(2, 9).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle1");
                worksheet.Cell(2, 10).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle2");
                worksheet.Cell(2, 11).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle3");
                // Loads up supplier entries into the table.
                foreach (var entry in financialReport.PurchaseItems)
                {
                    var entryItem = stockItems.FirstOrDefault(x => x.ID == entry.StockItemID) ?? new StockItem { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 7).Value = entryItem.Name;
                    worksheet.Cell(i, 8).Value = entry.Quantity;
                    worksheet.Cell(i, 9).Value = entry.Price;
                    worksheet.Cell(i, 10).Value = entry.PaidPrice;
                    worksheet.Cell(i, 11).Value = entry.Price - entry.PaidPrice;
                    i++;
                }

                i = 3;
                // Sets up Table Title
                worksheet.Cell(1, 13).Value = (string)Application.Current.FindResource("ProfitReportsTitle3");
                worksheet.Cell(1, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(1, 13), worksheet.Cell(1, 16)).Merge();
                // Sets up excel table headers.
                worksheet.Cell(2, 13).Value = (string)Application.Current.FindResource("DBAttributeDate");
                worksheet.Cell(2, 14).Value = (string)Application.Current.FindResource("DBAttributeName");
                worksheet.Cell(2, 15).Value = (string)Application.Current.FindResource("DBAttributeDescription");
                worksheet.Cell(2, 16).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle1");
                // Loads up supplier entries into the table.
                foreach (var entry in financialReport.StoreExpenses)
                {
                    worksheet.Cell(i, 13).Value = entry.ExpenseDate;
                    worksheet.Cell(i, 14).Value = entry.Name;
                    worksheet.Cell(i, 15).Value = entry.Description;
                    worksheet.Cell(i, 16).Value = entry.Amount;
                    i++;
                }

                worksheet.Cell(2, 19).Value = (string)Application.Current.FindResource("WordFrom");
                worksheet.Cell(3, 19).Value = (string)Application.Current.FindResource("WordTo");
                worksheet.Cell(2, 20).Value = financialReport.FilterFromDate;
                worksheet.Cell(3, 20).Value = financialReport.FilterToDate;

                worksheet.Cell(4, 18).Value = (string)Application.Current.FindResource("ProfitReportsTitle1");
                worksheet.Cell(4, 19).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle1");
                worksheet.Cell(5, 19).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle2");
                worksheet.Cell(6, 19).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle3");
                worksheet.Cell(4, 20).Value = financialReport.TotalSales;
                worksheet.Cell(5, 20).Value = financialReport.TotalSalesPaid;
                worksheet.Cell(6, 20).Value = financialReport.TotalSalesDue;

                worksheet.Cell(7, 18).Value = (string)Application.Current.FindResource("ProfitReportsTitle2");
                worksheet.Cell(7, 19).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle1");
                worksheet.Cell(8, 19).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle2");
                worksheet.Cell(9, 19).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle3");
                worksheet.Cell(7, 20).Value = financialReport.TotalPurchases;
                worksheet.Cell(8, 20).Value = financialReport.TotalPurchasesPaid;
                worksheet.Cell(9, 20).Value = financialReport.TotalPurchasesDue;

                worksheet.Cell(10, 18).Value = (string)Application.Current.FindResource("ProfitReportsTitle3");
                worksheet.Cell(10, 19).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle1");
                worksheet.Cell(10, 20).Value = financialReport.TotalExpenses;

                worksheet.Cell(11, 18).Value = (string)Application.Current.FindResource("ProfitReportsTitle4");
                worksheet.Cell(11, 19).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle4");
                worksheet.Cell(12, 19).Value = (string)Application.Current.FindResource("ProfitReportsSubTitle5");
                worksheet.Cell(11, 20).Value = financialReport.TotalGrossProfit;
                worksheet.Cell(12, 20).Value = financialReport.TotalNetProfit;

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }

        /// <summary>
        /// Exports List of ItemReport Into An Excel File.
        /// </summary>
        /// <param name="itemReport">Data About Selected Stock Item</param>
        /// <param name="stockItem">Selected Stock Item</param>
        /// <param name="storeCustomers">All of the Customers</param>
        /// <param name="stockSuppliers">All of the Suppliers</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportItemReport(ItemReport itemReport, StockItem stockItem, ICollection<StoreCustomer> storeCustomers, ICollection<StockSupplier> stockSuppliers)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_ItemReport_" + stockItem.ID + "_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 3;
                // Sets up Table Title
                worksheet.Cell(1, 1).Value = (string)Application.Current.FindResource("ItemReportsTitle2");
                worksheet.Cell(1, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 5)).Merge();
                // Sets up excel table headers.
                worksheet.Cell(2, 1).Value = (string)Application.Current.FindResource("DBAttributeDate");
                worksheet.Cell(2, 2).Value = (string)Application.Current.FindResource("DBAttributeQuantity");
                worksheet.Cell(2, 3).Value = (string)Application.Current.FindResource("DBAttributePrice");
                worksheet.Cell(2, 4).Value = (string)Application.Current.FindResource("DBAttributeTotal");
                worksheet.Cell(2, 5).Value = (string)Application.Current.FindResource("DBAttributeCustomer");
                // Loads up supplier entries into the table.
                foreach (var entry in itemReport.SalesTransactions)
                {
                    worksheet.Cell(i, 1).Value = entry.Date;
                    worksheet.Cell(i, 2).Value = entry.Quantity;
                    worksheet.Cell(i, 3).Value = entry.Price;
                    worksheet.Cell(i, 4).Value = entry.TotalPrice;
                    var entryCustomer = storeCustomers.FirstOrDefault(x => x.ID == entry.RecipientID) ?? new StoreCustomer { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 5).Value = entryCustomer.Name;
                    i++;
                }

                i = 3;
                // Sets up Table Title
                worksheet.Cell(1, 7).Value = (string)Application.Current.FindResource("ItemReportsTitle3");
                worksheet.Cell(1, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(1, 7), worksheet.Cell(1, 11)).Merge();
                // Sets up excel table headers.
                worksheet.Cell(2, 7).Value = (string)Application.Current.FindResource("DBAttributeDate");
                worksheet.Cell(2, 8).Value = (string)Application.Current.FindResource("DBAttributeQuantity");
                worksheet.Cell(2, 9).Value = (string)Application.Current.FindResource("DBAttributePrice");
                worksheet.Cell(2, 10).Value = (string)Application.Current.FindResource("DBAttributeTotal");
                worksheet.Cell(2, 11).Value = (string)Application.Current.FindResource("DBAttributeSupplier");
                // Loads up supplier entries into the table.
                foreach (var entry in itemReport.PurchaseTransactions)
                {
                    worksheet.Cell(i, 7).Value = entry.Date;
                    worksheet.Cell(i, 8).Value = entry.Quantity;
                    worksheet.Cell(i, 9).Value = entry.Price;
                    worksheet.Cell(i, 10).Value = entry.TotalPrice;
                    var entrySupplier = stockSuppliers.FirstOrDefault(x => x.ID == entry.RecipientID) ?? new StockSupplier { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 11).Value = entrySupplier.Name;
                    i++;
                }

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }

        /// <summary>
        /// Exports List of StoreExpenses Into An Excel File.
        /// </summary>
        /// <param name="storeExpenses">Stock Expenses Being Exported.</param>
        /// <returns>Bool indicating whether or not the operation was successful.</returns>
        public static bool ExportShifts(ICollection<StoreShift> storeShifts, ICollection<SystemUser> systemUsers)
        {
            // Opens a dialog to get a location from selected by the user, it saves data to an excel file (Using Excel Interop).
            var dialog = SetupExportDialog("POS_Shifts_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            if (dialog.ShowDialog() == true)
            {
                // Creates an Workbook instance to use to save the data.
                var workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add(DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                var i = 2;

                // Sets up excel table headers.
                worksheet.Cell(1, 1).Value = "#";
                worksheet.Cell(1, 2).Value = (string)Application.Current.FindResource("ShiftsSubTitle10");
                worksheet.Cell(1, 3).Value = (string)Application.Current.FindResource("ShiftsSubTitle11");
                worksheet.Cell(1, 4).Value = (string)Application.Current.FindResource("ShiftsSubTitle7");
                worksheet.Cell(1, 5).Value = (string)Application.Current.FindResource("ShiftsSubTitle8");
                worksheet.Cell(1, 6).Value = (string)Application.Current.FindResource("ShiftsSubTitle9");
                worksheet.Cell(1, 7).Value = (string)Application.Current.FindResource("ShiftsSubTitle12");
                worksheet.Cell(1, 8).Value = (string)Application.Current.FindResource("DBAttributeUser");
                // Loads up supplier entries into the table.
                foreach (var entry in storeShifts)
                {
                    worksheet.Cell(i, 1).Value = entry.ID;
                    worksheet.Cell(i, 2).Value = entry.StartDate;
                    worksheet.Cell(i, 3).Value = entry.EndDate;
                    worksheet.Cell(i, 4).Value = entry.CashIn;
                    worksheet.Cell(i, 5).Value = entry.CashOut;
                    worksheet.Cell(i, 6).Value = entry.CashOut - entry.CashIn;
                    worksheet.Cell(i, 7).Value = entry.Notes;
                    var entryUser = systemUsers.FirstOrDefault(x => x.ID == entry.UserID) ?? new SystemUser { Name = (string)Application.Current.FindResource("WordUnknown") };
                    worksheet.Cell(i, 8).Value = entryUser.Name;
                    i++;
                }

                // Auto Sizes Columns and Saves Export File, then disposes of the data.
                worksheet.Columns().AdjustToContents();
                workbook.SaveAs(dialog.FileName.ToString());
                workbook.Dispose();

                // Indicates that the export was successfully complete.
                return true;
            }

            // Indicates that the export was cancelled before it began/successful.
            return false;
        }        
        #endregion
    }
}
