﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace SaudiSoft.WinApp
{
    public class NullImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                switch(parameter as string)
                {
                    case "User":
                        return (BitmapImage)Application.Current.FindResource("Avatar");
                    case "Holder":
                        return (BitmapImage)Application.Current.FindResource("Placeholder");
                    default:
                        return DependencyProperty.UnsetValue;
                }
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
