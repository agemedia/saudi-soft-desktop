﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SaudiSoft.WinApp
{
    public class IndexBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && parameter is int[])
            {
                foreach (var entry in parameter as int[])
                {
                    if ( (int)value == entry ) return true;
                }
                return false;
            }
            else if (value != null && parameter != null)
            {
                return value.ToString() == parameter.ToString() ? true : false;
            }
            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
