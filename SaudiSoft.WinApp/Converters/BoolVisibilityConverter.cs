﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SaudiSoft.WinApp
{
    class BoolVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool) 
            { 
                return ((bool)value == true) ? Visibility.Visible : Visibility.Collapsed; 
            }
            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
