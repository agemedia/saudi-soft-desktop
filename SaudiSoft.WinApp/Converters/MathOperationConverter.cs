﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SaudiSoft.WinApp
{
    public class MathOperationConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] is double && values[1] is double)
            {
                switch ((string)parameter)
                {
                    case "Add":
                        return ((double)values[0] - (double)values[1]).ToString("F");
                    case "DiscountedValueWithTax":
                        return (((double)values[0] * (1 - ((double)values[1] / 100)))+((double)values[0]* ((double)values[2]/100))).ToString("F");
                    case "ChargePaymentDiscountedValueWithTax":
                        return ((((double)values[0] * (1 - ((double)values[1] / 100)))+((double)values[0]* ((double)values[2] / 100)))- (double)values[3]).ToString("F");
                    case "Subtract":
                        return ((double)values[0] - (double)values[1]).ToString("F");
                    case "Multiply":
                        return ((double)values[0] * (double)values[1]).ToString("F");
                    case "Divide":
                        return ((double)values[0] / (double)values[1]).ToString("F");
                    case "TaxedValue":
                        return ((double)values[0] * (1 + ((double)values[1] / 100))).ToString("F");
                    case "DiscountedValue":
                        return ((double)values[0] * (1 - ((double)values[1] / 100))).ToString("F");
                    case "PaymentChange":
                        return ((double)values[1] - (double)values[0]).ToString("F");
                }
            }
            return DependencyProperty.UnsetValue;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new[] { DependencyProperty.UnsetValue };
        }
    }
}
