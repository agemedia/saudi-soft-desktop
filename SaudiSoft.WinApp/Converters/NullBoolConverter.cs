﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SaudiSoft.WinApp
{
    public class NullBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int && parameter == null)
            {
                return (int)value == 0 ? true : false;
            }
            else if (value is int && (string)parameter == "Inverse")
            {
                return (int)value != 0 ? true : false;
            }
            return (value == null)? true : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
