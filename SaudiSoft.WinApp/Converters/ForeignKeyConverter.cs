﻿using SaudiSoft.Core;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace SaudiSoft.WinApp
{
    public class ForeignKeyConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            // Checks if the foreign key is not null or equal to 0.
            if (values[0] is int || values[0] is PaymentMethod)
            {
                // Checks the collection depending on the type provided as a parameter.
                switch (parameter as string)
                {
                    case "Brand":
                        var brand = (values[1] as ObservableCollection<StockBrand>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new StockBrand { Name = (string)Application.Current.FindResource("WordUnknown") };
                        return brand.Name;
                    case "Category":
                        var category = (values[1] as ObservableCollection<StockCategory>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new StockCategory { Name = (string)Application.Current.FindResource("WordUnknown") };
                        return category.Name;
                    case "Item":
                        var item = (values[1] as ObservableCollection<StockItem>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new StockItem { Name = (string)Application.Current.FindResource("WordUnknown") };
                        return item.Name;
                    case "Supplier":
                        var supplier = (values[1] as ObservableCollection<StockSupplier>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new StockSupplier { Name = (string)Application.Current.FindResource("WordUnknown") };
                        return supplier.Name;
                    case "Customer":
                        var customer = (values[1] as ObservableCollection<StoreCustomer>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new StoreCustomer { Name = (string)Application.Current.FindResource("WordUnknown") };
                        return customer.Name;
                    case "User":
                        var user = (values[1] as ObservableCollection<SystemUser>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new SystemUser { Name = (string)Application.Current.FindResource("WordUnknown") };
                        return user.Name;
                    case "Role":
                        var role = (values[1] as ObservableCollection<SystemRole>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new SystemRole { Name = (string)Application.Current.FindResource("WordUnknown") };
                        return role.Name;
                    case "Unit":
                        var units = (string[])Application.Current.FindResource("ItemsUnitList");
                        return units[(int)values[0]];
                    case "PayMethod":
                        var payMethod = (string[])Application.Current.FindResource("SalesPaymentMethod");
                        return payMethod[(int)values[0]];
                    case "ItemQuantity":
                        var itemq = (values[1] as ObservableCollection<StockItem>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new StockItem();
                        return itemq.Quantity;
                    case "SalesQuantity":
                        var salesitem = (values[1] as ObservableCollection<SalesItem>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new SalesItem();
                        return salesitem.Quantity;
                    case "PurchaseQuantity":
                        var purchaseitem = (values[1] as ObservableCollection<PurchaseItem>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new PurchaseItem();
                        return purchaseitem.Quantity;
                    case "ItemUnit":
                        var itemu = (values[1] as ObservableCollection<StockItem>).FirstOrDefault(x => x.ID == (int)values[0]) ?? new StockItem();
                        var itemunits = (string[])Application.Current.FindResource("ItemsUnitList");
                        return itemunits[itemu.Unit];
                }
            }
            return DependencyProperty.UnsetValue;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new[] { DependencyProperty.UnsetValue };
        }
    }
}
