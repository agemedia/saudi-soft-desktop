﻿using SaudiSoft.Core;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using System;
using System.Windows;
using System.IO;

namespace SaudiSoft.WinApp
{
    public partial class App : Application
    {
        #region Private Services
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor for the application
        /// </summary>
        public App()
        {
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }
        #endregion

        #region UnhandledException Event
        /// <summary>
        /// An event that fires when there is a unhandled exception (its data is saved in a log file)
        /// </summary>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = (Exception)e.ExceptionObject;
            _logger.Error("UnhandledException caught : " + exception.Message);
            _logger.Error("UnhandledException StackTrace : " + exception.StackTrace);
            _logger.Fatal("Runtime terminating: {0}", e.IsTerminating);
            MessageBox.Show("Unexpected error has occured, the application will be close. Error details can be found in a newly created log file.", "SaudiSoft | Error Encountered", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        #endregion

        #region IoC Service Provider
        /// <summary>
        /// Creates ServiceProvider For Dependency Injection of the Various Elements
        /// </summary>
        /// <returns>Serviceprovider with application services implementations.</returns>
        private IServiceProvider CreateServiceProvider()
        {
            IServiceCollection services = new ServiceCollection();

            // Database access services.
            services.AddSingleton<IDataConnection, DataConnection>();
            services.AddSingleton<IDataService<SystemUser>, DataService<SystemUser>>();
            services.AddSingleton<IDataService<SystemPassword>, DataService<SystemPassword>>();
            services.AddSingleton<IDataService<SystemRole>, DataService<SystemRole>>();
            services.AddSingleton<IDataService<StockItem>, DataService<StockItem>>();
            services.AddSingleton<IDataService<StockBrand>, DataService<StockBrand>>();
            services.AddSingleton<IDataService<StockCategory>, DataService<StockCategory>>();
            services.AddSingleton<IDataService<StoreExpense>, DataService<StoreExpense>>();
            services.AddSingleton<IDataService<StoreShift>, DataService<StoreShift>>();
            services.AddSingleton<IDataService<PurchaseTicket>, DataService<PurchaseTicket>>();
            services.AddSingleton<IDataService<PurchaseItem>, DataService<PurchaseItem>>();
            services.AddSingleton<IDataService<StockSupplier>, DataService<StockSupplier>>();
            services.AddSingleton<IDataService<SalesTicket>, DataService<SalesTicket>>();
            services.AddSingleton<IDataService<SalesItem>, DataService<SalesItem>>();
            services.AddSingleton<IDataService<StoreCustomer>, DataService<StoreCustomer>>();

            // Printing Services
            services.AddSingleton<IPrintSalesService, PrintSalesService>();
            services.AddSingleton<IPrintPurchasesService, PrintPurchasesService>();
            services.AddSingleton<IPrintFinancialReportService, PrintFinancialReportService>();
            services.AddSingleton<IPrintItemReportService, PrintItemReportService>();

            // Other core services.
            services.AddSingleton<IHasherService, HasherService>();
            services.AddSingleton<IAuthService, AuthService>();
            services.AddSingleton<IDashboardService, DashboardService>();
            services.AddSingleton<IFinancialReportService, FinancialReportService>();
            services.AddSingleton<IItemReportService, ItemReportService>();
            services.AddSingleton<ICustomerPaymentService, CustomerPaymentService>();
            services.AddSingleton<ISupplierPaymentService, SupplierPaymentService>();
            services.AddSingleton<IValidateLicenceService, ValidateLicenceService>();

            // Application ViewModels
            services.AddScoped<DashboardViewModel>();
            services.AddScoped<SalesViewModel>();
            services.AddScoped<PurchaseViewModel>();
            services.AddScoped<ItemsViewModel>();
            services.AddScoped<ExpensesViewModel>();
            services.AddScoped<ReportsViewModel>();
            services.AddScoped<UsersViewModel>();
            services.AddScoped<ShiftsViewModel>();
            services.AddScoped<SettingsViewModel>();

            // Main Application Initalizers.
            services.AddScoped<WindowViewModel>();
            services.AddScoped<MainWindow>();

            return services.BuildServiceProvider();
        }
        #endregion

        #region Application Start up Sequence
        /// <summary>
        /// Custom Application Startup Sequence.
        /// </summary>
        protected override void OnStartup(StartupEventArgs e)
        {
            IServiceProvider provider = CreateServiceProvider();
           // IValidateLicenceService licenceService = provider.GetRequiredService<IValidateLicenceService>();
            MainWindow window = provider.GetRequiredService<MainWindow>();
            window.DataContext = provider.GetRequiredService<WindowViewModel>();
            window.Show();
            base.OnStartup(e);
            //bool isSetupValid = licenceService.ValidateRegistry();
            //if (!isSetupValid)
            //{
            //    MessageBox.Show("This application was not installed correctly via the installer.", "SaudiSoft | Invalid Installation", MessageBoxButton.OK, MessageBoxImage.Error);
            //    this.Shutdown();
            //}
            //else
            //{
            //    MainWindow window = provider.GetRequiredService<MainWindow>();
            //    window.DataContext = provider.GetRequiredService<WindowViewModel>();
            //    window.Show();
            //    base.OnStartup(e);
            //}
        }
        
        #endregion
    }
}