﻿using SaudiSoft.Core;
using LiveCharts;
using LiveCharts.Wpf;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SaudiSoft.WinApp
{
    public class ReportsViewModel : ViewModelBase
    {
        #region Private Services
        private readonly IFinancialReportService _financialReportService;
        private readonly IItemReportService _itemReportService;
        private readonly IPrintFinancialReportService _printFinancialReportService;
        private readonly IPrintItemReportService _printItemReportService;
        #endregion

        #region Private Proprties
        /// <summary>
        /// Holds selected Tab Name.
        /// </summary>
        private string _selectedTab = "Financial";
        #endregion

        #region Public Properties
        /// <summary>
        /// Holds ContentArea's Current Selected Page Number.
        /// </summary>
        public int ContentAreaTab { get; set; } = 0;

        /// <summary>
        /// Holds Items Page Main Title.
        /// </summary>
        public string MainPageTitle { get; set; } = (string)Application.Current.FindResource("ReportsTitle1");

        /// <summary>
        /// Holds Items Page Secondary Title.
        /// </summary>
        public string SecPageTitle { get; set; } = (string)Application.Current.FindResource("ReportsSubTitle1");

        /// <summary>
        /// Holds status for Item Report Progress Bar.
        /// </summary>
        public bool ItemReportStatus { get; set; } = false;

        /// <summary>
        /// Holds status for Item Report Progress Bar.
        /// </summary>
        public bool FinancialReportStatus { get; set; } = false;

        /// <summary>
        /// Holds status for Excel Export Progress Bar.
        /// </summary>
        public bool ExcelProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds SnackBar Message Queue
        /// </summary>
        public SnackbarMessageQueue SnackbarMessage { get; set; } = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(1000));

        /// <summary>
        /// Holds all StockSuppliers Data.
        /// </summary>
        public ObservableCollection<StockSupplier> StockSuppliers { get; set; } = new ObservableCollection<StockSupplier>();

        /// <summary>
        /// Holds all StoreCustomers Data.
        /// </summary>
        public ObservableCollection<StoreCustomer> StoreCustomers { get; set; } = new ObservableCollection<StoreCustomer>();

        /// <summary>
        /// Holds a list of all Items in the Database.
        /// </summary>
        public ObservableCollection<StockItem> StockItems { get; set; } = new ObservableCollection<StockItem>();

        /// <summary>
        /// Holds Index for Date Range in Financial Report.
        /// </summary>
        public int FinancialRangeIndex { get; set; } = 0;

        /// <summary>
        /// Holds the Financial Report Start Date
        /// </summary>
        public DateTime FinancialReportFromDate { get; set; } = DateTime.Today;

        /// <summary>
        /// Holds the Financial Report End Date
        /// </summary>
        public DateTime FinancialReportToDate { get; set; } = DateTime.Today;

        /// <summary>
        /// Holds the Financial Report User
        /// </summary>
        public SystemUser FinancialReportUser { get; set; }

        /// <summary>
        /// Holds Profit Report Details.
        /// </summary>
        public FinancialReport ProfitReport { get; set; } = new FinancialReport();

        /// <summary>
        /// Holds Selected In List Item Data.
        /// </summary>
        public StockItem SelectedItem { get; set; }

        /// <summary>
        /// Holds the Item Report Start Date
        /// </summary>
        public DateTime ItemReportFromDate { get; set; } = DateTime.Today.AddMonths(-1);

        /// <summary>
        /// Holds the Item Report End Date
        /// </summary>
        public DateTime ItemReportToDate { get; set; } = DateTime.Today;

        /// <summary>
        /// Holds Item Report Details.
        /// </summary>
        public ItemReport ItemReport { get; set; } = new ItemReport();

        /// <summary>
        /// Holds Sales/Purchases Quantities Chart Values.
        /// </summary>
        public SeriesCollection QuantityValues { get; set; }

        /// <summary>
        /// Holds all SystemUsers Data.
        /// </summary>
        public ObservableCollection<SystemUser> SystemUsers { get; set; } = new ObservableCollection<SystemUser>();

        /// <summary>
        /// Currently Logged In User.
        /// </summary>
        public SystemUser CurrentUser { get; set; }

        /// <summary>
        /// Currently Logged In Role.
        /// </summary>
        public SystemRole CurrentRole { get; set; }
        #endregion

        #region Public Commands
        /// <summary>
        /// Loads the Financial Report Data.
        /// </summary>
        public RelayCommand LoadFinancialCommand { get; private set; }

        /// <summary>
        /// Loads the Report Data.
        /// </summary>
        public RelayCommand LoadItemCommand { get; private set; }

        /// <summary>
        /// Prints the Item Report Data.
        /// </summary>
        public RelayCommand PrintReportCommand { get; private set; }

        /// <summary>
        /// Exports DataGrid Data To Excel.
        /// </summary>
        public RelayCommand ExportExcelCommand { get; private set; }
        #endregion

        #region Class Methods
        /// <summary>
        /// Initializes UI Fired Commands.
        /// </summary>
        private void InitializeCommands()
        {
            SwitchContentCommand = new ParameterCommand<string>((param) => SwitchContent(param));
            LoadFinancialCommand = new RelayCommand(async () => await LoadFinancialReport());
            LoadItemCommand = new RelayCommand(async () => await LoadItemReport());
            PrintReportCommand = new RelayCommand(() => PrintReport());
            ExportExcelCommand = new RelayCommand(async () => await Task.Run(() => ExportToExcel()));
        }

        /// <summary>
        /// Switchs the Tabs or Page Content and Updates the UI.
        /// </summary>
        private void SwitchContent(string tabParameter)
        {
            switch (tabParameter)
            {
                // Opens View Financial Report Tab & Changes Tab Titles.
                case "Financial":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ReportsTitle1");
                    SecPageTitle = (string)Application.Current.FindResource("ReportsSubTitle1");
                    ContentAreaTab = 0;
                    break;
                // Opens View Financial Report Tab & Changes Tab Titles.
                case "ItemReport":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ReportsTitle2");
                    SecPageTitle = (string)Application.Current.FindResource("ReportsSubTitle3");
                    ContentAreaTab = 1;
                    break;
            }
        }

        /// <summary>
        /// Prints Report on the Screen.
        /// </summary>
        private void PrintReport()
        {
            switch (_selectedTab)
            {
                case "Financial":
                    _printFinancialReportService.Print(ProfitReport);
                    break;
                case "ItemReport":
                    _printItemReportService.Print(SelectedItem, ItemReport);
                    break;
            }          
        }

        /// <summary>
        /// Exports a copy of the data on display into an Excel File.
        /// </summary>
        private void ExportToExcel()
        {
            bool isSuccessful;
            // Turns on Progress Bar, Exports Entries, Turn off Progress Bar, then report completion with a message.
            switch (_selectedTab)
            {
                case "Financial":
                    ExcelProgressStatus = true;
                    isSuccessful = ExcelExporter.ExportFinancialReport(ProfitReport, StockItems);
                    ExcelProgressStatus = false;
                    if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("ReportsSnackBar1"));
                    break;
                case "ItemReport":
                    ExcelProgressStatus = true;
                    isSuccessful = ExcelExporter.ExportItemReport(ItemReport, SelectedItem, StoreCustomers, StockSuppliers);
                    ExcelProgressStatus = false;
                    if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("ReportsSnackBar2"));
                    break;
            }
        }

        /// <summary>
        /// Updates the Profit Range FromDate Depending on the Dropdown Selection.
        /// </summary>
        public void UpdateDateRange()
        {
            switch (FinancialRangeIndex)
            {
                case 0:
                    FinancialReportFromDate = DateTime.Today;
                    FinancialReportToDate = DateTime.Today;
                    break;
                case 1:
                    FinancialReportFromDate = DateTime.Today.AddDays(-7);
                    FinancialReportToDate = DateTime.Today;
                    break;
                case 2:
                    FinancialReportFromDate = DateTime.Today.AddMonths(-1);
                    FinancialReportToDate = DateTime.Today;
                    break;
                case 3:
                    FinancialReportFromDate = DateTime.Today.AddMonths(-1);
                    FinancialReportToDate = DateTime.Today.AddDays(1);
                    break;
            }
            OnPropertyChanged(nameof(ProfitReport));
        }

        /// <summary>
        /// Loads/Updates Financial Report Data.
        /// </summary>
        public async Task LoadFinancialReport()
        {
            FinancialReportStatus = true;
            ProfitReport = await _financialReportService.LoadReportData(FinancialReportFromDate, FinancialReportToDate, FinancialReportUser);
            OnPropertyChanged(nameof(ProfitReport));
            FinancialReportStatus = false;
        }

        /// <summary>
        /// Loads/Updates Item Report Data.
        /// </summary>
        public async Task LoadItemReport()
        {
            ItemReportStatus = true;
            if (SelectedItem == null) SelectedItem = StockItems[0];
            ItemReport = await _itemReportService.LoadReportData(SelectedItem.ID, ItemReportFromDate, ItemReportToDate);
            OnPropertyChanged(nameof(ItemReport));

            var converter = new BrushConverter();
            var brush = (Brush)converter.ConvertFromString("#128C73");
            var brush2 = (Brush)converter.ConvertFromString("#25D366");
            QuantityValues = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = (string)Application.Current.FindResource("ItemReportsSubTitle2"),
                    Values = new ChartValues<double>(ItemReport.SalesChartData),
                    Fill = brush,
                },
                new ColumnSeries
                {
                    Title = (string)Application.Current.FindResource("ItemReportsSubTitle3"),
                    Values = new ChartValues<double>(ItemReport.PurchasesChartData),
                    Fill = brush2,
                },
            };
            ItemReportStatus = false;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Main Class Constructor and Command Initializer
        /// </summary>
        public ReportsViewModel(IFinancialReportService financialReportService, IItemReportService itemReportService, IPrintFinancialReportService printFinancialReportService, IPrintItemReportService printItemReportService)
        {
            _financialReportService = financialReportService;
            _itemReportService = itemReportService;
            _printFinancialReportService = printFinancialReportService;
            _printItemReportService = printItemReportService;
            InitializeCommands();
            InitializeEvents();
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Initializes all Event Listensers for ReportsViewModel.
        /// </summary>
        private void InitializeEvents()
        {
            DataMessanger.OnUserSwitched += OnUserSwitched;
            DataMessanger.OnLanguageSwitched += OnLanguageSwitched;
            DataMessanger.OnMessageTransmitted += OnMessageTransmitted;
        }

        /// <summary>
        /// Sets CurrentUser Data on login event.
        /// </summary>
        private void OnUserSwitched(string message, SystemUser systemUser, SystemRole systemRole)
        {
            CurrentUser = systemUser;
            CurrentRole = systemRole;

            if (message == "Login")
            {
                if (CurrentRole.OpenFinancialReport) SwitchContent("Financial");
                else if (CurrentRole.OpenItemReport) SwitchContent("ItemReport");
            }
        }

        /// <summary>
        /// Updates data depending on message from other classes.
        /// </summary>
        private async void OnMessageTransmitted(string message, object payload)
        {
            switch (message)
            {
                case "LoadCustomers":
                    StoreCustomers = (ObservableCollection<StoreCustomer>)payload;
                    break;
                case "LoadSuppliers":
                    StockSuppliers = (ObservableCollection<StockSupplier>)payload;
                    break;
                case "LoadItems":
                    StockItems = (ObservableCollection<StockItem>)payload;
                    if (StockItems.Count != 0)
                    {
                        SelectedItem = StockItems[0];
                        await LoadItemReport();
                    }
                    break;
                case "LoadUsers":
                    SystemUsers = new ObservableCollection<SystemUser>((ObservableCollection<SystemUser>)payload);
                    SystemUsers.Insert(0, new SystemUser() { ID = 0, Name = (string)Application.Current.FindResource("ShiftsSubTitle13") });
                    FinancialReportUser = SystemUsers[0];
                    OnPropertyChanged(nameof(ProfitReport));
                    break;
                case "AddUser":
                    SystemUsers.Add(payload as SystemUser);
                    break;
                case "EditUser":
                    OnPropertyChanged(nameof(SystemUsers));
                    break;
                case "DeleteUser":
                    var removedUser = payload as SystemUser;
                    if (FinancialReportUser == removedUser) FinancialReportUser = SystemUsers[0];
                    OnPropertyChanged(nameof(ProfitReport));
                    SystemUsers.Remove(removedUser);
                    break;
                case "GetItemReport":
                    SwitchContent("ItemReport");
                    SelectedItem = (StockItem)payload;
                    await LoadItemReport();
                    break;
            }
        }

        /// <summary>
        /// Refreshes Elements On Language Change.
        /// </summary>
        private void OnLanguageSwitched(string message)
        {
            SwitchContent(_selectedTab);
            FinancialRangeIndex = 0;
            if (SystemUsers.Count != 0)
            {
                SystemUsers.Remove(SystemUsers[0]);
                SystemUsers.Insert(0, new SystemUser() { ID = 0, Name = (string)Application.Current.FindResource("ShiftsSubTitle13") });
                FinancialReportUser = SystemUsers[0];
                OnPropertyChanged(nameof(ProfitReport));
            }
        }
        #endregion
    }
}
