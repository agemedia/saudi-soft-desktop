﻿using SaudiSoft.Core;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SaudiSoft.WinApp
{
    public class PurchaseViewModel : ViewModelBase
    {
        #region Private Services
        private readonly IDataService<StockItem> _itemDataService;
        private readonly IDataService<StockCategory> _categoryDataService;
        private readonly IDataService<StockSupplier> _supplierDataService;
        private readonly IDataService<PurchaseTicket> _ticketDataService;
        private readonly IDataService<PurchaseItem> _ticketItemDataService;
        private readonly IPrintPurchasesService _printPurchasesService;
        private readonly ISupplierPaymentService _supplierPaymentService;
        #endregion

        #region Private Members
        // Holds selected Tab Name.
        private string _selectedTab = "PurchasesList";
        #endregion

        #region Public Properties
        /// <summary>
        /// Holds the Purchase Page Transistioner's Current Selected Page Number.
        /// </summary>
        public int ContentAreaTab { get; set; } = 0;

        /// <summary>
        /// Holds the Item Display Option
        /// </summary>
        public bool ItemDisplayOption { get; set; } = true;

        /// <summary>
        /// Holds PurchasePage Currenet Main Title.
        /// </summary>
        public string MainPageTitle { get; set; } = (string)Application.Current.FindResource("PurchaseTitle1");

        /// <summary>
        /// Holds PurchasePage Secondary Title.
        /// </summary>
        public string SecPageTitle { get; set; } = (string)Application.Current.FindResource("PurchaseSubTitle1");

        /// <summary>
        /// Holds all StockCategories currently in the database.
        /// </summary>
        public ObservableCollection<StockCategory> StockCategories { get; private set; }

        /// <summary>
        /// Holds all StockItems currently in the database.
        /// </summary>
        public ObservableCollection<StockItem> StockItems { get; private set; }

        /// <summary>
        /// Holds subset of StockItems on Display.
        /// </summary>
        public ObservableCollection<StockItem> FilteredStockItems { get; set; }

        /// <summary>
        /// Holds filter Keyword/Name for StoreItems.
        /// </summary>
        public string FilterKeyword { get; set; } = "";

        /// <summary>
        /// Holds filter Category for StoreItems on Display.
        /// </summary>
        public StockCategory FilterCategory { get; set; }

        /// <summary>
        /// Holds Searchkeyword (Barcode) of a StockItem, Calls a search if it holds 13 characters.
        /// </summary>
        public string SearchKeyword { get; set; }

        /// <summary>
        /// Holds Selected PurchaseItem/Cartitem in Cart.
        /// </summary>
        public PurchaseItem ActivePurchaseItem { get; set; }

        /// <summary>
        /// Payment Dialog IsOpen Status.
        /// </summary>
        public bool DialogStatus { get; set; } = false;

        /// <summary>
        /// Holds Command Loading Progress Bars.
        /// </summary>
        public bool ProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds PurchaseItems Currently in the Cart.
        /// </summary>
        public ObservableCollection<PurchaseItem> CartItems { get; set; } = new ObservableCollection<PurchaseItem>();

        /// <summary>
        /// Holds CartTicket which is Important Cart Details.
        /// </summary>
        public PurchaseTicket CartTicket { get; set; } = new PurchaseTicket();

        /// <summary>
        /// Currently Logged In User.
        /// </summary>
        public SystemUser CurrentUser { get; set; }

        /// <summary>
        /// Currently Logged In Role.
        /// </summary>
        public SystemRole CurrentRole { get; set; }

        /// <summary>
        /// Holds SnackBar Message Queue
        /// </summary>
        public SnackbarMessageQueue SnackbarMessage { get; set; } = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(1400));

        /// <summary>
        /// Holds Purchases List From Date Filter.
        /// </summary>
        public DateTime FromDateFilter { get; set; } = DateTime.Today.AddMonths(-1);

        /// <summary>
        /// Holds Purchases List To Date Filter.
        /// </summary>
        public DateTime ToDateFilter { get; set; } = DateTime.Today;

        /// <summary>
        /// Holds Purchases Supplier Filter.
        /// </summary>
        public StockSupplier SupplierFilter { get; set; }

        /// <summary>
        /// Holds Purchases User Filter.
        /// </summary>
        public SystemUser UserFilter { get; set; }

        /// <summary>
        /// Holds Selected Ticket in Purchases List Datagird.
        /// </summary>
        public PurchaseTicket SelectedTicket { get; set; }

        /// <summary>
        /// Holds Form Ticket Values in Edit Purchase Form.
        /// </summary>
        public PurchaseTicket FormTicket { get; set; } = new PurchaseTicket();

        /// <summary>
        /// Holds FormItems of the FormTicket On Display.
        /// </summary>
        public ObservableCollection<PurchaseItem> SelectedTicketItems { get; set; }

        /// <summary>
        /// Holds FormItems of the FormTicket On Display.
        /// </summary>
        public ObservableCollection<PurchaseItem> FormTicketItems { get; set; } = new ObservableCollection<PurchaseItem>();

        /// <summary>
        /// Holds PurchaseTickets On Display.
        /// </summary>
        public ObservableCollection<PurchaseTicket> PurchaseTickets { get; set; }

        /// <summary>
        /// Holds all SystemUsers Data.
        /// </summary>
        public ObservableCollection<SystemUser> SystemUsers { get; set; }

        /// <summary>
        /// Holds all StockSuppliers Data.
        /// </summary>
        public ObservableCollection<StockSupplier> StockSuppliers { get; set; }

        /// <summary>
        /// Holds Selected Purchase Supplier.
        /// </summary>
        public StockSupplier PurchaseSupplier { get; set; }

        /// <summary>
        /// Holds Filter Keyword/Name for Suppliers.
        /// </summary>
        public string FilterSupplierKeyword { get; set; } = "";

        /// <summary>
        /// Delete Dialog IsOpen Status.
        /// </summary>
        public bool DeleteDialogStatus { get; set; } = false;

        /// <summary>
        /// Holds Filter StockSuppliers Data.
        /// </summary>
        public ObservableCollection<StockSupplier> FilterStockSuppliers { get; set; }

        /// <summary>
        /// Holds Filtered StockSuppliers Data.
        /// </summary>
        public ObservableCollection<StockSupplier> FilteredStockSuppliers { get; set; }

        /// <summary>
        /// Holds Selected Supplier Data.
        /// </summary>
        public StockSupplier SelectedSupplier { get; set; }

        /// <summary>
        /// Holds Form Supplier Data.
        /// </summary>
        public StockSupplier FormSupplier { get; set; } = new StockSupplier();

        /// <summary>
        /// Holds Form Supplier Payment Amount
        /// </summary>
        public double FormSupplierPayment { get; set; }

        /// <summary>
        /// Holds Error Message for Various Forms.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Holds status for Excel Export Progress Bars.
        /// </summary>
        public ObservableCollection<bool> ExcelProgressStatus { get; set; } = new ObservableCollection<bool> { false, false };
        #endregion

        #region Public Commands
        /// <summary>
        /// Load Data From Database.
        /// </summary>
        public RelayCommand LoadDataCommand { get; private set; }

        /// <summary>
        /// Filters StockItems on Display.
        /// </summary>
        public RelayCommand FilterItemsCommand { get; private set; }

        /// <summary>
        /// Add a Item To Active Cart.
        /// </summary>
        public ParameterCommand<StockItem> AddToCartCommand { get; private set; }

        /// <summary>
        /// Removes a Item From Active Cart.
        /// </summary>
        public RelayCommand RemoveCartItemCommand { get; private set; }

        /// <summary>
        /// Updates CartTicket Depending on UI.
        /// </summary>
        public RelayCommand UpdateTicketCommand { get; private set; }

        /// <summary>
        /// Checksout CartItems When Fired.
        /// </summary>
        public RelayCommand CheckoutCommand { get; private set; }

        /// <summary>
        /// Checksout CartItems When Fired.
        /// </summary>
        public RelayCommand CheckoutPrintCommand { get; private set; }

        /// <summary>
        /// Filters PurchaseTickets on Display.
        /// </summary>
        public RelayCommand FilterPurchasesCommand { get; private set; }

        /// <summary>
        /// Prints Selected PurchaseTicket on Display.
        /// </summary>
        public RelayCommand PrintPurchaseCommand { get; private set; }

        /// <summary>
        /// Edit Selected Entry on Display.
        /// </summary>
        public RelayCommand EditEntryCommand { get; private set; }

        /// <summary>
        /// Saves Form Entry on Display.
        /// </summary>
        public RelayCommand SaveEntryCommand { get; private set; }

        /// <summary>
        /// Deletes Form Entry on Display.
        /// </summary>
        public RelayCommand DeleteEntryCommand { get; private set; }

        /// <summary>
        /// Filters Supplier Data Depending on Name.
        /// </summary>
        public RelayCommand FilterSuppliersCommand { get; private set; }

        /// <summary>
        /// Exports DataGrid Data To Excel.
        /// </summary>
        public RelayCommand ExportExcelCommand { get; private set; }
        public AppSettings _CurrentSettings { get; private set; }

        #endregion

        #region Private Functions / Methods
        /// <summary>
        /// Initializes UI Fired Commands.
        /// </summary>
        private void InitializeCommands()
        {
            SwitchContentCommand = new ParameterCommand<string>((param) => SwitchContent(param));
            FilterItemsCommand = new RelayCommand(() => FilterItems());
            AddToCartCommand = new ParameterCommand<StockItem>((param) => AddToCart(param));
            RemoveCartItemCommand = new RelayCommand(() => CartItems.Remove(ActivePurchaseItem));
            UpdateTicketCommand = new RelayCommand(() => UpdateTicket());
            CheckoutCommand = new RelayCommand(async () => await Checkout());
            CheckoutPrintCommand = new RelayCommand(async () => await Checkout(true));
            FilterPurchasesCommand = new RelayCommand(async () => await GetPurchasesAsync());
            PrintPurchaseCommand = new RelayCommand(async () => await PrintPurchaseAsync());
            EditEntryCommand = new RelayCommand(async () => await EditEntryAsync());
            SaveEntryCommand = new RelayCommand(async () => await SaveEntryAsync());
            DeleteEntryCommand = new RelayCommand(async () => await DeleteEntryAsync());
            FilterSuppliersCommand = new RelayCommand(() => FilterSuppliers());
            ExportExcelCommand = new RelayCommand(async() => await Task.Run(() => ExportToExcel()));
            _CurrentSettings = AppSettings.Load();
            CartTicket.Tax = _CurrentSettings.TaxValue;
            FormTicket.Tax = _CurrentSettings.TaxValue;
        }

        /// <summary>
        /// Switchs the Tabs or Page Content and Updates the UI.
        /// </summary>
        private void SwitchContent(string tabParameter)
        {
            switch (tabParameter)
            {
                // Opens View Purchase List Tab & Changes Tab Titles.
                case "PurchasesList":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("PurchaseTitle1");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle1");
                    ContentAreaTab = 0;
                    break;
                // Opens Add Purchase Tab & Changes Tab Titles.
                case "AddPurchase":
                    _selectedTab = tabParameter;
                    CartItems.Clear();
                    CartTicket = new PurchaseTicket();
                    CartTicket.Tax = _CurrentSettings.TaxValue;
                    PurchaseSupplier = StockSuppliers[0];
                    MainPageTitle = (string)Application.Current.FindResource("PurchaseTitle2");
                    SecPageTitle = (string)Application.Current.FindResource("PurchaseSubTitle2");
                    ContentAreaTab = 1;
                    break;
                // Opens Edit Purchase Tab & Changes Tab Titles.
                case "EditPurchase":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("PurchaseTitle3");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle3");
                    ContentAreaTab = 2;
                    break;
                // Opens View Suppliers List Tab & Changes Tab Titles.
                case "SuppliersList":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("SuppliersTitle1");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle1");
                    ContentAreaTab = 3;
                    break;
                // Opens Add Supplier Tab, Resets Form Values & Changes Tab Titles.
                case "AddSupplier":
                    FormSupplier = new StockSupplier();
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("SuppliersTitle2");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle2");
                    ContentAreaTab = 4;
                    break;
                // Opens Edit Supplier Tab & Changes Tab Titles.
                case "EditSupplier":
                    FormSupplier = SelectedSupplier.ShallowCopy();
                    FormSupplierPayment = 0;
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("SuppliersTitle3");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle3");
                    ContentAreaTab = 4;
                    break;
                // Opens Payment Dialog.
                case "OpenPayment":
                    DialogStatus = true;
                    break;
                // Closes Payment Dialog.
                case "CancelPayment":
                    DialogStatus = false;
                    break;
                // Opens Delete Entry Dialog.
                case "OpenDelete":
                    DeleteDialogStatus = true;
                    break;
                // Closes Delete Entry Dialog.
                case "CloseDelete":
                    DeleteDialogStatus = false;
                    break;
            }
        }

        /// <summary>
        /// Adds a Stockitem to the current Cart
        /// </summary>
        /// <param name="givenItem">Stockitem to be added to cart</param>
        private void AddToCart(StockItem givenItem)
        {
            // Gets givenItem from Barcode (SearchKeyword), If there is not givenItem Passed to it.
            if (givenItem == null)
            {
                givenItem = FilteredStockItems.FirstOrDefault(x => x.Barcode == SearchKeyword) ?? new StockItem();
                SearchKeyword = "";
            };

            // Searches if the Item Exists in the Code.
            var existingCartItem = CartItems.FirstOrDefault(x => x.StockItemID == givenItem.ID);

            // Another Check that item exists.
            if (givenItem.ID == 0) SnackbarMessage.Enqueue((string)Application.Current.FindResource("SalesSnackBar2"));
            // If the item is not in the Cart it Adds it.
            else if (existingCartItem == null)
            {
                var CartItem = new PurchaseItem()
                {
                    StockItemID = givenItem.ID,
                    StockItemName = givenItem.Name,
                    Quantity = 1,
                    Price = givenItem.SupplierPrice,
                    Tax = givenItem.Tax,
                    
                };
                CartItems.Add(CartItem);
            }
            // else increases quantity by 1 if givenItem quantity permits.
            else
            {
                // Best Way to Update DataGrid UI was to remove then add updated entry.
                CartItems.Remove(existingCartItem);
                existingCartItem.Quantity += 1;
                CartItems.Add(existingCartItem);
            }
        }

        /// <summary>
        /// Refreshs the items entries from allStockItems depending on filters.
        /// </summary>
        public void FilterItems()
        {
            // If filtercategory does not exist or equal to 0, only filter is the FilterKeyword.
            if (FilterCategory == null || FilterCategory.ID == 0)
                FilteredStockItems = new ObservableCollection<StockItem>(StockItems.Where(x => x.Name.ToLower().Contains(FilterKeyword.ToLower()) || x.Barcode == FilterKeyword));
            // else use both the category and name/keyword filters.
            else
                FilteredStockItems = new ObservableCollection<StockItem>(StockItems.Where(x => x.CategoryID == FilterCategory.ID && (x.Name.ToLower().Contains(FilterKeyword.ToLower()) || x.Barcode == FilterKeyword)));
        }

        /// <summary>
        /// Clears slected Cart and Resets Discount Value for selected Cart.
        /// </summary>
        public void ClearCart()
        {
            PurchaseSupplier = StockSuppliers[0];
            CartTicket.StockSupplierID = 0;
            CartTicket.Discount = 0;
            CartItems.Clear();
        }

        /// <summary>
        /// Updates the current PurchaseTicket Data.
        /// </summary>
        public void UpdateTicket()
        {
            switch(_selectedTab)
            {
                case "AddPurchase":
                    CartTicket.SubTotal = CartItems.Sum(x => x.Price * x.Quantity);
                    CartTicket.Entries = CartItems.Count;
                    OnPropertyChanged(nameof(CartTicket));
                    break;
                case "EditPurchase":
                    FormTicket.SubTotal = FormTicketItems.Sum(x => x.Price * x.Quantity);
                    OnPropertyChanged(nameof(FormTicket));
                    break;
            }
        }

        /// <summary>
        /// Filters Suppliers Depending on Given Filters.
        /// </summary>
        private void FilterSuppliers()
        {
            FilteredStockSuppliers = new ObservableCollection<StockSupplier>();
            foreach (var supplier in StockSuppliers)
            {
                var searchTerm = FilterSupplierKeyword.ToLower();
                if (supplier.ID == 0) continue;
                else if (supplier.Name != null && supplier.Name.ToLower().Contains(searchTerm)) FilteredStockSuppliers.Add(supplier);
                else if (supplier.Email != null && supplier.Email.ToLower().Contains(searchTerm)) FilteredStockSuppliers.Add(supplier);
                else if (supplier.Phone != null && supplier.Phone.ToLower().Contains(searchTerm)) FilteredStockSuppliers.Add(supplier);
                else if (supplier.Mobile != null && supplier.Mobile.ToLower().Contains(searchTerm)) FilteredStockSuppliers.Add(supplier);
            }
        }

        /// <summary>
        /// Exports a copy of the data on display into an Excel File.
        /// </summary>
        private void ExportToExcel()
        {
            bool isSuccessful;
            // Turns on Progress Bar, Exports Entries, Turn off Progress Bar, then report completion with a message.
            switch (_selectedTab)
            {
                case "PurchasesList":
                    ExcelProgressStatus[0] = true;
                    isSuccessful = ExcelExporter.ExportPurchases(PurchaseTickets,StockSuppliers,SystemUsers);
                    ExcelProgressStatus[0] = false;
                    if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("PurchaseSnackBar1"));
                    break;
                case "SuppliersList":
                    ExcelProgressStatus[1] = true;
                    isSuccessful = ExcelExporter.ExportSuppliers(StockSuppliers);
                    ExcelProgressStatus[1] = false;
                    if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("SuppliersSnackBar4"));
                    break;
            }
        }

        /// <summary>
        /// Gets a list of the Purchases Tickets Depending on Date Range Filter
        /// </summary>
        private async Task GetPurchasesAsync()
        {
            // Gets PurchaseTicket Data Depending on From and To Date Filters.
            DateTime FromDate = FromDateFilter;
            DateTime ToDate = ToDateFilter.AddDays(1);

            List<PurchaseTicket> savedTickets;
            if (SupplierFilter.ID == 0 && UserFilter.ID == 0)
            {
                savedTickets = await _ticketDataService.GetAll(predicate: x => x.PurchaseDate >= FromDate && x.PurchaseDate <= ToDate, orderBy: x => x.ID);
            }
            else if (SupplierFilter.ID == 0 && UserFilter.ID != 0)
            {
                savedTickets = await _ticketDataService.GetAll(predicate: x => x.PurchaseDate >= FromDate && x.PurchaseDate <= ToDate && x.BoughtBy == UserFilter.ID, orderBy: x => x.ID);
            }
            else if (SupplierFilter.ID != 0 && UserFilter.ID == 0)
            {
                savedTickets = await _ticketDataService.GetAll(predicate: x => x.PurchaseDate >= FromDate && x.PurchaseDate <= ToDate && x.StockSupplierID == SupplierFilter.ID, orderBy: x => x.ID);
            }
            else
            {
                savedTickets = await _ticketDataService.GetAll(predicate: x => x.PurchaseDate >= FromDate && x.PurchaseDate <= ToDate && x.BoughtBy == UserFilter.ID && x.StockSupplierID == SupplierFilter.ID, orderBy: x => x.ID);
            }
            PurchaseTickets = new ObservableCollection<PurchaseTicket>(savedTickets);
        }

        /// <summary>
        /// Loads Selected PurchaseTicket Data into Edit Form.       
        /// </summary>
        private async Task EditEntryAsync()
        {
            // Gets the PurchaseItems of the Ticket.
            List<PurchaseItem> purchaseItems = await _ticketItemDataService.GetAll(predicate: x => x.PurchaseTicketID == SelectedTicket.ID, orderBy: x => x.ID);
            SelectedTicketItems = new ObservableCollection<PurchaseItem>(purchaseItems);

            // Clones SelectedTicket to FormTicket.
            FormTicket = SelectedTicket.ShallowCopy();
            FormTicket.Tax = _CurrentSettings.TaxValue;
            FormTicketItems.Clear();

            // Clones SelectedTicketItems to FormTicketItems.
            foreach (var entry in SelectedTicketItems) 
            {
                FormTicketItems.Add(entry.ShallowCopy()); 
            }

            // Switches Page Content
            SwitchContent("EditPurchase");
        }
        
        /// <summary>
        /// Prints a recipt of SelectedTicket in Purchase List.
        /// </summary>
        private async Task PrintPurchaseAsync()
        {
            // Locks print purchase ticket command Until it is finished.
            PrintPurchaseCommand.CanExecutePredicate = false;

            // Gets PurchaseItems of the Ticket.
            List<PurchaseItem> purchaseItems = await _ticketItemDataService.GetAll(predicate: x => x.PurchaseTicketID == SelectedTicket.ID, orderBy: x => x.ID);
            SelectedTicketItems = new ObservableCollection<PurchaseItem>(purchaseItems);

            // Gets StockItem Names.
            foreach (var item in SelectedTicketItems)
            {
                item.StockItemName = (string)Application.Current.FindResource("WordUnknown");
                var stockItem = StockItems.FirstOrDefault(x => x.ID == item.StockItemID);
                if (stockItem != null) item.StockItemName = stockItem.Name;
            }

            // Gets the cashier name.
            string cashierName = SystemUsers.FirstOrDefault(x => x.ID == SelectedTicket.BoughtBy).Name ?? "";
            string paymentName = ((string[])Application.Current.FindResource("SalesPaymentMethod"))[(int)SelectedTicket.PayMethod];
            var dialog = SetupExportDialog("Bill" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            //if (dialog.ShowDialog() == true)
            //{
                if(!_printPurchasesService.Print(SelectedTicket, SelectedTicketItems, paymentName, cashierName, dialog.FileName))
                {
                    MessageBox.Show(Application.Current.FindResource("ErrorPrint").ToString());
                }
            //}
            // Unlocks print purchase ticket command.
            PrintPurchaseCommand.CanExecutePredicate = true;
        }
        private static Microsoft.Win32.SaveFileDialog SetupExportDialog(string fileName)
        {
            var dialog = new Microsoft.Win32.SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "PDF File (*.pdf) | *.pdf",
                FileName = fileName,
            };
            return dialog;
        }
        /// <summary>
        /// Updates the Database entries of the changes to PurchaseTicket.
        /// </summary>
        private async Task SaveEntryAsync()
        {
            // Turns on Progress Bar to indicate command in action.
            ProgressStatus = true;

            // Resets Error Message.
            ErrorMessage = null;
            switch (_selectedTab)
            {
                case "EditPurchase":
                    // Calculates discounted Total Price. (Saves it as TotalPaid if it was Payed by non-cash methods)
                    var discountedTotal = FormTicket.CalculatedTotalPrice;

                    // Saves discounted Total as TotalPaid if it was Payed by non-cash methods
                    if (FormTicket.TotalPaid >= discountedTotal) FormTicket.TotalPaid = discountedTotal;

                    // Saves the FormTicket Changes to UI and Updates the UI.
                    FormTicket.Editedby = CurrentUser.ID;
                    await _ticketDataService.UpdateEntry(FormTicket);

                    var payAmountChange = discountedTotal - SelectedTicket.CalculatedTotalPrice;
                    var paidAmountChange = FormTicket.TotalPaid - SelectedTicket.TotalPaid;

                    // Updates Selected Supplier For purchase.
                    var purchaseSupplier = StockSuppliers.FirstOrDefault(x => x.ID == FormTicket.StockSupplierID) ?? new StockSupplier();
                    purchaseSupplier.PurchaseDue += payAmountChange;
                    purchaseSupplier.PaidPurchaseDue += paidAmountChange;
                    await _supplierDataService.UpdateEntry(purchaseSupplier);
                    if (purchaseSupplier.ID != 0)
                    {
                        FilterSuppliers();
                    }

                    // Updates Shift Data.
                    DataMessanger.UpdateShift(SelectedTicket.TotalPaid - FormTicket.TotalPaid);

                    SelectedTicket = PurchaseTickets[PurchaseTickets.IndexOf(SelectedTicket)] = FormTicket;

                    foreach (var entry in FormTicketItems)
                    {
                        // Updates Purchase Items
                        await _ticketItemDataService.UpdateEntry(entry);

                        var uneditedEntry = SelectedTicketItems.FirstOrDefault(x => x.ID == entry.ID);
                        var quantityChange = uneditedEntry.Quantity - entry.Quantity;

                        // Updates Stock Quanitites in the Database
                        var stockEntry = StockItems.FirstOrDefault(x => x.ID == entry.StockItemID);
                        if (stockEntry != null)
                        {
                            stockEntry.Quantity -= quantityChange;
                            await _itemDataService.UpdateEntry(stockEntry);
                            StockItems[StockItems.IndexOf(stockEntry)] = stockEntry;

                            // Informs other Classes of Stockitem Changes.
                            DataMessanger.BroadCast("itemBought", stockEntry);
                        }
                    }
                    FilterItems();
                    // Switches Page Content
                    SwitchContent("PurchasesList");
                    break;
                case "AddSupplier":
                    if (FormSupplier.Name == null || FormSupplier.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    FormSupplier.ID = await _supplierDataService.CreateEntry(FormSupplier);
                    if (FormSupplier.Name.ToLower().Contains(FilterSupplierKeyword.ToLower()))
                    {
                        FilteredStockSuppliers.Add(FormSupplier);
                        SelectedSupplier = FormSupplier;
                    }
                    StockSuppliers.Add(FormSupplier);
                    FilterStockSuppliers.Add(FormSupplier);
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("SuppliersSnackBar1"));
                    // Switches Page Content
                    SwitchContent("SuppliersList");
                    break;
                case "EditSupplier":
                    if (FormSupplier.Name == null || FormSupplier.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    if (PurchaseSupplier.ID == SelectedSupplier.ID)
                    {
                        PurchaseSupplier = StockSuppliers[0];
                    }
                    if (SupplierFilter.ID == SelectedSupplier.ID)
                    {
                        SupplierFilter = FilterStockSuppliers[0];
                    }

                    if (FormSupplierPayment != 0)
                    {
                        await _supplierPaymentService.AddPayment(FormSupplier, FormSupplierPayment, CurrentUser);
                        DataMessanger.UpdateShift(-FormSupplierPayment);
                        await GetPurchasesAsync();
                    }
                    else await _supplierDataService.UpdateEntry(FormSupplier);
                    StockSuppliers[StockSuppliers.IndexOf(SelectedSupplier)] = FormSupplier;
                    FilterStockSuppliers[FilterStockSuppliers.IndexOf(SelectedSupplier)] = FormSupplier;
                    SelectedSupplier = FilteredStockSuppliers[FilteredStockSuppliers.IndexOf(SelectedSupplier)] = FormSupplier;
                    OnPropertyChanged(nameof(StockSuppliers));
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("SuppliersSnackBar2"));
                    // Switches Page Content
                    SwitchContent("SuppliersList");
                    break;
            }            
            // Turns off Progress Bar to indicate command is complete.
            ProgressStatus = false;
        }

        /// <summary>
        /// Deletes an existing data entry (And Adjusts the View).
        /// </summary>
        private async Task DeleteEntryAsync()
        {
            switch (_selectedTab)
            {
                case "PurchasesList":
                    await _ticketDataService.DeleteEntry(SelectedTicket);

                    // Updates Selected Supplier For purchase.
                    StockSupplier purchaseSupplier = StockSuppliers.FirstOrDefault(x => x.ID == SelectedTicket.StockSupplierID) ?? new StockSupplier();
                    purchaseSupplier.PurchaseDue -= SelectedTicket.CalculatedTotalPrice;
                    purchaseSupplier.PaidPurchaseDue -= SelectedTicket.TotalPaid;
                    await _supplierDataService.UpdateEntry(purchaseSupplier);
                    if (purchaseSupplier.ID != 0)
                    {
                        FilterSuppliers();
                    }

                    // Updates Shift Data.
                    DataMessanger.UpdateShift(SelectedTicket.TotalPaid);

                    List<PurchaseItem> purchaseItems = await _ticketItemDataService.GetAll(x => x.PurchaseTicketID == SelectedTicket.ID);

                    foreach (PurchaseItem entry in purchaseItems)
                    {
                        // Updates Purchase Items
                        await _ticketItemDataService.UpdateEntry(entry);

                        // Updates Stock Quanitites in the Database
                        var stockEntry = StockItems.FirstOrDefault(x => x.ID == entry.StockItemID);
                        if (stockEntry != null)
                        {
                            stockEntry.Quantity -= entry.Quantity;
                            await _itemDataService.UpdateEntry(stockEntry);
                            StockItems[StockItems.IndexOf(stockEntry)] = stockEntry;

                            // Informs other Classes of Stockitem Changes.
                            DataMessanger.BroadCast("itemBought", stockEntry);
                        }
                    }
                    PurchaseTickets.Remove(SelectedTicket);
                    FilterItems();
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("PurchaseSnackBar2"));
                    break;
                case "SuppliersList":
                    await _supplierDataService.DeleteEntry(SelectedSupplier);
                    if (PurchaseSupplier.ID == SelectedSupplier.ID)
                    {
                        PurchaseSupplier = StockSuppliers[0];
                    }
                    StockSuppliers.Remove(SelectedSupplier);
                    FilterStockSuppliers.Remove(SelectedSupplier);
                    FilteredStockSuppliers.Remove(SelectedSupplier);
                    DialogStatus = false;
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("SuppliersSnackBar3"));
                    break;
            }
            SwitchContent("CloseDelete");
            // Switches Page Content
        }

        /// <summary>
        /// Checkout Method Where Database Entires are Added.
        /// </summary>
        private async Task Checkout(bool Printable = false)
        {
            // Turns on Progress Bar to indicate command in action.
            ProgressStatus = true;

            // Calculates discounted Total Price. (Saves it as TotalPaid if it was Payed by non-cash methods)
            var discountedTotal = CartTicket.CalculatedTotalPrice;
            // Holds CartTicket Current TotalPaid Value.
            var paidCash = CartTicket.TotalPaid;

            // Saves discounted Total as TotalPaid if it was Payed by non-cash methods
            if (CartTicket.PayMethod != PaymentMethod.Cash || CartTicket.TotalPaid >= discountedTotal) 
                CartTicket.TotalPaid = discountedTotal;

            // Sets PurchaseDate & BoughtBy Attributes and Saves Ticket Details to Database.
            CartTicket.PurchaseDate = DateTime.Now;
            CartTicket.BoughtBy = CurrentUser.ID;
            CartTicket.StockSupplierID = PurchaseSupplier.ID;
            CartTicket.ID = await _ticketDataService.CreateEntry(CartTicket);

            // Updates Selected Supplier For purchase.
            PurchaseSupplier.PurchaseDue += discountedTotal;
            PurchaseSupplier.PaidPurchaseDue += CartTicket.TotalPaid;
            await _supplierDataService.UpdateEntry(PurchaseSupplier);
            if (PurchaseSupplier.ID != 0)
            {
                FilterSuppliers();
            }

            // Saves the Cart Details to the database.
            foreach (var entry in CartItems)
            {
                entry.PurchaseTicketID = CartTicket.ID;
                await _ticketItemDataService.CreateEntry(entry);

                // Updates Stock Quanitites in the Database
                var stockEntry = StockItems.FirstOrDefault(x => x.ID == entry.StockItemID);
                stockEntry.Quantity += entry.Quantity;
                await _itemDataService.UpdateEntry(stockEntry);
                StockItems[StockItems.IndexOf(stockEntry)] = stockEntry;

                // Informs other Classes of Stockitem Changes.
                DataMessanger.BroadCast("itemBought", stockEntry);
            }
             
            // If Cashier Decides to Print the Ticket.
            if (Printable == true)
            {
                if (CartTicket.PayMethod == PaymentMethod.Cash) CartTicket.TotalPaid = paidCash;

                string paymentName = ((string[])Application.Current.FindResource("SalesPaymentMethod"))[(int)CartTicket.PayMethod];
                var dialog = SetupExportDialog("Bill_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                //if (dialog.ShowDialog() == true)
               // {
                    if(!_printPurchasesService.Print(CartTicket, CartItems, paymentName, CurrentUser.Name,dialog.FileName))
                    {
                        MessageBox.Show(Application.Current.FindResource("ErrorPrint").ToString());
                    }
               // }
                // Resets Total Paid back to what it was before print.
                if (CartTicket.TotalPaid >= discountedTotal) CartTicket.TotalPaid = discountedTotal;
            }

            // Adds new Purchase Ticket to Purchase List, if it is with Date Range on Display.
            if (CartTicket.PurchaseDate >= FromDateFilter && CartTicket.PurchaseDate <= ToDateFilter.AddDays(1))
                PurchaseTickets.Insert(0, CartTicket);

            // Updates Shift Data.
            DataMessanger.UpdateShift(- CartTicket.TotalPaid);

            // Resets the UI for a New Sale
            PurchaseSupplier = StockSuppliers[0];
            CartTicket = new PurchaseTicket();
            CartTicket.Tax = _CurrentSettings.TaxValue;
            CartItems.Clear();
            FilterItems();

            // Turns off Progress Bar to indicate command is complete.
            ProgressStatus = false;

            // Switches Page Content
            SwitchContent("CancelPayment");
            SwitchContent("PurchasesList");
        }

        /// <summary>
        /// Refreshs the datagrid entries from the database.
        /// </summary>
        public async Task LoadDataAsync()
        {
            // Gets all stock items data from database.
            List<StockItem> savedItems = await _itemDataService.GetAll();
            StockItems = new ObservableCollection<StockItem>(savedItems);
            FilterItems();

            // Gets all categories data from database.
            List<StockCategory> savedCategories = await _categoryDataService.GetAll();
            StockCategories = new ObservableCollection<StockCategory>(savedCategories);

            // Adds a "All Category" entry to Categories as non filter.
            StockCategories.Insert(0, new StockCategory() { ID = 0, Name = (string)Application.Current.FindResource("ItemsSubTitle7") });
            FilterCategory = StockCategories[0];

            // Gets all suppliers data from database
            List<StockSupplier> savedSuppliers = await _supplierDataService.GetAll();
            StockSuppliers = new ObservableCollection<StockSupplier>(savedSuppliers);
            DataMessanger.BroadCast("LoadSuppliers", StockSuppliers);

            // Gets All filter customers data
            FilterStockSuppliers = new ObservableCollection<StockSupplier>(StockSuppliers);
            FilterStockSuppliers.Insert(0, new StockSupplier() { ID = 0, Name = (string)Application.Current.FindResource("SuppliersSubTitle3") });
            SupplierFilter = FilterStockSuppliers[0];

            // Adds a "Unknown" entry to Suppliers as non filter.
            StockSuppliers.Insert(0, new StockSupplier() { ID = 0, Name = (string)Application.Current.FindResource("WordUnknown") });
            PurchaseSupplier = StockSuppliers[0];
            FilterSuppliers();

            // Gets Some PurchaseTickets data from database.
            await GetPurchasesAsync();
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Main Class Constructor and Command Initializer
        /// </summary>
        public PurchaseViewModel(IDataService<StockItem> itemDataService, IDataService<StockCategory> categoryDataService, IDataService<StockSupplier> supplierDataService,
            IDataService<PurchaseTicket> ticketDataService, IDataService<PurchaseItem> ticketItemDataService, IPrintPurchasesService printPurchasesService, ISupplierPaymentService supplierPaymentService)
        {
            _itemDataService = itemDataService;
            _categoryDataService = categoryDataService;
            _supplierDataService = supplierDataService;
            _ticketDataService = ticketDataService;
            _ticketItemDataService = ticketItemDataService;
            _printPurchasesService = printPurchasesService;
            _supplierPaymentService = supplierPaymentService;
            InitializeCommands();
            InitializeEvents();
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Initializes all Event Listensers for PurchaseViewModel.
        /// </summary>
        private void InitializeEvents()
        {
            DataMessanger.OnMessageTransmitted += OnMessageTransmitted;
            DataMessanger.OnLanguageSwitched += OnLanguageSwitched;
            DataMessanger.OnUserSwitched += OnUserSwitched;
            CartItems.CollectionChanged += CartItems_CollectionChanged;
        }

        /// <summary>
        /// Sets CurrentUser Data on login event.
        /// </summary>
        private void OnUserSwitched(string message, SystemUser systemUser, SystemRole systemRole)
        {
            CurrentUser = systemUser;
            CurrentRole = systemRole;
            if (message == "Login")
            {
                if (CurrentRole.OpenPurchasesListTab) SwitchContent("PurchasesList");
                else if (CurrentRole.OpenSuppliersTab) SwitchContent("SuppliersList");
            }
        }

        /// <summary>
        /// Updates data depending on message from other classes.
        /// </summary>
        private void OnMessageTransmitted(string message, object payload)
        {
            switch (message)
            {
                case "add":
                    if (payload is StockItem)
                    {
                        var item = payload as StockItem;
                        StockItems.Add(item);
                        if ((FilterCategory.ID == 0 || item.CategoryID == FilterCategory.ID) && item.Name.ToLower().Contains(FilterKeyword.ToLower()))
                            FilteredStockItems.Add(item);
                    }
                    else if (payload is StockCategory)
                    {
                        var category = payload as StockCategory;
                        StockCategories.Add(category);
                    }
                    break;
                case "edit":
                    if (payload is StockItem)
                    {
                        var item = payload as StockItem;
                        StockItems[StockItems.IndexOf(item)] = item;
                        if ((FilterCategory.ID == 0 || item.CategoryID == FilterCategory.ID) && item.Name.ToLower().Contains(FilterKeyword.ToLower()))
                            FilteredStockItems[FilteredStockItems.IndexOf(item)] = item;
                    }
                    else if (payload is StockCategory)
                    {
                        var category = payload as StockCategory;
                        StockCategories[StockCategories.IndexOf(category)] = category;
                        FilterCategory = StockCategories[0];
                    }
                    break;
                case "delete":
                    if (payload is StockItem)
                    {
                        var item = payload as StockItem;
                        StockItems.Remove(item);
                        if ((FilterCategory.ID == 0 || item.CategoryID == FilterCategory.ID) && item.Name.ToLower().Contains(FilterKeyword.ToLower()))
                            FilteredStockItems.Remove(item);
                    }
                    else if (payload is StockCategory)
                    {
                        var category = payload as StockCategory;
                        if (FilterCategory == category) FilterCategory = StockCategories[0];
                        StockCategories.Remove(category);
                    }
                    break;
                case "itemSold":
                    OnMessageTransmitted("edit", payload);
                    OnPropertyChanged(nameof(CartItems));
                    break;
                case "LoadUsers":
                    SystemUsers = new ObservableCollection<SystemUser>((ObservableCollection<SystemUser>)payload);
                    SystemUsers.Insert(0, new SystemUser() { ID = 0, Name = (string)Application.Current.FindResource("ShiftsSubTitle13") });
                    UserFilter = SystemUsers[0];
                    break;
                case "AddUser":
                    SystemUsers.Add(payload as SystemUser);
                    break;
                case "EditUser":
                    OnPropertyChanged(nameof(SystemUsers));
                    break;
                case "DeleteUser":
                    var removedUser = payload as SystemUser;
                    if (UserFilter == removedUser) UserFilter = SystemUsers[0];
                    SystemUsers.Remove(removedUser);
                    break;
                case "TabSwitched":
                    DialogStatus = false;
                    DeleteDialogStatus = false;
                    break;
                case "SetItemDisplayOption":
                    ItemDisplayOption = (bool)payload;
                    break;
            }
        }

        /// <summary>
        /// Refreshes Elements On Language Change.
        /// </summary>
        private void OnLanguageSwitched(string message)
        {
            SwitchContent(_selectedTab);
            OnPropertyChanged(nameof(PurchaseTickets));
            OnPropertyChanged(nameof(CartItems));
            if (StockCategories != null)
            {
                StockCategories.Remove(StockCategories[0]);
                StockCategories.Insert(0, new StockCategory() { ID = 0, Name = (string)Application.Current.FindResource("ItemsSubTitle7") });
                FilterCategory = StockCategories[0];
            }
            if (StockSuppliers != null)
            {
                StockSuppliers.Remove(StockSuppliers[0]);
                StockSuppliers.Insert(0, new StockSupplier() { ID = 0, Name = (string)Application.Current.FindResource("WordUnknown") });
                PurchaseSupplier = StockSuppliers[0];
            }
            if (FilterStockSuppliers != null)
            {
                FilterStockSuppliers.Remove(FilterStockSuppliers[0]);
                FilterStockSuppliers.Insert(0, new StockSupplier() { ID = 0, Name = (string)Application.Current.FindResource("SuppliersSubTitle3") });
                SupplierFilter = FilterStockSuppliers[0];
            }
            if (SystemUsers != null && SystemUsers.Count != 0)
            {
                SystemUsers.Remove(SystemUsers[0]);
                SystemUsers.Insert(0, new SystemUser() { ID = 0, Name = (string)Application.Current.FindResource("ShiftsSubTitle13") });
                UserFilter = SystemUsers[0];
            }
        }

        /// <summary>
        /// Updates Tickets On Cartitems Collection Change.
        /// </summary>
        private void CartItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) => UpdateTicket();
        #endregion   
    }
}
