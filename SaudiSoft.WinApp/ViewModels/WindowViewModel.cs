﻿using SaudiSoft.Core;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SaudiSoft.WinApp
{
    public class WindowViewModel : ViewModelBase
    {
        #region Private Services
        /// <summary>
        /// Authentication service that access the database.
        /// </summary>
        private readonly IAuthService _authService;
        #endregion

        #region Application ViewModels
        private readonly DashboardViewModel _dashboardViewModel;
        private readonly SalesViewModel _salesViewModel;
        private readonly PurchaseViewModel _purchaseViewModel;
        private readonly ItemsViewModel _itemsViewModel;
        private readonly ExpensesViewModel _expensesViewModel;
        private readonly ReportsViewModel _reportsViewModel;
        private readonly UsersViewModel _usersViewModel;
        private readonly ShiftsViewModel _shiftsViewModel;
        private readonly SettingsViewModel _settingsViewModel;

        public DashboardViewModel DashboardViewModel => _dashboardViewModel;
        public SalesViewModel SalesViewModel => _salesViewModel;
        public PurchaseViewModel PurchaseViewModel => _purchaseViewModel;
        public ItemsViewModel ItemsViewModel => _itemsViewModel;
        public ExpensesViewModel ExpensesViewModel => _expensesViewModel;
        public ReportsViewModel ReportsViewModel => _reportsViewModel;
        public UsersViewModel UsersViewModel => _usersViewModel;
        public ShiftsViewModel ShiftsViewModel => _shiftsViewModel;
        public SettingsViewModel SettingsViewModel => _settingsViewModel;
        #endregion

        #region Private Members
        private bool _isDataLoaded = false;
        #endregion

        #region Public Properties
        /// <summary>
        /// Holds Content Area Transistioner's Current Selected Page Number.
        /// </summary>
        public int ContentAreaPage { get; set; } = 0;

        /// <summary>
        /// Holds Value of the Login/Signup Progress.
        /// </summary>
        public bool AuthProgress { get; set; } = false;

        /// <summary>
        /// Holds Whether or not Sidebar is Visible.
        /// </summary>
        public bool IsSidebarVisible { get; set; } = false;

        /// <summary>
        /// Holds Value Serial Key in Signup Page.
        /// </summary>
        public string SerialKey { get; set; }

        /// <summary>
        /// Holds Value Serial Key in Signup Page.
        /// </summary>
        public string UserFullName { get; set; }

        /// <summary>
        /// Holds Value Username in Login/Signup Pages.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Holds Login Error Messages.
        /// </summary>
        public string AuthErrorMessage { get; set; }

        /// <summary>
        /// Holds Window Title 
        /// </summary>
        public string WindowTitle { get; set; } = (string)Application.Current.FindResource("WindowTitle0");

        /// <summary>
        /// Holds Logged in User's Details
        /// </summary>
        public SystemUser ActiveUser { get; set; }

        /// <summary>
        /// Holds Logged in User's Role Permissions
        /// </summary>
        public SystemRole ActiveRole { get; set; }
        #endregion

        #region Public Command
        /// <summary>
        /// UI Caller To the Login Function
        /// </summary>
        public ParameterCommand<object> LoginCommand { get; set; }
        public ParameterCommand<object> RegisterKeyCommand { get; set; }
        /// <summary>
        /// UI Caller to Logout Current User.
        /// </summary>
        public RelayCommand LogoutCommand { get; set; }
        /// <summary>
        /// UI Caller To the Register Function
        /// </summary>
        public ParameterCommand<object> RegisterCommand { get; set; }
        AppSettings appsetting = AppSettings.Load();
        #endregion

        #region Class Functions / Methods
        /// <summary>
        /// Initializes UI Fired Commands.
        /// </summary>
        private void InitializeCommands()
        {
            SwitchContentCommand = new ParameterCommand<string>((param) => SwitchContent(param));
            LoginCommand = new ParameterCommand<object>(async (param) => await LoginMethod(param));
            RegisterKeyCommand = new ParameterCommand<object>(async (param) => await RegisterKeyMethod(param));
            LogoutCommand = new RelayCommand(() => LogoutMethod());
            RegisterCommand = new ParameterCommand<object>(async (param) => await RegisterMethod(param));

        }

        /// <summary>
        /// Switches the Window Page Content and Updates the UI.
        /// </summary>
        private void SwitchContent(string tabParameter)
        {
            // Saves current page number.
            var currentPage = ContentAreaPage;

            switch (tabParameter)
            {
                // Opens Login Page & Changes Window Title.
                case "LoginPage":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle0");
                    IsSidebarVisible = false;
                    ContentAreaPage = 0;
                    break;
                // Unlocks Application & Changes Window to Dashboard Page & Title.
                case "UnlockApp":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle1");
                    IsSidebarVisible = true;
                    ContentAreaPage = 1;
                    break;
                // Opens Dashboard Page & Changes Window Title.
                case "Dashboard":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle1");
                    ContentAreaPage = 1;
                    if (currentPage != 1) DataMessanger.BroadCast("RefreshDashboard");
                    break;
                // Opens Sales Page & Changes Window Title.
                case "Sales":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle2");
                    ContentAreaPage = 2;
                    break;
                // Opens Purchases Page & Changes Window Title.
                case "Purchases":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle3");
                    ContentAreaPage = 3;
                    break;
                // Opens Items Page & Changes Window Title.
                case "Items":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle4");
                    ContentAreaPage = 4;
                    break;
                // Opens Expenses Page & Changes Window Title.
                case "Expenses":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle5");
                    ContentAreaPage = 5;
                    break;
                // Opens Reports Page & Changes Window Title.
                case "Reports":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle6");
                    ContentAreaPage = 6;
                    break;
                // Opens Users Page & Changes Window Title.
                case "Users":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle7");
                    ContentAreaPage = 7;
                    break;
                // Opens Shifts Page & Changes Window Title.
                case "Shifts":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle8");
                    ContentAreaPage = 8;
                    break;
                // Opens Settings Page & Changes Window Title.
                case "Settings":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle9");
                    ContentAreaPage = 9;
                    break;
                case "FirstTimeSignUp":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle0");
                    ContentAreaPage = 10;
                    break;
                // Opens Settings Page & Changes Window Title.
                case "RegisterKeyPage":
                    WindowTitle = (string)Application.Current.FindResource("WindowTitle0");
                    ContentAreaPage = 11;
                    break;
            }
            DataMessanger.BroadCast("TabSwitched");
        }

        /// <summary>
        /// Login a user by verifying username and password.
        /// </summary>
        /// <param name="parameter">Holds the PasswordBox</param>
        private async Task LoginMethod(object parameter)
        {
            // Turns on Progress Bar & Clears Login Error.
            AuthProgress = true;
            AuthErrorMessage = null;
            AuthServiceResult authServiceResult = await _authService.Login(Username, (parameter as PasswordBox).Password);
            (parameter as PasswordBox).Password = "";

            switch (authServiceResult.ResultMessage)
            {
                case AuthMessage.MissingName:
                    AuthErrorMessage = (string)Application.Current.FindResource("LoginError1");
                    break;
                case AuthMessage.MissingPassword:
                    AuthErrorMessage = (string)Application.Current.FindResource("LoginError2");
                    break;
                case AuthMessage.InvalidUsername:
                    AuthErrorMessage = (string)Application.Current.FindResource("LoginError3");
                    break;
                case AuthMessage.InvalidPassword:
                    AuthErrorMessage = (string)Application.Current.FindResource("LoginError4");
                    break;
                case AuthMessage.LoginSuccessful:
                    if (!_isDataLoaded) await LoadViewModelData();
                    ActiveUser = authServiceResult.ResultUser;
                    ActiveRole = authServiceResult.ResultRole;
                    DataMessanger.UserSwitch("Login", ActiveUser, ActiveRole);
                    SwitchContent("UnlockApp");
                    break;
            }

            // Turns off Progress Bar to indicate command is complete.
            AuthProgress = false;
        }
        private bool CheckConnection(string URL)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Register the App using key.
        /// </summary>
        /// <param name="parameter">Holds the PasswordBox</param>
        private async Task RegisterKeyMethod(object parameter)
        {
            // Turns on Progress Bar & Clears Login Error.
            if (string.IsNullOrEmpty(appsetting.Key))
            {
                if (CheckConnection("https://www.google.com"))
                {

                    AuthProgress = true;
                    AuthErrorMessage = null;

                    var authServiceResult = await _authService.RegisterKey(SerialKey);

                    switch (authServiceResult.Item1.ResultMessage)
                    {

                        case AuthMessage.MissingSerial:
                            AuthErrorMessage = authServiceResult.Item2;
                            break;
                        case AuthMessage.LoginSuccessful:
                            appsetting.Key = SerialKey;
                            appsetting.RegisterDate = DateTime.Now;
                            appsetting.Save();
                            SwitchContent("FirstTimeSignUp");
                            break;
                    }
                }
                else
                {
                    AuthErrorMessage = (string)Application.Current.FindResource("WindowLogin13");
                    return;
                }
            }
            else
            {
                SwitchContent("FirstTimeSignUp");
            }

            // Turns off Progress Bar to indicate command is complete.
            AuthProgress = false;
        }

        /// <summary> 
        /// Logs out and clear current user data from viewmodel.
        /// </summary>
        /// <param name="parameter">Holds the PasswordBox</param>
        private void LogoutMethod()
        {
            // Clears user & role Data, as well as login fields.
            ActiveUser = null;
            ActiveRole = null;
            Username = "";

            // Informs other classes that User was Logged out.
            DataMessanger.UserSwitch("Logout", null, null);

            // Locks App. Swtiches to loginPage.
            SwitchContent("LoginPage");
        }

        /// <summary>
        /// Check if this is the firs time launch of the application. (By checking if there are no users in the system)
        /// </summary>
        private async void CheckForFirstTime()
        {
            if (string.IsNullOrEmpty(appsetting.Key))
            {
                SwitchContent("RegisterKeyPage");
            }
            else
            {
                var date = DateTime.Now - appsetting.RegisterDate;

                if (date.TotalDays > 180)
                {
                    if (CheckConnection("https://www.google.com"))
                    {
                        var authServiceResult = await _authService.RegisterKey(appsetting.Key);
                        if (authServiceResult.Item1.ResultMessage != AuthMessage.LoginSuccessful)
                        {
                            SwitchContent("RegisterKeyPage");
                            return;
                        }
                    }
                    else
                    {
                        AuthErrorMessage = (string)Application.Current.FindResource("WindowLogin13");
                        return;
                    }
                }
                else
                {

                    int savedUser = await _authService.CheckForSavedUsers();
                    // if there are no users, open the sign up page.
                    if (savedUser == 0)
                    {
                        SwitchContent("FirstTimeSignUp");
                    }
                }
            }
        }

        /// <summary>
        /// Regoster a user if there are no users in the system.
        /// </summary>
        /// <param name="parameter">Holds the PasswordBoxes</param>
        private async Task RegisterMethod(object parameter)
        {
            // Turns on Progress Bar & Clears Login Error.
            AuthProgress = true;
            AuthErrorMessage = null;

            AuthServiceResult authServiceResult = await _authService.FirstTimeRegister(SerialKey, UserFullName, Username, (parameter as PasswordBox).Password);
            (parameter as PasswordBox).Password = "";

            switch (authServiceResult.ResultMessage)
            {
                case AuthMessage.MissingName:
                    AuthErrorMessage = (string)Application.Current.FindResource("LoginError1");
                    break;
                case AuthMessage.MissingPassword:
                    AuthErrorMessage = (string)Application.Current.FindResource("LoginError2");
                    break;
                case AuthMessage.InvalidSerial:
                    AuthErrorMessage = (string)Application.Current.FindResource("LoginError6");
                    break;
                case AuthMessage.RegisterSuccessful:
                    if (!_isDataLoaded) await LoadViewModelData();
                    ActiveUser = authServiceResult.ResultUser;
                    ActiveRole = authServiceResult.ResultRole;
                    DataMessanger.UserSwitch("Register", ActiveUser, ActiveRole);
                    SwitchContent("UnlockApp");
                    break;
            }

            // Turns off Progress Bar to indicate command is complete.
            AuthProgress = false;
        }

        /// <summary>
        /// Loads all ViewModels data and set them up.
        /// </summary>
        private async Task LoadViewModelData()
        {
            await _dashboardViewModel.LoadDataAsync();
            await _usersViewModel.LoadDataAsync();
            await _itemsViewModel.LoadDataAsync();
            await _shiftsViewModel.LoadDataAsync();
            await _expensesViewModel.LoadDataAsync();
            await _salesViewModel.LoadDataAsync();
            await _purchaseViewModel.LoadDataAsync();
            await _reportsViewModel.LoadFinancialReport();
            _isDataLoaded = true;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default Class Constructor and Command Initializer
        /// </summary>
        public WindowViewModel(IAuthService authService, DashboardViewModel dashboardViewModel, SalesViewModel salesViewModel,
            PurchaseViewModel purchaseViewModel, ItemsViewModel itemsViewModel, ExpensesViewModel expensesViewModel, ReportsViewModel reportsViewModel,
            UsersViewModel usersViewModel, ShiftsViewModel shiftsViewModel, SettingsViewModel settingsViewModel)
        {
            _authService = authService;
            _dashboardViewModel = dashboardViewModel;
            _salesViewModel = salesViewModel;
            _purchaseViewModel = purchaseViewModel;
            _itemsViewModel = itemsViewModel;
            _expensesViewModel = expensesViewModel;
            _reportsViewModel = reportsViewModel;
            _usersViewModel = usersViewModel;
            _shiftsViewModel = shiftsViewModel;
            _settingsViewModel = settingsViewModel;
            CheckForFirstTime();
            InitializeCommands();
            InitializeEvents();
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Initializes all Event Listensers for SalesViewModel.
        /// </summary>
        private void InitializeEvents()
        {
            DataMessanger.OnMessageTransmitted += OnMessageTransmitted;
            DataMessanger.OnUserSwitched += OnUserSwitched;
        }

        /// <summary>
        /// Sets CurrentUser Data on login event.
        /// </summary>
        private void OnUserSwitched(string message, SystemUser systemUser, SystemRole systemRole)
        {
            ActiveUser = systemUser;
            ActiveRole = systemRole;
        }

        /// <summary>
        /// Updates data depending on message from other classes.
        /// </summary>
        private void OnMessageTransmitted(string message, object payload)
        {
            switch (message)
            {
                case "GetItemReport":
                    SwitchContent("Reports");
                    break;
            }
        }
        #endregion
    }
}
