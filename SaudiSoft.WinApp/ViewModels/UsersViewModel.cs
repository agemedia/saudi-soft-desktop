﻿using SaudiSoft.Core;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;

namespace SaudiSoft.WinApp
{
    public class UsersViewModel : ViewModelBase
    {
        #region Private Services
        private readonly IDataService<SystemUser> _userDataService;
        private readonly IDataService<SystemPassword> _passwordDataService;
        private readonly IDataService<SystemRole> _roleDataService;
        private readonly IHasherService _hasherService;
        #endregion

        #region Public Properties
        /// <summary>
        /// Holds selected Tab Name.
        /// </summary>
        private string _selectedTab = "ViewUsers";
        #endregion

        #region Public Properties
        /// <summary>
        /// Holds ItemsPage Transistioner's Current Selected Page Number.
        /// </summary>
        public int ContentAreaTab { get; set; } = 0;

        /// <summary>
        /// Holds Items Page Main Title.
        /// </summary>
        public string MainPageTitle { get; set; } = (string)Application.Current.FindResource("UsersTitle1");

        /// <summary>
        /// Holds Items Page Secondary Title.
        /// </summary>
        public string SecPageTitle { get; set; } = (string)Application.Current.FindResource("MiscDBSubTitle1");

        /// <summary>
        /// Holds DialogBox Status.
        /// </summary>
        public bool DialogStatus { get; set; } = false;

        /// <summary>
        /// Holds status for Various Forms Progress Bars.
        /// </summary>
        public bool ProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds Error Message for Various Forms.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Holds Password Value.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Holds ConfirmPassword Value.
        /// </summary>
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Holds Active User Currently.
        /// </summary>
        public SystemUser FormUser { get; set; } = new SystemUser();

        /// <summary>
        /// Holds Active User Currently.
        /// </summary>
        public SystemUser SelectedUser { get; set; } = new SystemUser();

        /// <summary>
        /// Holds Role Details in the Form.
        /// </summary>
        public SystemRole FormRole { get; set; } = new SystemRole();

        /// <summary>
        /// Holds Selected Role in the Datagrid.
        /// </summary>
        public SystemRole SelectedRole { get; set; }

        /// <summary>
        /// Holds SnackBar Message Queue
        /// </summary>
        public SnackbarMessageQueue SnackbarMessage { get; set; } = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(1250));

        /// <summary>
        /// Holds a list of all Users in the Database.
        /// </summary>
        public ObservableCollection<SystemUser> SystemUsers { get; set; }

        /// <summary>
        /// Holds a list of all Roles in the Database.
        /// </summary>
        public ObservableCollection<SystemRole> SystemRoles { get; set; }

        /// <summary>
        /// Holds Currently Logged In User.
        /// </summary>
        public SystemUser CurrentUser { get; set; }

        /// <summary>
        /// Holds Currently Logged In User's Role.
        /// </summary>
        public SystemRole CurrentRole { get; set; }
        #endregion

        #region Public Commands
        /// <summary>
        /// Load Image A Selected Image Into the UI.
        /// </summary>
        public RelayCommand OpenImageCommand { get; private set; }

        /// <summary>
        /// Adds a new Data Entry or Updates an Existing One to the Database (And Adjusts the View).
        /// </summary>
        public RelayCommand SaveEntryCommand { get; private set; }

        /// <summary>
        /// Deletes an existing data entry (And Adjusts the View).
        /// </summary>
        public RelayCommand DeleteEntryCommand { get; private set; }
        #endregion

        #region Class Methods
        /// <summary>
        /// Initializes UI Fired Commands.
        /// </summary>
        private void InitializeCommands()
        {
            SwitchContentCommand = new ParameterCommand<string>((param) => SwitchContent(param));
            OpenImageCommand = new RelayCommand(() => OpenImage());
            SaveEntryCommand = new RelayCommand(async () => await SaveEntryAsync());
            DeleteEntryCommand = new RelayCommand(async () => await DeleteEntryAsync());
        }

        /// <summary>
        /// Switches the Tabs or Page Content and Updates the UI.
        /// </summary>
        private void SwitchContent(string tabParameter)
        {
            // Resets Error Message.
            ErrorMessage = null;

            // Updates Page Title and View Depending on the Parameter. 
            switch (tabParameter)
            {
                // Opens the View Users Tab.
                case "ViewUsers":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("UsersTitle1");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle1");
                    ContentAreaTab = 0;
                    break;
                // Opens the Add User Tab.
                case "AddUser":
                    FormUser = new SystemUser();
                    Password = ConfirmPassword = "";
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("UsersTitle2");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle2");
                    ContentAreaTab = 1;
                    break;
                // Opens the Edit User Tab.
                case "EditUser":
                    FormUser = SelectedUser;
                    Password = ConfirmPassword = "";
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("UsersTitle3");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle3");
                    ContentAreaTab = 1;
                    break;
                // Opens the View Roles Tab.
                case "ViewRoles":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("UsersTitle4");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle1");
                    ContentAreaTab = 2;
                    break;
                // Opens the Add Role Tab.
                case "AddRole":
                    FormRole = new SystemRole();
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("UsersTitle5");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle2");
                    ContentAreaTab = 3;
                    break;
                // Opens the Edit Role Tab.
                case "EditRole":
                    FormRole = SelectedRole.ShallowCopy();
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("UsersTitle6");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle3");
                    ContentAreaTab = 3;
                    break;
                // Opens Delete Entry Dialog.
                case "OpenDelete":
                    DialogStatus = true;
                    break;
                // Closes Delete Entry Dialog.
                case "CloseDelete":
                    DialogStatus = false;
                    break;
            }
        }

        /// <summary>
        /// Opens a dialog to select an image and sets it in the Form Entry.
        /// </summary>
        private void OpenImage()
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog
            {
                Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.bmp) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.bmp"
            };
            if (dialog.ShowDialog() == true)
            {
                FormUser.Image = File.ReadAllBytes(dialog.FileName);
                OnPropertyChanged(nameof(FormUser));
            }
        }

        /// <summary>
        /// Adds a new Data Entry or Updates an Existing One to the Database (And Adjusts the View).
        /// </summary>
        private async Task SaveEntryAsync()
        {
            // Turns on Progress Bar to indicate command in action.
            ProgressStatus = true;

            // Resets Error Message.
            ErrorMessage = null;
            switch (_selectedTab)
            {
                case "AddUser":
                    if (FormUser.Username == null || FormUser.Username == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("UsersError1");
                        break;
                    }
                    else if (FormUser.RoleID == 0)
                    {
                        ErrorMessage = (string)Application.Current.FindResource("UsersError2");
                        break;
                    }
                    else if (Password == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("UsersError3");
                        break;
                    } 
                    else if (Password != ConfirmPassword)
                    {
                        ErrorMessage = (string)Application.Current.FindResource("UsersError4");
                        break;
                    }
                    FormUser.ID = await _userDataService.CreateEntry(FormUser);
                    var formPassword = await Task.Run(() => _hasherService.EncryptPassword(Password));
                    formPassword.UserID = FormUser.ID;
                    formPassword.ID = await _passwordDataService.CreateEntry(formPassword);
                    DataMessanger.BroadCast("AddUser", FormUser);
                    SystemUsers.Add(FormUser);
                    SelectedUser = FormUser;
                    SwitchContent("ViewUsers");
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("UsersSnackBar1"));
                    break;

                case "EditUser":
                    if (FormUser.Username == null || FormUser.Username == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("UsersError1");
                        break;
                    }
                    else if (FormUser.RoleID == 0)
                    {
                        ErrorMessage = (string)Application.Current.FindResource("UsersError2");
                        break;
                    }
                    else if (Password != ConfirmPassword)
                    {
                        ErrorMessage = (string)Application.Current.FindResource("UsersError4");
                        break;
                    }
                    else if (Password != "")
                    {
                        var editPassword = await Task.Run(() => _hasherService.EncryptPassword(Password));
                        editPassword.UserID = FormUser.ID;
                        await _passwordDataService.UpdateEntry(editPassword);
                    }
                    await _userDataService.UpdateEntry(FormUser);
                    DataMessanger.BroadCast("EditUser", FormUser);
                    var userIndex = SystemUsers.IndexOf(SelectedUser);
                    SelectedUser = SystemUsers[userIndex] = FormUser;
                    SwitchContent("ViewUsers");
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("UsersSnackBar2"));
                    break;

                case "AddRole":
                    if (FormRole.Name == null || FormRole.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    FormRole.ID = await _roleDataService.CreateEntry(FormRole);
                    SystemRoles.Add(FormRole);
                    SelectedRole = FormRole;
                    SwitchContent("ViewRoles");
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("UsersSnackBar4"));
                    break;

                case "EditRole":
                    if (FormRole.Name == null || FormRole.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    await _roleDataService.UpdateEntry(FormRole);
                    if (FormRole.ID == CurrentRole.ID) DataMessanger.UserSwitch("Login", CurrentUser, FormRole);
                    var roleIndex = SystemRoles.IndexOf(SelectedRole);
                    SelectedRole = SystemRoles[roleIndex] = FormRole;
                    SwitchContent("ViewRoles");
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("UsersSnackBar5"));
                    break;
            }
            // Turns off Progress Bar to indicate command is complete.
            ProgressStatus = false;
        }

        /// <summary>
        /// Deletes an existing data entry (And Adjusts the View).
        /// </summary>
        private async Task DeleteEntryAsync()
        {
            switch (_selectedTab)
            {
                case "ViewUsers":
                    // Does not allow for deletion of current active User.
                    if (SelectedUser.ID == CurrentUser.ID)
                    {
                        DialogStatus = false;
                        SnackbarMessage.Enqueue((string)Application.Current.FindResource("UsersError5"));
                        break;
                    }
                    SystemPassword selectedPassword = await _passwordDataService.Get(x => x.UserID == SelectedUser.ID);
                    await _passwordDataService.DeleteEntry(selectedPassword);
                    await _userDataService.DeleteEntry(SelectedUser);
                    DataMessanger.BroadCast("DeleteUser", SelectedUser);
                    SystemUsers.Remove(SelectedUser);
                    DialogStatus = false;
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("UsersSnackBar3"));
                    break;

                case "ViewRoles":
                    // Does not allow for deletion of current active User's Role.
                    if (SelectedRole.ID == CurrentRole.ID)
                    {
                        DialogStatus = false;
                        SnackbarMessage.Enqueue((string)Application.Current.FindResource("UsersError6"));
                        break;
                    }
                    await _roleDataService.DeleteEntry(SelectedRole);
                    SystemRoles.Remove(SelectedRole);
                    DialogStatus = false;
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("UsersSnackBar6"));
                    break;
            }
        }

        /// <summary>
        /// Refreshs the datagrid entries from the database.
        /// </summary>
        public async Task LoadDataAsync()
        {
            // Loads All role Data From Database.
            List<SystemRole> savedRoles = await _roleDataService.GetAll();
            SystemRoles = new ObservableCollection<SystemRole>(savedRoles);

            // Loads All users Data From Database.
            List<SystemUser> savedUsers = await _userDataService.GetAll();
            SystemUsers = new ObservableCollection<SystemUser>(savedUsers);

            // Gives a reference to other VMs.
            DataMessanger.BroadCast("LoadUsers", SystemUsers);
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Main Class Constructor and Command Initializer
        /// </summary>
        public UsersViewModel(IDataService<SystemUser> userDataService, IDataService<SystemPassword> passwordDataService, IDataService<SystemRole> roleDataService, IHasherService hasherService)
        {
            _userDataService = userDataService;
            _passwordDataService = passwordDataService;
            _roleDataService = roleDataService;
            _hasherService = hasherService;
            InitializeCommands();
            InitializeEvents();
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Initializes all Event Listensers for UsersViewModel.
        /// </summary>
        private void InitializeEvents()
        {
            DataMessanger.OnMessageTransmitted += OnMessageTransmitted;
            DataMessanger.OnLanguageSwitched += OnLanguageSwitched;
            DataMessanger.OnUserSwitched += OnUserSwitched;
        }

        /// <summary>
        /// Updates data depending on message from other classes.
        /// </summary>
        private async void OnMessageTransmitted(string message, object payload)
        {
            switch (message)
            {
                case "FirstTimeRegister":
                    await LoadDataAsync();
                    break;
                case "TabSwitched":
                    DialogStatus = false;
                    break;
            }
        }

        /// <summary>
        /// Refreshes Elements On Language Change.
        /// </summary>
        private void OnLanguageSwitched(string message) => SwitchContent(_selectedTab);

        /// <summary>
        /// Sets current User and Role Data on login event.
        /// </summary>
        private void OnUserSwitched(string message, SystemUser systemUser, SystemRole systemRole)
        {
            CurrentUser = systemUser;
            CurrentRole = systemRole;
            if (message == "Login")
            {
                if (CurrentRole.OpenUsersListTab) SwitchContent("ViewUsers");
                else if (CurrentRole.OpenRolesTab) SwitchContent("ViewRoles");
            }    
        }
        #endregion
    }
}
