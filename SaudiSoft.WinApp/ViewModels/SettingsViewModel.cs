﻿using SaudiSoft.Core;
using MaterialDesignThemes.Wpf;
using System;
using System.IO;
using System.Linq;
using System.Windows;

namespace SaudiSoft.WinApp
{
    public class SettingsViewModel : ViewModelBase
    {
        #region Public Properties
        /// <summary>
        /// Holds ContentArea's Current Selected Page Number.
        /// </summary>
        public int ContentAreaTab { get; set; } = 0;

        /// <summary>
        /// Holds Items Page Main Title.
        /// </summary>
        public string MainPageTitle { get; set; } = (string)Application.Current.FindResource("SettingsTitle1");

        /// <summary>
        /// Holds Items Page Secondary Title.
        /// </summary>
        public string SecPageTitle { get; set; } = (string)Application.Current.FindResource("SettingsSubTitle1");

        /// <summary>
        /// Gets and Holds the Value of the Current Version.
        /// </summary>
        public string CurrentVersion
        {
            get { return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(); }
        }

        /// <summary>
        /// Gets and Holds the Value of the Current Year.
        /// </summary>
        public string CurrentYear
        {
            get { return DateTime.Now.Year.ToString(); }
        }

        /// <summary>
        /// Holds list of Languages
        /// </summary>
        public string[] LanguageList { get; set; } = { "English", "العربية" };

        /// <summary>
        /// Holds the Current Index Value Locally.
        /// </summary>
        public int LocalCurrencyIndex { get; set; } = 0;

        /// <summary>
        /// Holds list of Languages
        /// </summary>
        public string[] CurrencyList { get; set; } = (string[])Application.Current.FindResource("CurrencyList");

        /// <summary>
        /// Status of the Restore Message Info Message.
        /// </summary>
        public bool RestoreMessage { get; set; } = false;

        /// <summary>
        /// Holds CurrentSettingss Details.
        /// </summary>
        public AppSettings CurrentSettings { get; set; } = AppSettings.Load();

        /// <summary>
        /// Holds SnackBar Message Queue
        /// </summary>
        public SnackbarMessageQueue SnackbarMessage { get; set; } = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(1250));
        #endregion

        #region Public Commands
        /// <summary>
        /// Receipt Print Preview Command.
        /// </summary>
        public RelayCommand SaveDetailsCommand { get; private set; }

        /// <summary>
        /// Backup Database Command.
        /// </summary>
        public RelayCommand BackupCommand { get; private set; }

        /// <summary>
        /// Restore Database Command.
        /// </summary>
        public RelayCommand RestoreCommand { get; private set; }

        /// <summary>
        /// Opens Company Website In Browser.
        /// </summary>
        public RelayCommand OpenWebsiteCommand { get; private set; }
        #endregion

        #region Class Methods
        /// <summary>
        /// Initializes UI Fired Commands.
        /// </summary>
        private void InitializeCommands()
        {
            SwitchContentCommand = new ParameterCommand<string>((param) => SwitchContent(param));
            SaveDetailsCommand = new RelayCommand(() => SaveSettingsReceipt());
            BackupCommand = new RelayCommand(() => BackupDatabase());
            RestoreCommand = new RelayCommand(() => RestoreDatabase());
            OpenWebsiteCommand = new RelayCommand(() => OpenWebsiteLink());
        }

        /// <summary>
        /// Switchs the Tabs or Page Content and Updates the UI.
        /// </summary>
        private void SwitchContent(string tabParameter)
        {
            switch (tabParameter)
            {
                // Opens View Settings Tab & Changes Tab Titles.
                case "Settings":
                    MainPageTitle = (string)Application.Current.FindResource("SettingsTitle1");
                    SecPageTitle = (string)Application.Current.FindResource("SettingsSubTitle1");
                    ContentAreaTab = 0;
                    break;
                // Opens About Us Tab & Changes Tab Titles.
                case "About":
                    MainPageTitle = (string)Application.Current.FindResource("SettingsTitle2");
                    SecPageTitle = (string)Application.Current.FindResource("SettingsSubTitle2");
                    ContentAreaTab = 1;
                    break;
            }
        }

        /// <summary>
        /// Shows a print preview receipt.
        /// </summary>
        private void SaveSettingsReceipt()
        {
            // Locks button to indicate command in action.
            SaveDetailsCommand.CanExecutePredicate = false;

            // Saves Current Setting Data on Display.
            CurrentSettings.Save();

            // Unlocks button to indicate command in complete.
            SaveDetailsCommand.CanExecutePredicate = true;
        }

        /// <summary>
        /// Backups A copy of the local database file.
        /// </summary>
        private void BackupDatabase()
        {
            // Locks button to indicate command in action.
            BackupCommand.CanExecutePredicate = false;

            // Opens a SaveFileDialog to get a location from the user.
            var dialog = new Microsoft.Win32.SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                FileName = "POS_Data_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"),
                Filter = "Database File (*.db, *.sqlite3) | *.db; *.sqlite3"
            };

            // Create a copy of current database file to selected location.
            if (dialog.ShowDialog() == true && File.Exists(DataPath.localDBFile))
            {
                File.Copy(DataPath.localDBFile, dialog.FileName);
                SnackbarMessage.Enqueue((string)Application.Current.FindResource("SettingsSnackBar1"));
            }

            // Unlocks button to indicate command in complete.
            BackupCommand.CanExecutePredicate = true;
        }

        /// <summary>
        /// Restores/replaces the local database with a given File.
        /// </summary>
        private void RestoreDatabase()
        {
            // Locks button to indicate command in action.
            RestoreCommand.CanExecutePredicate = false;

            RestoreMessage = false;
            // Opens an OpenFileDialog to get a file from the user.
            var dialog = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "Database File (*.db, *.sqlite3) | *.db; *.sqlite3"
            };

            // Replace current database of the program with it.
            if (dialog.ShowDialog() == true)
            {
                File.Copy(dialog.FileName, DataPath.localDBFile, true);
                SnackbarMessage.Enqueue((string)Application.Current.FindResource("SettingsSnackBar2"));
                RestoreMessage = true;
            }

            // Unlocks button to indicate command in complete.
            RestoreCommand.CanExecutePredicate = true;
        }

        /// <summary>
        /// Opens the Program Website.
        /// </summary>
        public void OpenWebsiteLink()
        {
            var processInfo = new System.Diagnostics.ProcessStartInfo
            {
                FileName = "https://saudi-soft.laman.sa/",
                UseShellExecute = true
            };
            System.Diagnostics.Process.Start(processInfo);
        }

        /// <summary>
        /// Sets the Item Display Option.
        /// </summary>
        public void SetItemDisplayOption()
        {
            CurrentSettings.Key = AppSettings.Load().Key;
            CurrentSettings.RegisterDate = AppSettings.Load().RegisterDate;
            // Saves changes to settings
            CurrentSettings.Save();

            // Tells Other ViewModels of the Changes to this Settings.
            DataMessanger.BroadCast("SetItemDisplayOption", CurrentSettings.ItemDisplayOption);
        }

        /// <summary>
        /// Sets the Language for the Entire Application.
        /// </summary>
        public void SetLanguage()
        {
            // Initializes a variable to hold new language name to be broadcasted.
            string languageName;
            CurrentSettings.Key = AppSettings.Load().Key;
            CurrentSettings.RegisterDate = AppSettings.Load().RegisterDate;
            // Loads the new selected language.
            var dictionary = new ResourceDictionary();
            switch (CurrentSettings.LanguageIndex)
            {
                case 1:
                    dictionary.Source = new Uri("pack://application:,,,/Localization/Language_ar-EG.xaml");
                    languageName = "English";
                    break;
                default:
                    dictionary.Source = new Uri("pack://application:,,,/Localization/Language_en-US.xaml");
                    languageName = "Arabic";
                    break;
            }
            Application.Current.Resources.MergedDictionaries.Add(dictionary);

            // Removes Current Loaded Language.
            var currentResource = Application.Current.Resources.MergedDictionaries.FirstOrDefault(x => x.Source != null && x.Source.ToString().Contains("Localization"));
            if (currentResource != null)
                Application.Current.Resources.MergedDictionaries.Remove(currentResource);

            // Updates Currency List.
            LocalCurrencyIndex = CurrentSettings.CurrencyIndex;
            CurrentSettings.Key = AppSettings.Load().Key;
            CurrentSettings.RegisterDate = AppSettings.Load().RegisterDate;

            // Saves changes to settings
            CurrentSettings.Save();

            // Broadcast LangugeChange to other listening classes.
            DataMessanger.LanguageSwitch(languageName);

            // Refreshes UI titles.
            SwitchContent("Settings");
        }

        /// <summary>
        /// Updates the Currency Being displayed all around the application.
        /// </summary>
        public void SetCurrency()
        {
            if (LocalCurrencyIndex != -1)
            {
                // Saves New Currency Index
                CurrentSettings.CurrencyIndex = LocalCurrencyIndex;
                CurrentSettings.Key = AppSettings.Load().Key;
                CurrentSettings.RegisterDate = AppSettings.Load().RegisterDate;
                CurrentSettings.Save();

                // Get the languague specific name for to the current resource.
                string[] currencyList = (string[])Application.Current.FindResource("CurrencyAbbreviationsList");
                string currentCurrency = currencyList[LocalCurrencyIndex];

                // Dynamic Resources that need to have their currency updated.
                var defaultResources = new ResourceDictionary();
                switch (CurrentSettings.LanguageIndex)
                {
                    case 1:
                        defaultResources.Source = new Uri("pack://application:,,,/Localization/Language_ar-EG.xaml");
                        break;
                    default:
                        defaultResources.Source = new Uri("pack://application:,,,/Localization/Language_en-US.xaml");
                        break;
                }
                Application.Current.Resources["WordCurrency"] = currentCurrency;
                Application.Current.Resources["DBAttributePrice"] = defaultResources["DBAttributePrice"] + " (" + currentCurrency+ ")";
                Application.Current.Resources["DBAttributeSupplierPrice"] = defaultResources["DBAttributeSupplierPrice"] + " (" + currentCurrency+ ")";
                Application.Current.Resources["DBAttributeTaxedPrice"] = defaultResources["DBAttributeTaxedPrice"] + " (" + currentCurrency+ ")";
                Application.Current.Resources["DBAttributeSalesPrice"] = defaultResources["DBAttributeSalesPrice"] + " (" + currentCurrency+ ")";
                Application.Current.Resources["DBAttributeAmount"] = defaultResources["DBAttributeAmount"] + " (" + currentCurrency+ ")";
                Application.Current.Resources["DBAttributeAmountDue"] = defaultResources["DBAttributeAmountDue"] + " (" + currentCurrency+ ")";
                Application.Current.Resources["DBAttributePaidAmount"] = defaultResources["DBAttributePaidAmount"] + " (" + currentCurrency+ ")";
                Application.Current.Resources["ItemReportsSubTitle2"] = defaultResources["ItemReportsSubTitle2"] + " (" + currentCurrency+ ")";
                Application.Current.Resources["ItemReportsSubTitle3"] = defaultResources["ItemReportsSubTitle3"] + " (" + currentCurrency+ ")";
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default Class Constructor and Command Initializer
        /// </summary>
        public SettingsViewModel()
        {
            InitializeCommands();
            // If language is not set to english (Default), loads the new language.
            // This is mainly so it does not reload english. (Useful for debug/design in Visual Studio)
            if (CurrentSettings.LanguageIndex != 0) SetLanguage();
            if (CurrentSettings.CurrencyIndex != 0) LocalCurrencyIndex = CurrentSettings.CurrencyIndex;
            if (CurrentSettings.ItemDisplayOption == false) SetItemDisplayOption();
        }
        #endregion
    }
}
