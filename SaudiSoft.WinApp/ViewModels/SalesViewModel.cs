﻿using SaudiSoft.Core;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SaudiSoft.WinApp
{
    public class SalesViewModel : ViewModelBase
    {
        #region Private Services
        private readonly IDataService<StockItem> _itemDataService;
        private readonly IDataService<StockCategory> _categoryDataService;
        private readonly IDataService<StoreCustomer> _customerDataService;
        private readonly IDataService<SalesTicket> _ticketDataService;
        private readonly IDataService<SalesItem> _ticketItemDataService;
        private readonly IPrintSalesService _printSalesService;
        private readonly ICustomerPaymentService _customerPaymentService;
        #endregion

        #region Private Members
        // Holds Cart 1 Data. (Accessed By Property)
        private ObservableCollection<SalesItem> _cart1Items = new ObservableCollection<SalesItem>();
        // Holds Cart 2 Data. (Accessed By Property)
        private ObservableCollection<SalesItem> _cart2Items = new ObservableCollection<SalesItem>();
        // Holds Cart 3 Data. (Accessed By Property)
        private ObservableCollection<SalesItem> _cart3Items = new ObservableCollection<SalesItem>();
        // Holds Cart 4 Data. (Accessed By Property)
        private ObservableCollection<SalesItem> _cart4Items = new ObservableCollection<SalesItem>();
        // Holds Cart 5 Data. (Accessed By Property)
        private ObservableCollection<SalesItem> _cart5Items = new ObservableCollection<SalesItem>();
        // Holds Cart 1 Ticket Data. (Accessed By Property)
        private SalesTicket _cart1Ticket = new SalesTicket();
        // Holds Cart 2 Ticket Data. (Accessed By Property)
        private SalesTicket _cart2Ticket = new SalesTicket();
        // Holds Cart 3 Ticket Data. (Accessed By Property)
        private SalesTicket _cart3Ticket = new SalesTicket();
        // Holds Cart 4 Ticket Data. (Accessed By Property)
        private SalesTicket _cart4Ticket = new SalesTicket();
        // Holds Cart 5 Ticket Data. (Accessed By Property)
        private SalesTicket _cart5Ticket = new SalesTicket();
        // Holds selected Tab Name.
        private string _selectedTab = "POS";
        #endregion

        #region Public Properties
        /// <summary>
        /// Holds the Sales Transistioner's Current Selected Page Number.
        /// </summary>
        public int ContentAreaTab { get; set; } = 0;
        
        /// <summary>
        /// Holds the Item Display Option
        /// </summary>
        public bool ItemDisplayOption { get; set; } = true;

        /// <summary>
        /// Holds SalesPage Main Title.
        /// </summary>
        public string MainPageTitle { get; set; } = (string)Application.Current.FindResource("SalesTitle1");

        /// <summary>
        /// Holds SalesPage Secondary Title.
        /// </summary>
        public string SecPageTitle { get; set; } = (string)Application.Current.FindResource("SalesSubTitle1");

        /// <summary>
        /// Holds all StockCategories currently in the database.
        /// </summary>
        public ObservableCollection<StockCategory> StockCategories { get; private set; }

        /// <summary>
        /// Holds all StockItems currently in the database.
        /// </summary>
        public ObservableCollection<StockItem> AllStockItems { get; private set; }

        /// <summary>
        /// Holds subset of StockItems on Display.
        /// </summary>
        public ObservableCollection<StockItem> StockItems { get; set; }

        /// <summary>
        /// Holds Index of which ItemsCart is on Display.
        /// </summary>
        public int SelectedCartIndex { get; set; } = 0;

        /// <summary>
        /// Holds filter Keyword/Name for StoreItems.
        /// </summary>
        public string FilterKeyword { get; set; } = "";

        /// <summary>
        /// Holds filter Category for StoreItems on Display.
        /// </summary>
        public StockCategory FilterCategory { get; set; }

        /// <summary>
        /// Holds Searchkeyword (Barcode) of a StockItem, Calls a search if it holds 13 characters.
        /// </summary>
        public string SearchKeyword { get; set; }

        /// <summary>
        /// Holds Selected Salesitem/Cartitem in Cart.
        /// </summary>
        public SalesItem ActiveSalesItem { get; set; }

        /// <summary>
        /// Payment Dialog IsOpen Status.
        /// </summary>
        public bool DialogStatus { get; set; }

        /// <summary>
        /// Holds Command Loading Progress Bars.
        /// </summary>
        public bool ProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds SalesItems Currently in the Cart.
        /// </summary>
        public ObservableCollection<SalesItem> CartItems
        {
            get
            {
                switch (SelectedCartIndex)
                {
                    default: return _cart1Items;
                    case 1: return _cart2Items;
                    case 2: return _cart3Items;
                    case 3: return _cart4Items;
                    case 4: return _cart5Items;
                }
            }
            set
            {
                switch (SelectedCartIndex)
                {
                    default: _cart1Items = value; break;
                    case 1: _cart2Items = value; break;
                    case 2: _cart3Items = value; break;
                    case 3: _cart4Items = value; break;
                    case 4: _cart5Items = value; break;
                }
            }
        }

        /// <summary>
        /// Holds CartTicket which is Important Cart Details.
        /// </summary>
        public SalesTicket CartTicket
        {
            get
            {
                switch (SelectedCartIndex)
                {
                    default: return _cart1Ticket;
                    case 1: return _cart2Ticket;
                    case 2: return _cart3Ticket;
                    case 3: return _cart4Ticket;
                    case 4: return _cart5Ticket;
                }
            }
            set
            {
                switch (SelectedCartIndex)
                {
                    default: _cart1Ticket = value; break;
                    case 1: _cart2Ticket = value; break;
                    case 2: _cart3Ticket = value; break;
                    case 3: _cart4Ticket = value; break;
                    case 4: _cart5Ticket = value; break;
                }
            }
        }

        /// <summary>
        /// Currently Logged In User.
        /// </summary>
        public SystemUser CurrentUser { get; set; }

        /// <summary>
        /// Currently Logged In Role.
        /// </summary>
        public SystemRole CurrentRole { get; set; }

        /// <summary>
        /// Holds SnackBar Message Queue
        /// </summary>
        public SnackbarMessageQueue SnackbarMessage { get; set; } = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(1400));

        /// <summary>
        /// Holds Sales List From Date Filter.
        /// </summary>
        public DateTime SalesFromDateFilter { get; set; } = DateTime.Today.AddMonths(-1);

        /// <summary>
        /// Holds Sales List To Date Filter.
        /// </summary>
        public DateTime SalesToDateFilter { get; set; } = DateTime.Today;

        /// <summary>
        /// Holds Sales Customer Filter.
        /// </summary>
        public StoreCustomer SalesCustomerFilter { get; set; }

        /// <summary>
        /// Holds Sales User Filter.
        /// </summary>
        public SystemUser SalesUserFilter { get; set; }

        /// <summary>
        /// Holds Selected Ticket in Sales List Datagird.
        /// </summary>
        public SalesTicket SelectedTicket { get; set; }

        /// <summary>
        /// Holds Form Ticket Values in Edit Sales Form.
        /// </summary>
        public SalesTicket FormTicket { get; set; } = new SalesTicket();

        /// <summary>
        /// Holds FormItems of the FormTicket On Display.
        /// </summary>
        public ObservableCollection<SalesItem> SelectedTicketItems { get; set; }

        /// <summary>
        /// Holds FormItems of the FormTicket On Display.
        /// </summary>
        public ObservableCollection<SalesItem> FormTicketItems { get; set; } = new ObservableCollection<SalesItem>();

        /// <summary>
        /// Holds SalesTickets On Display.
        /// </summary>
        public ObservableCollection<SalesTicket> SalesTickets { get; set; }

        /// <summary>
        /// Holds all SystemUsers Data.
        /// </summary>
        public ObservableCollection<SystemUser> SystemUsers { get; set; } = new ObservableCollection<SystemUser>();

        /// <summary>
        /// Holds all StoreCustomers Data.
        /// </summary>
        public ObservableCollection<StoreCustomer> StoreCustomers { get; set; }

        /// <summary>
        /// Holds Sale Customer Data.
        /// </summary>
        public StoreCustomer SaleCustomer { get; set; }

        /// <summary>
        /// Holds Filter Keyword/Name for Customers.
        /// </summary>
        public string FilterCustomerKeyword { get; set; } = "";

        /// <summary>
        /// Delete Dialog IsOpen Status.
        /// </summary>
        public bool DeleteDialogStatus { get; set; } = false;

        /// <summary>
        /// Holds Filter StoreCustomers Data.
        /// </summary>
        public ObservableCollection<StoreCustomer> FilterStoreCustomers { get; set; }

        /// <summary>
        /// Holds Filtered StoreCustomers Data.
        /// </summary>
        public ObservableCollection<StoreCustomer> FilteredStoreCustomers { get; set; }

        /// <summary>
        /// Holds Selected Customer Data.
        /// </summary>
        public StoreCustomer SelectedCustomer { get; set; }

        /// <summary>
        /// Holds Form Customer Data.
        /// </summary>
        public StoreCustomer FormCustomer { get; set; } = new StoreCustomer();

        /// <summary>
        /// Holds Form Customer Payment Amount
        /// </summary>
        public double FormCustomerPayment { get; set; }

        /// <summary>
        /// Holds Error Message for Various Forms.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Holds status for Excel Export Progress Bars.
        /// </summary>
        public ObservableCollection<bool> ExcelProgressStatus { get; set; } = new ObservableCollection<bool> { false, false };
        #endregion

        #region Public Commands
        /// <summary>
        /// Load Data From Database.
        /// </summary>
        public RelayCommand LoadDataCommand { get; private set; }

        /// <summary>
        /// Filters StockItems on Display.
        /// </summary>
        public RelayCommand FilterItemsCommand { get; private set; }

        /// <summary>
        /// Add a Item To Active Cart.
        /// </summary>
        public ParameterCommand<StockItem> AddToCartCommand { get; private set; }

        /// <summary>
        /// Removes a Item From Active Cart.
        /// </summary>
        public RelayCommand RemoveCartItemCommand { get; private set; }

        /// <summary>
        /// Clears Current Cart.
        /// </summary>
        public RelayCommand ClearCartCommand { get; private set; }

        /// <summary>
        /// Updates CartTicket Depending on UI.
        /// </summary>
        public RelayCommand UpdateTicketCommand { get; private set; }

        /// <summary>
        /// Checksout CartItems When Fired.
        /// </summary>
        public RelayCommand CheckoutCommand { get; private set; }

        /// <summary>
        /// Checksout CartItems When Fired.
        /// </summary>
        public RelayCommand CheckoutPrintCommand { get; private set; }

        /// <summary>
        /// Filters SalesTickets on Display.
        /// </summary>
        public RelayCommand FilterSalesCommand { get; private set; }

        /// <summary>
        /// Prints Selected SalesTicket on Display.
        /// </summary>
        public RelayCommand PrintSalesCommand { get; private set; }

        /// <summary>
        /// Edit Selected SalesTicket on Display.
        /// </summary>
        public RelayCommand EditEntryCommand { get; private set; }

        /// <summary>
        /// Edit Form SalesTicket on Display.
        /// </summary>
        public RelayCommand SaveEntryCommand { get; private set; }

        /// <summary>
        /// Deletes Form Entry on Display.
        /// </summary>
        public RelayCommand DeleteEntryCommand { get; private set; }

        /// <summary>
        /// Filters Customer Data Depending on Name.
        /// </summary>
        public RelayCommand FilterCustomersCommand { get; private set; }

        /// <summary>
        /// Exports DataGrid Data To Excel.
        /// </summary>
        public RelayCommand ExportExcelCommand { get; private set; }
        public AppSettings _CurrentSettings { get; private set; }
        #endregion

        #region Private Functions / Methods
        /// <summary>
        /// Initializes UI Fired Commands.
        /// </summary>
        private void InitializeCommands()
        {
            SwitchContentCommand = new ParameterCommand<string>((param) => SwitchContent(param));
            FilterItemsCommand = new RelayCommand(() => FilterItems());
            AddToCartCommand = new ParameterCommand<StockItem>((param) => AddToCart(param));
            RemoveCartItemCommand = new RelayCommand(() => CartItems.Remove(ActiveSalesItem));
            ClearCartCommand = new RelayCommand(() => ClearCart());
            UpdateTicketCommand = new RelayCommand(() => UpdateTicket());
            CheckoutCommand = new RelayCommand(async () => await Checkout());
            CheckoutPrintCommand = new RelayCommand(async () => await Checkout(true));
            FilterSalesCommand = new RelayCommand(async () => await GetSalesAsync());
            PrintSalesCommand = new RelayCommand(async () => await PrintSalesAsync());
            EditEntryCommand = new RelayCommand(async () => await EditEntryAsync());
            SaveEntryCommand = new RelayCommand(async () => await SaveEntryAsync());
            DeleteEntryCommand = new RelayCommand(async () => await DeleteEntryAsync());
            FilterCustomersCommand = new RelayCommand(() => FilterCustomers());
            ExportExcelCommand = new RelayCommand(async () => await Task.Run(() => ExportToExcel()));
            _CurrentSettings = AppSettings.Load();
            CartTicket.Tax = _CurrentSettings.TaxValue;
            FormTicket.Tax = _CurrentSettings.TaxValue;
        }

        /// <summary>
        /// Switchs the Tabs or Page Content and Updates the UI.
        /// </summary>
        private void SwitchContent(string tabParameter)
        {
            switch (tabParameter)
            {
                // Opens View Items Tab & Changes Tab Titles.
                case "POS":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("SalesTitle1");
                    SecPageTitle = (string)Application.Current.FindResource("SalesSubTitle1");
                    ContentAreaTab = 0;
                    break;
                // Opens Add Item Tab & Changes Tab Titles.
                case "SalesList":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("SalesTitle2");
                    SecPageTitle = (string)Application.Current.FindResource("SalesSubTitle2");
                    ContentAreaTab = 1;
                    break;
                case "EditSale":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("SalesTitle3");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle3");
                    ContentAreaTab = 2;
                    break;
                // Opens View Customers List Tab & Changes Tab Titles.
                case "CustomersList":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("CustomersTitle1");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle1");
                    ContentAreaTab = 3;
                    break;
                // Opens Add Customer Tab, Resets Form Values & Changes Tab Titles.
                case "AddCustomer":
                    FormCustomer = new StoreCustomer();
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("CustomersTitle2");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle2");
                    ContentAreaTab = 4;
                    break;
                // Opens Edit Customer Tab & Changes Tab Titles.
                case "EditCustomer":
                    FormCustomer = SelectedCustomer.ShallowCopy();
                    FormCustomerPayment = 0;
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("CustomersTitle3");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle3");
                    ContentAreaTab = 4;
                    break;
                // Opens Payment Dialog.
                case "OpenPayment":
                    DialogStatus = true;
                    break;
                // Closes Payment Dialog.
                case "CancelPayment":
                    DialogStatus = false;
                    break;
                // Opens Delete Entry Dialog.
                case "OpenDelete":
                    DeleteDialogStatus = true;
                    break;
                // Closes Delete Entry Dialog.
                case "CloseDelete":
                    DeleteDialogStatus = false;
                    break;
            }
        }

        /// <summary>
        /// Adds a Stockitem to the current Cart
        /// </summary>
        /// <param name="givenItem">Stockitem to be added to cart</param>
        private void AddToCart(StockItem givenItem)
        {
            // Gets givenItem from Barcode (SearchKeyword), If there is not givenItem Passed to it.
            if (givenItem == null)
            {
                givenItem = StockItems.FirstOrDefault(x => x.Barcode == SearchKeyword) ?? new StockItem();
                SearchKeyword = "";
            };

            // Searches if the Item Exists in the Code.
            var existingCartItem = CartItems.FirstOrDefault(x => x.StockItemID == givenItem.ID) ?? null;
            // Another Check that item exists.
            if (givenItem.ID == 0) SnackbarMessage.Enqueue((string)Application.Current.FindResource("SalesSnackBar2"));
            // if the quantity is 0, it gives a warning that it cant add the item.
            else if (givenItem.Quantity == 0) SnackbarMessage.Enqueue((string)Application.Current.FindResource("SalesSnackBar1"));
            // If the item is not in the Cart it Adds it.
            else if (existingCartItem == null)
            {
                var CartItem = new SalesItem()
                {
                    StockItemID = givenItem.ID,
                    StockItemName = givenItem.Name,
                    Quantity = 1,
                    Price = givenItem.SalesPrice,
                    Tax=givenItem.Tax
                };
                CartItems.Add(CartItem);
            }
            // else increases quantity by 1 if givenItem quantity permits.
            else if (existingCartItem.Quantity < givenItem.Quantity)
            {
                // Best Way to Update DataGrid UI was to remove then add updated entry.
                CartItems.Remove(existingCartItem);
                existingCartItem.Quantity += 1;
                CartItems.Add(existingCartItem);
            }
        }

        /// <summary>
        /// Clears slected Cart and Resets Discount Value for selected Cart.
        /// </summary>
        public void ClearCart()
        {
            SaleCustomer = StoreCustomers[0];
            CartTicket.Discount = 0;
            CartTicket.CustomerID = 0;
            CartItems.Clear();
        }

        /// <summary>
        /// Refreshs the items entries from allStockItems depending on filters.
        /// </summary>
        public void FilterItems()
        {
            // If filtercategory does not exist or equal to 0, only filter is the FilterKeyword.
            if (FilterCategory == null || FilterCategory.ID == 0)
                StockItems = new ObservableCollection<StockItem>(AllStockItems.Where(x => x.Name.ToLower().Contains(FilterKeyword.ToLower()) || x.Barcode == FilterKeyword));
            // else use both the category and name/keyword filters.
            else
                StockItems = new ObservableCollection<StockItem>(AllStockItems.Where(x => x.CategoryID == FilterCategory.ID && (x.Name.ToLower().Contains(FilterKeyword.ToLower()) || x.Barcode == FilterKeyword)));
        }


        /// <summary>
        /// Updates the current SalesTicket Data.
        /// </summary>
        public void UpdateTicket()
        {
            switch(ContentAreaTab)
            {
                case 0:
                    CartTicket.SubTotal = CartItems.Sum(x => x.Price * x.Quantity);
                    CartTicket.Entries = CartItems.Count;
                    OnPropertyChanged(nameof(CartTicket));
                    break;
                case 2:
                    FormTicket.SubTotal = FormTicketItems.Sum(x => x.Price * x.Quantity);
                    OnPropertyChanged(nameof(FormTicket));
                    break;
            }
        }

        /// <summary>
        /// Filters Customers Depending on Given Filters.
        /// </summary>
        private void FilterCustomers()
        {
            FilteredStoreCustomers = new ObservableCollection<StoreCustomer>();
            foreach(var customer in StoreCustomers)
            {
                var searchTerm = FilterCustomerKeyword.ToLower();
                if (customer.ID == 0) continue;
                else if (customer.Name != null && customer.Name.ToLower().Contains(searchTerm)) FilteredStoreCustomers.Add(customer);
                else if (customer.Email != null && customer.Email.ToLower().Contains(searchTerm)) FilteredStoreCustomers.Add(customer);
                else if (customer.Phone != null && customer.Phone.ToLower().Contains(searchTerm)) FilteredStoreCustomers.Add(customer);
                else if (customer.Mobile != null && customer.Mobile.ToLower().Contains(searchTerm)) FilteredStoreCustomers.Add(customer);
            }
        }

        /// <summary>
        /// Exports a copy of the data on display into an Excel File.
        /// </summary>
        private void ExportToExcel()
        {
            bool isSuccessful;
            // Turns on Progress Bar, Exports Entries, Turn off Progress Bar, then report completion with a message.
            switch (_selectedTab)
            {
                case "SalesList":
                    ExcelProgressStatus[0] = true;
                    isSuccessful = ExcelExporter.ExportSales(SalesTickets, StoreCustomers, SystemUsers);
                    ExcelProgressStatus[0] = false;
                    if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("SalesSnackBar3"));
                    break;
                case "CustomersList":
                    ExcelProgressStatus[1] = true;
                    isSuccessful = ExcelExporter.ExportCustomers(StoreCustomers);
                    ExcelProgressStatus[1] = false;
                    if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("CustomersSnackBar4"));
                    break;
            }
        }

        /// <summary>
        /// Gets a a list of the Sales Tickets Depending on Date Range Filter
        /// </summary>
        private async Task GetSalesAsync()
        {
            // Gets SalesTicket Data Depending on From and To Date Filters.
            DateTime FromDate = SalesFromDateFilter;
            DateTime ToDate = SalesToDateFilter.AddDays(1);

            List<SalesTicket> savedTickets;
            if (SalesCustomerFilter.ID == 0 && SalesUserFilter.ID == 0)
            {
                savedTickets = await _ticketDataService.GetAll(predicate: x => x.SaleDate >= FromDate && x.SaleDate <= ToDate, orderBy: x => x.ID);
            }
            else if (SalesCustomerFilter.ID == 0 && SalesUserFilter.ID != 0)
            {
                savedTickets = await _ticketDataService.GetAll(predicate: x => x.SaleDate >= FromDate && x.SaleDate <= ToDate && x.SoldBy == SalesUserFilter.ID, orderBy: x => x.ID);
            }
            else if (SalesCustomerFilter.ID != 0 && SalesUserFilter.ID == 0)
            {
                savedTickets = await _ticketDataService.GetAll(predicate: x => x.SaleDate >= FromDate && x.SaleDate <= ToDate && x.CustomerID == SalesCustomerFilter.ID, orderBy: x => x.ID);
            }
            else
            {
                savedTickets = await _ticketDataService.GetAll(predicate: x => x.SaleDate >= FromDate && x.SaleDate <= ToDate && x.SoldBy == SalesUserFilter.ID && x.CustomerID == SalesCustomerFilter.ID, orderBy: x => x.ID);
            }
            SalesTickets = new ObservableCollection<SalesTicket>(savedTickets);
        }

        /// <summary>
        /// Loads Selected SalesTicket Data into Edit Form.
        /// </summary>
        private async Task EditEntryAsync()
        {
            // Gets the SalesItems of the Ticket.
            List<SalesItem> salesItems = await _ticketItemDataService.GetAll(x => x.SalesTicketID == SelectedTicket.ID) ;
            SelectedTicketItems = new ObservableCollection<SalesItem>(salesItems);

            // Clones SelectedTicket to FormTicket.
            FormTicket = SelectedTicket.ShallowCopy();
            FormTicket.Tax = _CurrentSettings.TaxValue;
            FormTicketItems.Clear();

            // Clones SelectedTicketItems to FormTicketItems.
            foreach (var entry in SelectedTicketItems) 
            {
                FormTicketItems.Add(entry.ShallowCopy()); 
            }

            SwitchContent("EditSale");
        }
        
        /// <summary>
        /// Prints a recipt of SelectedTicket in Sales List.
        /// </summary>
        private async Task PrintSalesAsync()
        {
            // Locks print Sales Command Until it is finished.
            PrintSalesCommand.CanExecutePredicate = false;

            // Gets SalesItems of the Ticket.
            List<SalesItem> salesItems = await _ticketItemDataService.GetAll(predicate: x => x.SalesTicketID == SelectedTicket.ID, orderBy: x => x.ID);
            SelectedTicketItems = new ObservableCollection<SalesItem>(salesItems);

            // Gets StockItem Names.
            foreach (var item in SelectedTicketItems)
            {
                item.StockItemName = (string)Application.Current.FindResource("WordUnknown");
                var stockItem = AllStockItems.FirstOrDefault(x => x.ID == item.StockItemID);
                if (stockItem != null) item.StockItemName = stockItem.Name;
            }

            // Gets the cashier name.
            string cashierName = SystemUsers.FirstOrDefault(x => x.ID == SelectedTicket.SoldBy).Name ?? "";
            string paymentName = ((string[])Application.Current.FindResource("SalesPaymentMethod"))[(int)SelectedTicket.PayMethod];
            var dialog = SetupExportDialog("Bill_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
            //  if (dialog.ShowDialog() == true)
            // {
            var currency = (string[])Application.Current.FindResource("CurrencyAbbreviationsList");
                if (!_printSalesService.Print(SelectedTicket, SelectedTicketItems, paymentName, cashierName, currency[AppSettings.Load().CurrencyIndex]))
                {
                    MessageBox.Show(Application.Current.FindResource("ErrorPrint").ToString());
                }
           // }
            // Unlocks print Sales command.
            PrintSalesCommand.CanExecutePredicate = true;
        }
        private static Microsoft.Win32.SaveFileDialog SetupExportDialog(string fileName)
        {
            var dialog = new Microsoft.Win32.SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "PDF File (*.pdf) | *.pdf",
                FileName = fileName,
            };
            return dialog;
        }
        /// <summary>
        /// Updates the Database entries of the changes to SalesTicket.
        /// </summary>
        /// <returns></returns>
        private async Task SaveEntryAsync()
        {
            // Turns on Progress Bar to indicate command in action.
            ProgressStatus = true;
            // Resets Error Message.
            ErrorMessage = null;
            switch (_selectedTab)
            {
                case "EditSale":
                    // Calculates discounted Total Price. (Saves it as TotalPaid if it was Payed by non-cash methods)
                    var discountedTotal = FormTicket.CalculatedTotalPrice;

                    // Saves discounted Total as TotalPaid if it was Payed by non-cash methods
                    if (FormTicket.TotalPaid >= discountedTotal) FormTicket.TotalPaid = discountedTotal;

                    // Saves the FormTicket Changes to UI and Updates the UI.
                    FormTicket.Editedby = CurrentUser.ID;
                    await _ticketDataService.UpdateEntry(FormTicket);

                    var payAmountChange = discountedTotal - SelectedTicket.CalculatedTotalPrice;
                    var paidAmountChange = FormTicket.TotalPaid - SelectedTicket.TotalPaid;

                    // Updates Selected Customer For Sale.
                    var saleCustomer = StoreCustomers.FirstOrDefault(x => x.ID == FormTicket.CustomerID) ?? new StoreCustomer();
                    saleCustomer.PurchaseDue += payAmountChange;
                    saleCustomer.PaidPurchaseDue += paidAmountChange;
                    await _customerDataService.UpdateEntry(saleCustomer);
                    if (saleCustomer.ID != 0)
                    {
                        FilterCustomers();
                    }

                    // Updates Shift Data.
                    DataMessanger.UpdateShift(FormTicket.TotalPaid - SelectedTicket.TotalPaid);

                    SelectedTicket = SalesTickets[SalesTickets.IndexOf(SelectedTicket)] = FormTicket;

                    foreach (var entry in FormTicketItems)
                    {
                        // Updates Sales Items
                        await _ticketItemDataService.UpdateEntry(entry);

                        var uneditedEntry = SelectedTicketItems.FirstOrDefault(x => x.ID == entry.ID);
                        var quantityChange = uneditedEntry.Quantity - entry.Quantity;

                        // Updates Stock Quanitites in the Database
                        var stockEntry = AllStockItems.FirstOrDefault(x => x.ID == entry.StockItemID);
                        if (stockEntry != null)
                        {
                            stockEntry.Quantity += quantityChange;
                            await _itemDataService.UpdateEntry(stockEntry);
                            AllStockItems[AllStockItems.IndexOf(stockEntry)] = stockEntry;
                            // Informs other Classes of Stockitem Changes.
                            DataMessanger.BroadCast("itemSold", stockEntry);
                        }
                    }
                    FilterItems();
                    // Switches Page Content
                    SwitchContent("SalesList");
                    break;
                case "AddCustomer":
                    if (FormCustomer.Name == null || FormCustomer.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    FormCustomer.ID = await _customerDataService.CreateEntry(FormCustomer);
                    if (FormCustomer.Name.ToLower().Contains(FilterCustomerKeyword.ToLower()))
                    {
                        FilteredStoreCustomers.Add(FormCustomer);
                        SelectedCustomer = FormCustomer;
                    }
                    StoreCustomers.Add(FormCustomer);
                    FilterStoreCustomers.Add(FormCustomer);
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("CustomersSnackBar1"));
                    // Switches Page Content
                    SwitchContent("CustomersList");
                    break;
                case "EditCustomer":
                    if (FormCustomer.Name == null || FormCustomer.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    if (CartTicket.CustomerID == SelectedCustomer.ID)
                    {
                        SaleCustomer = StoreCustomers[0];
                    }
                    if (SalesCustomerFilter.ID == SelectedCustomer.ID)
                    {
                        SalesCustomerFilter = FilterStoreCustomers[0];
                    }

                    if (FormCustomerPayment != 0)
                    {
                        await _customerPaymentService.AddPayment(FormCustomer, FormCustomerPayment, CurrentUser);
                        DataMessanger.UpdateShift(FormCustomerPayment);
                    }
                    else  await _customerDataService.UpdateEntry(FormCustomer);
                    StoreCustomers[StoreCustomers.IndexOf(SelectedCustomer)] = FormCustomer;
                    FilterStoreCustomers[FilterStoreCustomers.IndexOf(SelectedCustomer)] = FormCustomer;
                    SelectedCustomer = FilteredStoreCustomers[FilteredStoreCustomers.IndexOf(SelectedCustomer)] = FormCustomer;
                    OnPropertyChanged(nameof(StoreCustomers));
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("CustomersSnackBar2"));
                    // Switches Page Content
                    SwitchContent("CustomersList");
                    break;
            }
            // Turns off Progress Bar to indicate command is complete.
            ProgressStatus = false;
        }

        /// <summary>
        /// Deletes an existing data entry (And Adjusts the View).
        /// </summary>
        private async Task DeleteEntryAsync()
        {
            switch (_selectedTab)
            {
                case "SalesList":
                    await _ticketDataService.DeleteEntry(SelectedTicket);

                    // Updates Selected Customer For Sale.
                    StoreCustomer saleCustomer = StoreCustomers.FirstOrDefault(x => x.ID == SelectedTicket.CustomerID) ?? new StoreCustomer();
                    saleCustomer.PurchaseDue -= SelectedTicket.CalculatedTotalPrice;
                    saleCustomer.PaidPurchaseDue -= SelectedTicket.TotalPaid;
                    await _customerDataService.UpdateEntry(saleCustomer);
                    if (saleCustomer.ID != 0)
                    {
                        FilterCustomers();
                    }

                    // Updates Shift Data.
                    DataMessanger.UpdateShift(-SelectedTicket.CalculatedTotalPrice);

                    // Gets the SalesItems of the Ticket.
                    List<SalesItem> salesItems = await _ticketItemDataService.GetAll(x => x.SalesTicketID == SelectedTicket.ID);

                    foreach (SalesItem entry in salesItems)
                    {
                        // Updates Sales Items
                        await _ticketItemDataService.UpdateEntry(entry);

                        // Updates Stock Quanitites in the Database
                        var stockEntry = AllStockItems.FirstOrDefault(x => x.ID == entry.StockItemID);
                        if (stockEntry != null)
                        {
                            stockEntry.Quantity += entry.Quantity;
                            await _itemDataService.UpdateEntry(stockEntry);
                            AllStockItems[AllStockItems.IndexOf(stockEntry)] = stockEntry;
                            // Informs other Classes of Stockitem Changes.
                            DataMessanger.BroadCast("itemSold", stockEntry);
                        }
                    }
                    SalesTickets.Remove(SelectedTicket);
                    FilterItems();
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("SalesSnackBar4"));
                    break;
                case "CustomersList":
                    await _customerDataService.DeleteEntry(SelectedCustomer);
                    if (CartTicket.CustomerID == SelectedCustomer.ID)
                    {
                        SaleCustomer = StoreCustomers[0];
                    }
                    StoreCustomers.Remove(SelectedCustomer);
                    FilterStoreCustomers.Remove(SelectedCustomer);
                    FilteredStoreCustomers.Remove(SelectedCustomer);
                    DialogStatus = false;
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("CustomersSnackBar3"));
                    break;
            }
            // Switches Page Content
            SwitchContent("CloseDelete");
        }

        /// <summary>
        /// Checkout Method Where Database Entires are Added.
        /// </summary>
        private async Task Checkout(bool Printable = false)
        {
            // Turns on Progress Bar to indicate command in action.
            ProgressStatus = true;

            // Calculates discounted Total Price. (Saves it as TotalPaid if it was Payed by non-cash methods)
            var discountedTotal = CartTicket.CalculatedTotalPrice;
            // Holds CartTicket Current TotalPaid Value.
            var paidCash = CartTicket.TotalPaid;

            // Saves discounted Total as TotalPaid if it was Payed by non-cash methods
            if (CartTicket.PayMethod != PaymentMethod.Cash || CartTicket.TotalPaid >= discountedTotal) 
                CartTicket.TotalPaid = discountedTotal;

            // Sets the CustomerID to 0 if none is selected.
            if (SaleCustomer == null) SaleCustomer = StoreCustomers[0];

            // Sets SaleDate & SoldBy Attributes and Saves Ticket Details to Database.
            CartTicket.SaleDate = DateTime.Now;
            CartTicket.SoldBy = CurrentUser.ID;
            CartTicket.CustomerID = SaleCustomer.ID;
            CartTicket.ID = await _ticketDataService.CreateEntry(CartTicket);

            // Updates Selected Customer For Given SaleTicket.
            SaleCustomer.PurchaseDue += discountedTotal;
            SaleCustomer.PaidPurchaseDue += CartTicket.TotalPaid;
            await _customerDataService.UpdateEntry(SaleCustomer);
            if (SaleCustomer.ID != 0)
            {
                FilterCustomers();
            }

            // Saves the Cart Details to the database.
            foreach (var entry in CartItems)
            {
                entry.SalesTicketID = CartTicket.ID;
                await _ticketItemDataService.CreateEntry(entry);

                // Updates Stock Quanitites in the Database
                var stockEntry = AllStockItems.FirstOrDefault(x => x.ID == entry.StockItemID);
                stockEntry.Quantity -= entry.Quantity;
                await _itemDataService.UpdateEntry(stockEntry);
                AllStockItems[AllStockItems.IndexOf(stockEntry)] = stockEntry;

                // Informs other Classes of Stockitem Changes.
                DataMessanger.BroadCast("itemSold", stockEntry);
            }
             
            // If Cashier Decides to Print the Ticket.
            if (Printable == true)
            {
                if (CartTicket.PayMethod == PaymentMethod.Cash) CartTicket.TotalPaid = paidCash;
                
                string paymentName = ((string[])Application.Current.FindResource("SalesPaymentMethod"))[(int)CartTicket.PayMethod];
                var dialog = SetupExportDialog("Bill_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm"));
                //if (dialog.ShowDialog() == true)
                //{
                var currency = (string[])Application.Current.FindResource("CurrencyAbbreviationsList");
                    if (!_printSalesService.Print(CartTicket, CartItems, paymentName, CurrentUser.Name, currency[AppSettings.Load().CurrencyIndex]))
                    {
                        MessageBox.Show(Application.Current.FindResource("ErrorPrint").ToString());
                    }
               // }
                // Resets Total Paid back to what it was before print.
                if (CartTicket.TotalPaid >= discountedTotal) CartTicket.TotalPaid = discountedTotal;
            }

            // Adds the Cart Ticket to Sales List, if it is with Date Range on Display.
            if (CartTicket.SaleDate >= SalesFromDateFilter && CartTicket.SaleDate <= SalesToDateFilter.AddDays(1))
                SalesTickets.Insert(0, CartTicket);

            // Updates Shift Data.
            DataMessanger.UpdateShift(CartTicket.TotalPaid);

            // Resets the UI for a New Sale
            SaleCustomer = StoreCustomers[0];
            CartTicket = new SalesTicket();
            CartTicket.Tax = _CurrentSettings.TaxValue;
            CartItems.Clear();
            FilterItems();
            SwitchContent("CancelPayment");

            // Turns off Progress Bar to indicate command is complete.
            ProgressStatus = false;
        }

        /// <summary>
        /// Refreshs the datagrid entries from the database.
        /// </summary>
        public async Task LoadDataAsync()
        {
            // Gets all stock items data from database.
            List<StockItem> savedItems = await _itemDataService.GetAll();
            AllStockItems = new ObservableCollection<StockItem>(savedItems);
            FilterItems();

            // Gets all categories data from database.
            List<StockCategory> savedCategories = await _categoryDataService.GetAll();
            StockCategories = new ObservableCollection<StockCategory>(savedCategories);

            // Adds a "All Category" entry to Categories as non filter.
            StockCategories.Insert(0, new StockCategory() { ID = 0, Name = (string)Application.Current.FindResource("ItemsSubTitle7") });
            FilterCategory = StockCategories[0];

            // Gets all customers data from database.
            List<StoreCustomer> savedCustomers = await _customerDataService.GetAll();
            StoreCustomers = new ObservableCollection<StoreCustomer>(savedCustomers);
            DataMessanger.BroadCast("LoadCustomers", StoreCustomers);

            // Gets All filter customers data
            FilterStoreCustomers = new ObservableCollection<StoreCustomer>(StoreCustomers);
            FilterStoreCustomers.Insert(0, new StoreCustomer() { ID = 0, Name = (string)Application.Current.FindResource("CustomersSubTitle3") });
            SalesCustomerFilter = FilterStoreCustomers[0];

            // Adds an "Unknown" entry to Customers as non filter.
            StoreCustomers.Insert(0, new StoreCustomer() { ID = 0, Name = (string)Application.Current.FindResource("WordUnknown") });
            SaleCustomer = StoreCustomers[0];
            FilterCustomers();

            // Selects Cart Index.
            SelectedCartIndex = 0;

            // Gets Some SalesTickets data from database.
            await GetSalesAsync();
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default Class Constructor and Command Initializer
        /// </summary>
        public SalesViewModel(IDataService<StockItem> itemDataService, IDataService<StockCategory> categoryDataService, IDataService<StoreCustomer> customerDataService,
            IDataService<SalesTicket> ticketDataService, IDataService<SalesItem> ticketItemDataService, IPrintSalesService printSalesService, ICustomerPaymentService customerPaymentService)
        {
            _itemDataService = itemDataService;
            _categoryDataService = categoryDataService;
            _customerDataService = customerDataService;
            _ticketDataService = ticketDataService;
            _ticketItemDataService = ticketItemDataService;
            _printSalesService = printSalesService;
            _customerPaymentService = customerPaymentService;
            InitializeCommands();
            InitializeEvents();
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Initializes all Event Listensers for SalesViewModel.
        /// </summary>
        private void InitializeEvents()
        {
            DataMessanger.OnMessageTransmitted += OnMessageTransmitted;
            DataMessanger.OnLanguageSwitched += OnLanguageSwitched;
            DataMessanger.OnUserSwitched += OnUserSwitched;
            _cart1Items.CollectionChanged += CartItems_CollectionChanged;
            _cart2Items.CollectionChanged += CartItems_CollectionChanged;
            _cart3Items.CollectionChanged += CartItems_CollectionChanged;
            _cart4Items.CollectionChanged += CartItems_CollectionChanged;
            _cart5Items.CollectionChanged += CartItems_CollectionChanged;
        }

        /// <summary>
        /// Sets CurrentUser Data on login event.
        /// </summary>
        private void OnUserSwitched(string message, SystemUser systemUser, SystemRole systemRole)
        {
            CurrentUser = systemUser;
            CurrentRole = systemRole;
            if (message == "Login")
            {
                if (CurrentRole.OpenPOSTab) SwitchContent("POS");
                else if (CurrentRole.OpenSalesListTab) SwitchContent("SalesList");
                else if (CurrentRole.OpenCustomersTab) SwitchContent("CustomersList");
            }
        }

        /// <summary>
        /// Updates data depending on message from other classes.
        /// </summary>
        private void OnMessageTransmitted(string message, object payload)
        {
            switch (message)
            {
                case "add":
                    if (payload is StockItem)
                    {
                        var item = payload as StockItem;
                        AllStockItems.Add(item);
                        if ((FilterCategory.ID == 0 || item.CategoryID == FilterCategory.ID) && (item.Name.ToLower().Contains(FilterKeyword.ToLower()) || item.Barcode == FilterKeyword))
                            StockItems.Add(item);
                    }
                    else if (payload is StockCategory)
                    {
                        var category = payload as StockCategory;
                        StockCategories.Add(category);
                    }
                    break;
                case "edit":
                    if (payload is StockItem)
                    {
                        var item = payload as StockItem;
                        AllStockItems[AllStockItems.IndexOf(item)] = item;
                        if ((FilterCategory.ID == 0 || item.CategoryID == FilterCategory.ID) && (item.Name.ToLower().Contains(FilterKeyword.ToLower()) || item.Barcode == FilterKeyword))
                            StockItems[StockItems.IndexOf(item)] = item;
                    }
                    else if (payload is StockCategory)
                    {
                        var category = payload as StockCategory;
                        StockCategories[StockCategories.IndexOf(category)] = category;
                        FilterCategory = StockCategories[0];
                    }
                    break;
                case "delete":
                    if (payload is StockItem)
                    {
                        var item = payload as StockItem;
                        AllStockItems.Remove(item);
                        if ((FilterCategory.ID == 0 || item.CategoryID == FilterCategory.ID) && (item.Name.ToLower().Contains(FilterKeyword.ToLower()) || item.Barcode == FilterKeyword))
                            StockItems.Remove(item);
                    }
                    else if (payload is StockCategory)
                    {
                        var category = payload as StockCategory;
                        if (FilterCategory == category) FilterCategory = StockCategories[0];
                        StockCategories.Remove(category);
                    }
                    break;
                case "itemBought":
                    OnMessageTransmitted("edit", payload);
                    OnPropertyChanged(nameof(StockItems));
                    break;
                case "LoadUsers":
                    SystemUsers = new ObservableCollection<SystemUser>((ObservableCollection<SystemUser>)payload);
                    SystemUsers.Insert(0, new SystemUser() { ID = 0, Name = (string)Application.Current.FindResource("ShiftsSubTitle13") });
                    SalesUserFilter = SystemUsers[0];
                    break;
                case "AddUser":
                    SystemUsers.Add(payload as SystemUser);
                    break;
                case "EditUser":
                    OnPropertyChanged(nameof(SystemUsers));
                    break;
                case "DeleteUser":
                    var removedUser = payload as SystemUser;
                    if (SalesUserFilter == removedUser) SalesUserFilter = SystemUsers[0];
                    SystemUsers.Remove(removedUser);
                    break;
                case "TabSwitched":
                    DialogStatus = false;
                    DeleteDialogStatus = false;
                    break;
                case "SetItemDisplayOption":
                    ItemDisplayOption = (bool)payload;
                    break;
            }
        }

        /// <summary>
        /// Refreshes Elements On Language Change.
        /// </summary>
        private void OnLanguageSwitched(string message)
        {
            SwitchContent(_selectedTab);
            OnPropertyChanged(nameof(SalesTickets));
            OnPropertyChanged(nameof(CartItems));
            if (StockCategories != null)
            {
                StockCategories.Remove(StockCategories[0]);
                StockCategories.Insert(0, new StockCategory() { ID = 0, Name = (string)Application.Current.FindResource("ItemsSubTitle7") });
                FilterCategory = StockCategories[0];
            }
            if (StoreCustomers != null)
            {
                StoreCustomers.Remove(StoreCustomers[0]);
                StoreCustomers.Insert(0, new StoreCustomer() { ID = 0, Name = (string)Application.Current.FindResource("WordUnknown") });
                SaleCustomer = StoreCustomers[0];
            }
            if (FilterStoreCustomers != null)
            {
                FilterStoreCustomers.Remove(FilterStoreCustomers[0]);
                FilterStoreCustomers.Insert(0, new StoreCustomer() { ID = 0, Name = (string)Application.Current.FindResource("CustomersSubTitle3") });
                SalesCustomerFilter = FilterStoreCustomers[0];
            }
            if (SystemUsers != null && SystemUsers.Count != 0)
            {
                SystemUsers.Remove(SystemUsers[0]);
                SystemUsers.Insert(0, new SystemUser() { ID = 0, Name = (string)Application.Current.FindResource("ShiftsSubTitle13") });
                SalesUserFilter = SystemUsers[0];
            }
            SelectedCartIndex = 0;
        }

        /// <summary>
        /// Updates Tickets On Cartitems Collection Change.
        /// </summary>
        private void CartItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) => UpdateTicket();
        #endregion
    }
}
