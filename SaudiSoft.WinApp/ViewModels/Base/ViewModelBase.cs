﻿using System.ComponentModel;

namespace SaudiSoft.WinApp
{
    /// <summary>
    /// Implementation of a Base for all ViewModels.
    /// </summary>
    public class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Switchs the Tabs or Page Content and Updates the UI.
        /// </summary>
        public ParameterCommand<string> SwitchContentCommand { get;  set; }

        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
