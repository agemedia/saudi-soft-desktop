﻿using System;
using System.Windows.Input;

namespace SaudiSoft.WinApp
{
    /// <summary>
    /// Impelementation of a base RelayCommand that runs an action.
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Public Members
        /// <summary>
        /// Member holding a reference to command's action.
        /// </summary>
        public Action ExecuteAction { get; set; }

        /// <summary>
        /// Predicate holding a current CanExecute Status. Defaults to True.
        /// </summary>
        public bool CanExecutePredicate { get; set; } = true;

        /// <summary>
        /// Impelementation of a <see cref="CanExecuteChanged"/> event that fires when CanExecute changes value.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Main Constructor of the RelayCommand.
        /// </summary>
        /// <param name="action"></param>
        public RelayCommand(Action execute, bool canExecute = true)
        {
            ExecuteAction = execute;
            CanExecutePredicate = canExecute;
        }
        #endregion

        #region Command Methods/Properties
        /// <summary>
        /// Default RelayCommand can Always execute.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            return CanExecutePredicate;
        }

        /// <summary>
        /// Call to execute the command's action.
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            ExecuteAction();
        }
        #endregion
    }
}
