﻿using SaudiSoft.Core;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;

namespace SaudiSoft.WinApp
{
    public class ShiftsViewModel : ViewModelBase
    {
        #region Private Services
        private readonly IDataService<StoreShift> _shiftDataService;
        #endregion

        #region Private Proprties
        /// <summary>
        /// Holds selected Tab Name.
        /// </summary>
        private string _selectedTab = "CurrentShift";
        #endregion

        #region Public Properties
        /// <summary>
        /// Holds ItemsPage Transistioner's Current Selected Page Number.
        /// </summary>
        public int ContentAreaTab { get; set; } = 0;

        /// <summary>
        /// Holds Items Page Main Title.
        /// </summary>
        public string MainPageTitle { get; set; } = (string)Application.Current.FindResource("ExpensesTitle1");

        /// <summary>
        /// Holds Items Page Secondary Title.
        /// </summary>
        public string SecPageTitle { get; set; } = (string)Application.Current.FindResource("MiscDBSubTitle1");

        /// <summary>
        /// Holds DialogBox Status.
        /// </summary>
        public bool DialogStatus { get; set; } = false;

        /// <summary>
        /// Holds status for Start Shift Progress Bar.
        /// </summary>
        public bool StartProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds status for End Shift Progress Bar.
        /// </summary>
        public bool EndProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds status for Search Progress Bars.
        /// </summary>
        public bool SearchProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds status for Excel Export Progress Bars.
        /// </summary>
        public bool ExcelProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds SnackBar Message Queue
        /// </summary>
        public SnackbarMessageQueue SnackbarMessage { get; set; } = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(1000));

        /// <summary>
        /// Holds Current Supplier Data.
        /// </summary>
        public StoreShift CurrentShift { get; set; }

        /// <summary>
        /// Holds a list of all Shifts in the Database.
        /// </summary>
        public ObservableCollection<StoreShift> StoreShifts { get; set; }

        /// <summary>
        /// Holds Selected In List Shift Data.
        /// </summary>
        public StoreShift SelectedShift { get; set; }

        /// <summary>
        /// Holds Filter From Date for Expenses on Display.
        /// </summary>
        public DateTime FromDateFilter { get; set; } = DateTime.Today.AddMonths(-1);

        /// <summary>
        /// Holds Filter To Date for Expenses on Display.
        /// </summary>
        public DateTime ToDateFilter { get; set; } = DateTime.Today.AddDays(1);

        /// <summary>
        /// Holds all SystemUsers Data.
        /// </summary>
        public ObservableCollection<SystemUser> SystemUsers { get; set; } = new ObservableCollection<SystemUser>();

        /// <summary>
        /// Holds Filter User for Shifts on Display.
        /// </summary>
        public SystemUser FilterUser { get; set; }

        /// <summary>
        /// Currently Logged In User.
        /// </summary>
        public SystemUser CurrentUser { get; set; }

        /// <summary>
        /// Currently Logged In Role.
        /// </summary>
        public SystemRole CurrentRole { get; set; }
        #endregion

        #region Public Commands
        /// <summary>
        /// Starts Current Shift & Saves its Data.
        /// </summary>
        public RelayCommand StartShiftCommand { get; private set; }

        /// <summary>
        /// Ends Current Shift & Saves its Data.
        /// </summary>
        public RelayCommand EndShiftCommand { get; private set; }

        /// <summary>
        /// Deletes an existing data entry (And Adjusts the View).
        /// </summary>
        public RelayCommand DeleteEntryCommand { get; private set; }

        /// <summary>
        /// Loads filtered data from database.
        /// </summary>
        public RelayCommand SearchShiftsCommand { get; private set; }

        /// <summary>
        /// Exports DataGrid Data To Excel.
        /// </summary>
        public RelayCommand ExportExcelCommand { get; private set; }
        #endregion

        #region Class Methods
        /// <summary>
        /// Initializes UI Fired Commands.
        /// </summary>
        private void InitializeCommands()
        {
            SwitchContentCommand = new ParameterCommand<string>((param) => SwitchContent(param));
            StartShiftCommand = new RelayCommand(async () => await StartShift());
            EndShiftCommand = new RelayCommand(async () => await EndShiftAsync());
            DeleteEntryCommand = new RelayCommand(async () => await DeleteShiftAsync());
            SearchShiftsCommand = new RelayCommand(async () => await SearchShiftsAsync());
            ExportExcelCommand = new RelayCommand(async () => await Task.Run(() => ExportToExcel()));
        }

        /// <summary>
        /// Switchs the Tabs or Page Content and Updates the UI.
        /// </summary>
        private void SwitchContent(string tabParameter)
        {
            switch (tabParameter)
            {
                // Opens Add Expense Tab & Changes Tab Titles.
                case "CurrentShift":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ShiftsTitle1");
                    SecPageTitle = (string)Application.Current.FindResource("ShiftsSubTitle1");
                    ContentAreaTab = 0;
                    break;
                // Opens View Expenses Tab & Changes Tab Titles.
                case "ShiftsList":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ShiftsTitle2");
                    SecPageTitle = (string)Application.Current.FindResource("ShiftsSubTitle2");
                    ContentAreaTab = 1;
                    break;
                // Opens Delete Entry Dialog.
                case "OpenDelete":
                    DialogStatus = true;
                    break;
                // Closes Delete Entry Dialog.
                case "CloseDelete":
                    DialogStatus = false;
                    break;
            }
        }

        /// <summary>
        /// Exports a copy of the data on display into an Excel File.
        /// </summary>
        private void ExportToExcel()
        {
            bool isSuccessful;
            // Turns on Progress Bar, Exports Entries, Turn off Progress Bar, then report completion with a message.
            ExcelProgressStatus = true;
            isSuccessful = ExcelExporter.ExportShifts(StoreShifts, SystemUsers);
            ExcelProgressStatus = false;
            if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("ShiftsSnackBar4"));
        }

        /// <summary>
        /// Starts the Shift by saving it in the Database.
        /// </summary>
        private async Task StartShift()
        {
            // Turns on Progress Bar, Starts Shift, Saves its Data, Turn off Progress Bar, then report completion with a message.
            StartProgressStatus = true;
            CurrentShift.StartShift(CurrentUser.ID);
            CurrentShift.ID = await _shiftDataService.CreateEntry(CurrentShift);
            OnPropertyChanged(nameof(CurrentShift));
            StartProgressStatus = false;
            SnackbarMessage.Enqueue((string)Application.Current.FindResource("ShiftsSnackBar1"));
        }

        /// <summary>
        /// Ends the shift by update its data in the Database.
        /// </summary>
        private async Task EndShiftAsync()
        {
            // Turns on Progress Bar, Ends Shift, Saves its Data, Adds it to shifts table, resets FormShift, Turn off Progress Bar, then report completion with a message.
            EndProgressStatus = true;
            CurrentShift.EndShift();
            await _shiftDataService.UpdateEntry(CurrentShift);
            if (CurrentShift.StartDate >= FromDateFilter && CurrentShift.StartDate <= ToDateFilter && (CurrentShift.UserID == FilterUser.ID || FilterUser.ID == 0))
            {
                StoreShifts.Insert(0, CurrentShift);
            }
            CurrentShift = new StoreShift();
            EndProgressStatus = false;
            SnackbarMessage.Enqueue((string)Application.Current.FindResource("ShiftsSnackBar2"));
        }

        /// <summary>
        /// Deletes an existing data entry (And Adjusts the View).
        /// </summary>
        private async Task DeleteShiftAsync()
        {
            // Deletes Shift, Closes Delete Dialog, then report completion with a message.
            await _shiftDataService.DeleteEntry(SelectedShift);
            StoreShifts.Remove(SelectedShift);
            DialogStatus = false;
            SnackbarMessage.Enqueue((string)Application.Current.FindResource("ShiftsSnackBar3"));
        }

        /// <summary>
        /// Refreshs the datagrid entries from the database.
        /// </summary>
        public async Task SearchShiftsAsync()
        {
            // Turns on Progress Bar, Gets Expenses Date From Database, then turn off Progress Bar.
            SearchProgressStatus = true;
            List<StoreShift> savedShifts;
            if (FilterUser == null || FilterUser.ID == 0)
            {
                savedShifts = await _shiftDataService.GetAll(predicate: x => x.StartDate >= FromDateFilter && x.StartDate <= ToDateFilter, orderBy: x => x.ID);
            }
            else
            {
                savedShifts = await _shiftDataService.GetAll(predicate: x => x.StartDate >= FromDateFilter && x.StartDate <= ToDateFilter && x.UserID == FilterUser.ID, orderBy: x => x.ID);
            }
            StoreShifts = new ObservableCollection<StoreShift>(savedShifts);
            SearchProgressStatus = false;
        }

        /// <summary>
        /// Refreshes the data entries from the database.
        /// </summary>
        public async Task LoadDataAsync()
        {
            List<StoreShift> savedShifts = await _shiftDataService.GetAll(predicate: x => x.StartDate >= FromDateFilter && x.StartDate <= ToDateFilter, orderBy: x => x.ID);
            StoreShifts = new ObservableCollection<StoreShift>(savedShifts);
            CurrentShift = await _shiftDataService.Get(x => x.StartDate == x.EndDate) ?? new StoreShift();
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Main Class Constructor and Command Initializer
        /// </summary>
        public ShiftsViewModel(IDataService<StoreShift> shiftsDataService)
        {
            _shiftDataService = shiftsDataService;
            InitializeCommands();
            InitializeEvents();
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Initializes all Event Listensers for ShiftsViewModel.
        /// </summary>
        private void InitializeEvents()
        {
            DataMessanger.OnUserSwitched += OnUserSwitched;
            DataMessanger.OnLanguageSwitched += OnLanguageSwitched;
            DataMessanger.OnMessageTransmitted += OnMessageTransmitted;
            DataMessanger.OnShiftDataTransmitted += OnShiftDataTransmitted;
        }

        /// <summary>
        /// Updates CurrentShift Data According to given Value.
        /// </summary>
        /// <param name="value">Value Changes to Shift Cash</param>
        private async void OnShiftDataTransmitted(double value)
        {
            CurrentShift.CashOut += value;
            await _shiftDataService.UpdateEntry(CurrentShift);
            OnPropertyChanged(nameof(CurrentShift));
        }

        /// <summary>
        /// Sets CurrentUser Data on login event.
        /// </summary>
        private void OnUserSwitched(string message, SystemUser systemUser, SystemRole systemRole)
        {
            CurrentUser = systemUser;
            CurrentRole = systemRole;
            if (message == "Login")
            {
                if (CurrentRole.OpenCurrentShiftTab) SwitchContent("CurrentShift");
                else if (CurrentRole.OpenShiftsListTab) SwitchContent("ShiftsList");
            }
        }

        /// <summary>
        /// Refreshes Elements On Language Change.
        /// </summary>
        private void OnLanguageSwitched(string message)
        {
            SwitchContent(_selectedTab);
            if (SystemUsers.Count != 0)
            {
                SystemUsers.Remove(SystemUsers[0]);
                SystemUsers.Insert(0, new SystemUser() { ID = 0, Name = (string)Application.Current.FindResource("ShiftsSubTitle13") });
                FilterUser = SystemUsers[0];
            }
        }

        /// <summary>
        /// Updates data depending on message from other classes.
        /// </summary>
        private void OnMessageTransmitted(string message, object payload)
        {
            switch (message)
            {
                case "LoadUsers":
                    SystemUsers = new ObservableCollection<SystemUser>((ObservableCollection<SystemUser>)payload);
                    SystemUsers.Insert(0, new SystemUser() { ID = 0, Name = (string)Application.Current.FindResource("ShiftsSubTitle13") });
                    FilterUser = SystemUsers[0];
                    break;
                case "AddUser":
                    SystemUsers.Add(payload as SystemUser);
                    break;
                case "EditUser":
                    OnPropertyChanged(nameof(SystemUsers));
                    break;
                case "DeleteUser":
                    var removedUser = payload as SystemUser;
                    if (FilterUser == removedUser) FilterUser = SystemUsers[0];
                    SystemUsers.Remove(removedUser);
                    break;
                case "TabSwitched":
                    DialogStatus = false;
                    break;
            }
        }
        #endregion
    }
}
