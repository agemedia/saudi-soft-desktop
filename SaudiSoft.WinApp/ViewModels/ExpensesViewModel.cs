﻿using SaudiSoft.Core;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;

namespace SaudiSoft.WinApp
{
    public class ExpensesViewModel : ViewModelBase
    {
        #region Private Services
        private readonly IDataService<StoreExpense> _expenseDataService;
        #endregion

        #region Private Proprties
        /// <summary>
        /// Holds selected Tab Name.
        /// </summary>
        private string _selectedTab = "ViewExpenses";
        #endregion

        #region Public Properties
        /// <summary>
        /// Holds ItemsPage Transistioner's Current Selected Page Number.
        /// </summary>
        public int ContentAreaTab { get; set; } = 0;

        /// <summary>
        /// Holds Items Page Main Title.
        /// </summary>
        public string MainPageTitle { get; set; } = (string)Application.Current.FindResource("ExpensesTitle1");

        /// <summary>
        /// Holds Items Page Secondary Title.
        /// </summary>
        public string SecPageTitle { get; set; } = (string)Application.Current.FindResource("MiscDBSubTitle1");

        /// <summary>
        /// Holds DialogBox Status.
        /// </summary>
        public bool DialogStatus { get; set; } = false;

        /// <summary>
        /// Holds status for Various Forms Progress Bars.
        /// </summary>
        public bool ProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds Error Message for Various Forms.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Holds SnackBar Message Queue
        /// </summary>
        public SnackbarMessageQueue SnackbarMessage { get; set; } = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(1000));

        /// <summary>
        /// Holds Form Expense Data.
        /// </summary>
        public StoreExpense FormExpense { get; set; } = new StoreExpense();

        /// <summary>
        /// Holds Selected In List Expense Data.
        /// </summary>
        public StoreExpense SelectedExpense { get; set; }

        /// <summary>
        /// Holds a list of all Expenses in the Database.
        /// </summary>
        public ObservableCollection<StoreExpense> StoreExpenses { get; set; }

        /// <summary>
        /// Holds Filter From Date for Expenses on Display.
        /// </summary>
        public DateTime FromDateFilter { get; set; } = DateTime.Today.AddMonths(-1);

        /// <summary>
        /// Holds Filter To Date for Expenses on Display.
        /// </summary>
        public DateTime ToDateFilter { get; set; } = DateTime.Today.AddMonths(1);

        /// <summary>
        /// Holds Filter Keyword for Expenses on Display.
        /// </summary>
        public string FilterExpensesKeyword { get; set; } = "";

        /// <summary>
        /// Holds status for Excel Export Progress Bars.
        /// </summary>
        public bool ExcelProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds all SystemUsers Data.
        /// </summary>
        public ObservableCollection<SystemUser> SystemUsers { get; set; } = new ObservableCollection<SystemUser>();

        /// <summary>
        /// Holds Filter User for Expenses on Display.
        /// </summary>
        public SystemUser FilterUser { get; set; }

        /// <summary>
        /// Holds Currently Logged In User.
        /// </summary>
        public SystemUser CurrentUser { get; set; }

        /// <summary>
        /// Holds Currently Logged In User's Role.
        /// </summary>
        public SystemRole CurrentRole { get; set; }
        #endregion

        #region Public Commands
        /// <summary>
        /// Adds a new Data Entry or Updates an Existing One to the Database (And Adjusts the View).
        /// </summary>
        public RelayCommand SaveEntryCommand { get; private set; }

        /// <summary>
        /// Deletes an existing data entry (And Adjusts the View).
        /// </summary>
        public RelayCommand DeleteEntryCommand { get; private set; }

        /// <summary>
        /// Loads filtered data from database.
        /// </summary>
        public RelayCommand LoadDataCommand { get; private set; }

        /// <summary>
        /// Exports DataGrid Data To Excel.
        /// </summary>
        public RelayCommand ExportExcelCommand { get; private set; }
        #endregion

        #region Private Methods
        /// <summary>
        /// Initializes UI Fired Commands.
        /// </summary>
        private void InitializeCommands()
        {
            SwitchContentCommand = new ParameterCommand<string>((param) => SwitchContent(param));
            SaveEntryCommand = new RelayCommand(async () => await SaveEntryAsync());
            DeleteEntryCommand = new RelayCommand(async () => await DeleteEntryAsync());
            LoadDataCommand = new RelayCommand(async () => await LoadDataAsync());
            ExportExcelCommand = new RelayCommand(async () => await Task.Run(() => ExportToExcel()));
        }

        /// <summary>
        /// Switchs the Tabs or Page Content and Updates the UI.
        /// </summary>
        private void SwitchContent(string tabParameter)
        {
            // Resets Error Message.
            ErrorMessage = null;

            switch (tabParameter)
            {
                // Opens View Expenses Tab & Changes Tab Titles.
                case "ViewExpenses":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ExpensesTitle1");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle1");
                    ContentAreaTab = 0;
                    break;
                // Opens Add Expense Tab & Changes Tab Titles.
                case "AddExpense":
                    _selectedTab = tabParameter;
                    FormExpense = new StoreExpense();
                    MainPageTitle = (string)Application.Current.FindResource("ExpensesTitle2");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle2");
                    ContentAreaTab = 1;
                    break;
                // Opens Edit Expense Tab & Changes Tab Titles.
                case "EditExpense":
                    _selectedTab = tabParameter;
                    FormExpense = SelectedExpense.ShallowCopy();
                    MainPageTitle = (string)Application.Current.FindResource("ExpensesTitle3");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle3");
                    ContentAreaTab = 1;
                    break;
                // Opens Delete Entry Dialog.
                case "OpenDelete":
                    DialogStatus = true;
                    break;
                // Closes Delete Entry Dialog.
                case "CloseDelete":
                    DialogStatus = false;
                    break;
            }
        }

        /// <summary>
        /// Exports a copy of the data on display into an Excel File.
        /// </summary>
        private void ExportToExcel()
        {
            bool isSuccessful;
            // Turns on Progress Bar, Exports Entries, Turn off Progress Bar, then report completion with a message.
            ExcelProgressStatus = true;
            isSuccessful = ExcelExporter.ExportExpenses(StoreExpenses);
            ExcelProgressStatus = false;
            if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("ExpensesSnackBar4"));
        }

        /// <summary>
        /// Adds a new Data Entry or Updates an Existing One to the Database (And Adjusts the View).
        /// </summary>
        private async Task SaveEntryAsync()
        {
            // Turns on Progress Bar to indicate command in action.
            ProgressStatus = true;

            // Resets Error Message.
            ErrorMessage = null;
            switch (_selectedTab)
            {
                case "AddExpense":
                    if (FormExpense.Name == null || FormExpense.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    FormExpense.CreatedBy = CurrentUser.ID;
                    FormExpense.ID = await _expenseDataService.CreateEntry(FormExpense);
                    DataMessanger.UpdateShift(-FormExpense.Amount);
                    if (FormExpense.ExpenseDate >= FromDateFilter && FormExpense.ExpenseDate <= ToDateFilter && FormExpense.Name.ToLower().Contains(FilterExpensesKeyword.ToLower()))
                    {
                        StoreExpenses.Insert(0, FormExpense);
                        SelectedExpense = FormExpense;
                    }
                    SwitchContent("ViewExpenses");
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ExpensesSnackBar1"));
                    break;
                case "EditExpense":
                    if (FormExpense.Name == null || FormExpense.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    FormExpense.Editedby = CurrentUser.ID;
                    await _expenseDataService.UpdateEntry(FormExpense);
                    int index = StoreExpenses.IndexOf(SelectedExpense);
                    DataMessanger.UpdateShift(SelectedExpense.Amount - FormExpense.Amount);
                    SelectedExpense = StoreExpenses[index] = FormExpense;
                    SwitchContent("ViewExpenses");
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ExpensesSnackBar2"));
                    break;
            }
            // Turns off Progress Bar to indicate command is complete.
            ProgressStatus = false;
        }
        /// <summary>
        /// Deletes an existing data entry (And Adjusts the View).
        /// </summary>
        private async Task DeleteEntryAsync()
        {
            await _expenseDataService.DeleteEntry(SelectedExpense);
            StoreExpenses.Remove(SelectedExpense);
            DialogStatus = false;
            SnackbarMessage.Enqueue((string)Application.Current.FindResource("ExpensesSnackBar3"));
        }

        /// <summary>
        /// Refreshs the datagrid entries from the database.
        /// </summary>
        public async Task LoadDataAsync()
        {
            // Locks command while processing.
            LoadDataCommand.CanExecutePredicate = false;

            // Gets Expenses Date From Database.
            List<StoreExpense> savedExpenses;
            if (FilterUser == null || FilterUser.ID == 0)
            {
                savedExpenses = await _expenseDataService.GetAll(predicate: x => x.ExpenseDate >= FromDateFilter && x.ExpenseDate <= ToDateFilter && x.Name.ToLower().Contains(FilterExpensesKeyword.ToLower()), orderBy: x => x.ID);
            }
            else
            {
                savedExpenses = await _expenseDataService.GetAll(predicate: x => x.ExpenseDate >= FromDateFilter && x.ExpenseDate <= ToDateFilter && x.Name.ToLower().Contains(FilterExpensesKeyword.ToLower()) && x.CreatedBy == FilterUser.ID, orderBy: x => x.ID);
            }
            StoreExpenses = new ObservableCollection<StoreExpense>(savedExpenses);

            // Unlocks command after process is done.
            LoadDataCommand.CanExecutePredicate = true;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Main Class Constructor and Command Initializer
        /// </summary>
        public ExpensesViewModel(IDataService<StoreExpense> expenseDataService)
        {
            _expenseDataService = expenseDataService;
            InitializeCommands();
            InitializeEvents();
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Initializes all Event Listensers for ExpensesViewModel.
        /// </summary>
        private void InitializeEvents()
        {
            DataMessanger.OnUserSwitched += OnUserSwitched;
            DataMessanger.OnLanguageSwitched += OnLanguageSwitched;
            DataMessanger.OnMessageTransmitted += OnMessageTransmitted;
        }

        /// <summary>
        /// Sets CurrentUser Data on login event.
        /// </summary>
        private void OnUserSwitched(string message, SystemUser systemUser, SystemRole systemRole)
        {
            CurrentUser = systemUser;
            CurrentRole = systemRole;
        }

        /// <summary>
        /// Updates data depending on message from other classes.
        /// </summary>
        private void OnMessageTransmitted(string message, object payload)
        {
            switch (message)
            {
                case "LoadUsers":
                    SystemUsers = new ObservableCollection<SystemUser>((ObservableCollection<SystemUser>)payload);
                    SystemUsers.Insert(0, new SystemUser() { ID = 0, Name = (string)Application.Current.FindResource("ShiftsSubTitle13") });
                    FilterUser = SystemUsers[0];
                    break;
                case "AddUser":
                    SystemUsers.Add(payload as SystemUser);
                    break;
                case "EditUser":
                    OnPropertyChanged(nameof(SystemUsers));
                    break;
                case "DeleteUser":
                    var removedUser = payload as SystemUser;
                    if (FilterUser == removedUser) FilterUser = SystemUsers[0];
                    SystemUsers.Remove(removedUser);
                    break;
                case "TabSwitched":
                    DialogStatus = false;
                    break;
            }
        }

        /// <summary>
        /// Refreshes Elements On Language Change.
        /// </summary>
        private void OnLanguageSwitched(string message)
        {
            SwitchContent(_selectedTab);
            if (SystemUsers.Count != 0)
            {
                SystemUsers.Remove(SystemUsers[0]);
                SystemUsers.Insert(0, new SystemUser() { ID = 0, Name = (string)Application.Current.FindResource("ShiftsSubTitle13") });
                FilterUser = SystemUsers[0];
            }
        }
        #endregion
    }
}
