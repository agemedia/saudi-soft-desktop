﻿using SaudiSoft.Core;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SaudiSoft.WinApp
{
    public class ItemsViewModel : ViewModelBase
    {
        #region Private Services
        private readonly IDataService<StockItem> _itemDataService;
        private readonly IDataService<StockCategory> _categoryDataService;
        private readonly IDataService<StockBrand> _brandDataService;
        #endregion

        #region Private Proprties
        /// <summary>
        /// Holds selected Tab Name.
        /// </summary>
        private string _selectedTab = "ItemsList";
        #endregion

        #region Public Properties
        /// <summary>
        /// Holds ItemsPage Transistioner's Current Selected Page Number.
        /// </summary>
        public int ContentAreaTab { get; set; } = 0;

        /// <summary>
        /// Holds Items Page Main Title.
        /// </summary>
        public string MainPageTitle { get; set; } = (string)Application.Current.FindResource("ItemsTitle1");

        /// <summary>
        /// Holds Items Page Secondary Title.
        /// </summary>
        public string SecPageTitle { get; set; } = (string)Application.Current.FindResource("MiscDBSubTitle1");

        /// <summary>
        /// Holds DialogBox Status.
        /// </summary>
        public bool DialogStatus { get; set; } = false;

        /// <summary>
        /// Holds status for Various Forms Progress Bars.
        /// </summary>
        public bool ProgressStatus { get; set; } = false;

        /// <summary>
        /// Holds Error Message for Various Forms.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Holds SnackBar Message Queue
        /// </summary>
        public SnackbarMessageQueue SnackbarMessage { get; set; } = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(1000));

        /// <summary>
        /// Holds Form Item Data.
        /// </summary>
        public StockItem FormItem { get; set; } = new StockItem();

        /// <summary>
        /// Holds Selected In List Item Data.
        /// </summary>
        public StockItem SelectedItem { get; set; }

        /// <summary>
        /// Holds a list of all Items in the Database.
        /// </summary>
        public ObservableCollection<StockItem> StockItems { get; set; }

        /// <summary>
        /// Holds Form Brand Data.
        /// </summary>
        public StockBrand FormBrand { get; set; } = new StockBrand();

        /// <summary>
        /// Holds Selected In List Brand Data.
        /// </summary>
        public StockBrand SelectedBrand { get; set; }

        /// <summary>
        /// Holds a list of all StockBrands in the Database.
        /// </summary>
        public ObservableCollection<StockBrand> StockBrands { get; set; }

        /// <summary>
        /// Holds Form Category Data.
        /// </summary>
        public StockCategory FormCategory { get; set; } = new StockCategory();

        /// <summary>
        /// Holds Selected In List Category Data.
        /// </summary>
        public StockCategory SelectedCategory { get; set; }

        /// <summary>
        /// Holds a list of all Categories in the Database.
        /// </summary>
        public ObservableCollection<StockCategory> StockCategories { get; set; }

        /// <summary>
        /// Holds Filter Brand for Items.
        /// </summary>
        public StockBrand FilterBrand { get; set; }

        /// <summary>
        /// Holds a list of all StockBrands in the Database.
        /// </summary>
        public ObservableCollection<StockBrand> FilterStockBrands { get; set; }

        /// <summary>
        /// Holds Filter Category for Items.
        /// </summary>
        public StockCategory FilterCategory { get; set; }

        /// <summary>
        /// Holds a list of all Categories in the Database.
        /// </summary>
        public ObservableCollection<StockCategory> FilterStockCategories { get; set; }

        /// <summary>
        /// Suppress FilterEntries from updating collection (useful for LanguageChanged.)
        /// </summary>
        public bool SuppressFilterAction = false;

        /// <summary>
        /// Holds Filter keyword for Items Filtering.
        /// </summary>
        public string FilterItemKeyword { get; set; } = "";

        /// <summary>
        /// Holds a list of Filtered Items in the Database On Display.
        /// </summary>
        public ObservableCollection<StockItem> DisplayStockItems { get; set; }

        /// <summary>
        /// Holds Filter keyword for Category Filtering.
        /// </summary>
        public string FilterCategoryKeyword { get; set; } = "";

        /// <summary>
        /// Holds a list of Filtered Categories in the Database On Display.
        /// </summary>
        public ObservableCollection<StockCategory> DisplayStockCategories { get; set; }

        /// <summary>
        /// Holds Filter keyword for Brand Filtering.
        /// </summary>
        public string FilterBrandKeyword { get; set; } = "";

        /// <summary>
        /// Holds a list of Filtered Brands in the Database On Display.
        /// </summary>
        public ObservableCollection<StockBrand> DisplayStockBrands { get; set; }

        /// <summary>
        /// Holds status for Excel Export Progress Bars.
        /// </summary>
        public ObservableCollection<bool> ExcelProgressStatus { get; set; } = new ObservableCollection<bool> { false, false, false };

        /// <summary>
        /// Currently Logged In User.
        /// </summary>
        public SystemUser CurrentUser { get; set; }

        /// <summary>
        /// Currently Logged In Role.
        /// </summary>
        public SystemRole CurrentRole { get; set; }
        #endregion

        #region Public Commands
        /// <summary>
        /// Load Image A Selected Image Into the UI.
        /// </summary>
        public RelayCommand OpenImageCommand { get; private set; }

        /// <summary>
        /// Adds a new Data Entry or Updates an Existing One to the Database (And Adjusts the View).
        /// </summary>
        public RelayCommand SaveEntryCommand { get; private set; }

        /// <summary>
        /// Deletes an existing data entry (And Adjusts the View).
        /// </summary>
        public RelayCommand DeleteEntryCommand { get; private set; }

        /// <summary>
        /// Filters existing tables (And depending on the View).
        /// </summary>
        public ParameterCommand<string> FilterEntriesCommand { get; private set; }

        /// <summary>
        /// Gets an item report on the selected item.
        /// </summary>
        public RelayCommand GetItemReportCommand { get; private set; }

        /// <summary>
        /// Exports DataGrid Data To Excel.
        /// </summary>
        public RelayCommand ExportExcelCommand { get; private set; }
        #endregion

        #region Private Functions / Methods
        /// <summary>
        /// Initializes UI Fired Commands.
        /// </summary>
        private void InitializeCommands()
        {
            SwitchContentCommand = new ParameterCommand<string>((param) => SwitchContent(param));
            OpenImageCommand = new RelayCommand(() => OpenImage());
            SaveEntryCommand = new RelayCommand(async () => await SaveEntryAsync());
            DeleteEntryCommand = new RelayCommand(async () => await DeleteEntryAsync());
            FilterEntriesCommand = new ParameterCommand<string>((param) => FilterEntries(param));
            GetItemReportCommand = new RelayCommand(() => DataMessanger.BroadCast("GetItemReport", SelectedItem));
            ExportExcelCommand = new RelayCommand(async () => await Task.Run(() => ExportToExcel()));
        }

        /// <summary>
        /// Switchs the Tabs or Page Content and Updates the UI.
        /// </summary>
        private void SwitchContent(string tabParameter)
        {
            // Resets Error Message.
            ErrorMessage = null;

            switch (tabParameter)
            {
                // Opens View Items Tab & Changes Tab Titles.
                case "ItemsList":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ItemsTitle1");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle1");
                    ContentAreaTab = 0;
                    break;
                // Opens Add Item Tab & Changes Tab Titles.
                case "AddItem":
                    FormItem = new StockItem();
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ItemsTitle2");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle2");
                    ContentAreaTab = 1;
                    break;
                // Opens Edit Item Tab & Changes Tab Titles.
                case "EditItem":
                    FormItem = SelectedItem.ShallowCopy();
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ItemsTitle3");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle3");
                    ContentAreaTab = 1;
                    break;
                // Opens View Brands Tab & Changes Tab Titles.
                case "BrandsList":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ItemsTitle4");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle1");
                    ContentAreaTab = 2;
                    break;
                // Opens Add Brand Tab & Changes Tab Titles.
                case "AddBrand":
                    FormBrand = new StockBrand();
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ItemsTitle5");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle2");
                    ContentAreaTab = 3;
                    break;
                // Opens Add Brand Tab & Changes Tab Titles.
                case "EditBrand":
                    FormBrand = SelectedBrand.ShallowCopy();
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ItemsTitle6");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle2");
                    ContentAreaTab = 3;
                    break;
                // Opens View Categories Tab & Changes Tab Titles.
                case "CategoriesList":
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ItemsTitle7");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle1");
                    ContentAreaTab = 4;
                    break;
                // Opens Add Category Tab & Changes Tab Titles.
                case "AddCategory":
                    FormCategory = new StockCategory();
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ItemsTitle8");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle2");
                    ContentAreaTab = 5;
                    break;
                // Opens Edit Category Tab & Changes Tab Titles.
                case "EditCategory":
                    FormCategory = SelectedCategory.ShallowCopy();
                    _selectedTab = tabParameter;
                    MainPageTitle = (string)Application.Current.FindResource("ItemsTitle9");
                    SecPageTitle = (string)Application.Current.FindResource("MiscDBSubTitle3");
                    ContentAreaTab = 5;
                    break;
                // Opens Delete Entry Dialog.
                case "OpenDelete":
                    DialogStatus = true;
                    break;
                // Closes Delete Entry Dialog.
                case "CloseDelete":
                    DialogStatus = false;
                    break;
            }
        }

        /// <summary>
        /// Opens a dialog to select an image to open.
        /// </summary>
        private void OpenImage()
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog
            {
                Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.bmp) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.bmp"
            };
            if (dialog.ShowDialog() == true)
            {
                FormItem.Image = File.ReadAllBytes(dialog.FileName);
                OnPropertyChanged(nameof(FormItem));
            }
        }

        /// <summary>
        /// Filteres Entries depending on the view and given filteres.
        /// </summary>
        public void FilterEntries(string parameter)
        {
            switch (parameter)
            {
                case "Items":
                    // If filtercategory and filterBrand are null, cancel the filter process.
                    if (FilterCategory == null || FilterBrand == null || SuppressFilterAction)
                        break;

                    // If filtercategory and filterBrand does not exist or equal to 0, only filter is the FilterKeyword.
                    if (FilterCategory.ID == 0 && FilterBrand.ID == 0)
                        DisplayStockItems = new ObservableCollection<StockItem>(StockItems.Where(x => x.Name.ToLower().Contains(FilterItemKeyword.ToLower()) || x.Barcode == FilterItemKeyword));

                    // If filtercategory does not exist or equal to 0, filter is the filterbrand and FilterKeyword.
                    else if (FilterCategory.ID == 0 && FilterBrand.ID != 0)
                        DisplayStockItems = new ObservableCollection<StockItem>(StockItems.Where(x => x.BrandID == FilterBrand.ID && (x.Name.ToLower().Contains(FilterItemKeyword.ToLower()) || x.Barcode == FilterItemKeyword)));

                    // If filterbrand does not exist or equal to 0, filter is the filtercategory and FilterKeyword.
                    else if (FilterCategory.ID != 0 && FilterBrand.ID == 0)
                        DisplayStockItems = new ObservableCollection<StockItem>(StockItems.Where(x => x.CategoryID == FilterCategory.ID && (x.Name.ToLower().Contains(FilterItemKeyword.ToLower()) || x.Barcode == FilterItemKeyword)));

                    // else category, brand and name/keyword filters.
                    else
                        DisplayStockItems = new ObservableCollection<StockItem>(StockItems.Where(x => x.CategoryID == FilterCategory.ID && x.BrandID == FilterBrand.ID && (x.Name.ToLower().Contains(FilterItemKeyword.ToLower()) || x.Barcode == FilterItemKeyword)));
                    break;
                case "Brands":
                    DisplayStockBrands = new ObservableCollection<StockBrand>(StockBrands.Where(x => x.Name.ToLower().Contains(FilterBrandKeyword.ToLower()) || x.Code.ToLower().Contains(FilterBrandKeyword.ToLower())));
                    break;
                case "Categories":
                    DisplayStockCategories = new ObservableCollection<StockCategory>(StockCategories.Where(x => x.Name.ToLower().Contains(FilterCategoryKeyword.ToLower()) || x.Code.ToLower().Contains(FilterCategoryKeyword.ToLower())));
                    break;
            }
        }

        /// <summary>
        /// Exports a copy of the data on display into an Excel File.
        /// </summary>
        private void ExportToExcel()
        {
            bool isSuccessful;
            // Turns on Progress Bar, Exports Entries, Turn off Progress Bar, then report completion with a message.
            switch (_selectedTab)
            {
                case "ItemsList":
                    ExcelProgressStatus[0] = true;
                    isSuccessful = ExcelExporter.ExportItems(DisplayStockItems, StockCategories, StockBrands);
                    ExcelProgressStatus[0] = false;
                    if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar10"));
                    break;
                case "BrandsList":
                    ExcelProgressStatus[1] = true;
                    isSuccessful = ExcelExporter.ExportBrands(DisplayStockBrands);
                    ExcelProgressStatus[1] = false;
                    if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar11"));
                    break;
                case "CategoriesList":
                    ExcelProgressStatus[2] = true;
                    isSuccessful = ExcelExporter.ExportCategories(DisplayStockCategories);
                    ExcelProgressStatus[2] = false;
                    if (isSuccessful) SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar12"));
                    break;
            }
        }

        /// <summary>
        /// Adds a new Data Entry or Updates an Existing One to the Database (And Adjusts the View).
        /// </summary>
        private async Task SaveEntryAsync()
        {
            // Turns on Progress Bar to indicate command in action.
            ProgressStatus = true;

            // Resets Error Message.
            ErrorMessage = null;
            switch (_selectedTab)
            {
                case "AddItem":
                    if (FormItem.Name == null || FormItem.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    FormItem.ID = await _itemDataService.CreateEntry(FormItem);
                    StockItems.Add(FormItem);
                    // If item conditions fit the filters it gets added to the displayed items.
                    if (FormItem.Name.ToLower().Contains(FilterItemKeyword.ToLower()) && (FilterBrand.ID == 0 || FormItem.BrandID == FilterBrand.ID) && (FilterCategory.ID == 0 || FormItem.CategoryID == FilterCategory.ID))
                    {
                        DisplayStockItems.Add(FormItem);
                        SelectedItem = FormItem;
                    }
                    SwitchContent("ItemsList");
                    DataMessanger.BroadCast("add", FormItem);
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar1"));
                    break;
                case "EditItem":
                    if (FormItem.Name == null || FormItem.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    await _itemDataService.UpdateEntry(FormItem);
                    StockItems[StockItems.IndexOf(SelectedItem)] = FormItem;
                    SelectedItem = DisplayStockItems[DisplayStockItems.IndexOf(SelectedItem)] = FormItem;
                    OnPropertyChanged(nameof(DisplayStockItems));
                    SwitchContent("ItemsList");
                    DataMessanger.BroadCast("edit", SelectedItem);
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar2"));
                    break;
                case "AddBrand":
                    if (FormBrand.Name == null || FormBrand.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    if (FormBrand.Name.ToLower().Contains(FilterBrandKeyword.ToLower()) || FormBrand.Code.ToLower().Contains(FilterBrandKeyword.ToLower()))
                    {
                        DisplayStockBrands.Add(FormBrand);
                        SelectedBrand = FormBrand;
                    }
                    FormBrand.ID = await _brandDataService.CreateEntry(FormBrand);
                    StockBrands.Add(FormBrand);
                    FilterStockBrands.Add(FormBrand);
                    SwitchContent("BrandsList");
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar3"));
                    break;
                case "EditBrand":
                    if (FormBrand.Name == null || FormBrand.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    await _brandDataService.UpdateEntry(FormBrand);
                    if (FilterBrand.ID == FormBrand.ID)
                    {
                        FilterBrand = FilterStockBrands[0];
                    }
                    StockBrands[StockBrands.IndexOf(SelectedBrand)] = FormBrand;
                    FilterStockBrands[FilterStockBrands.IndexOf(SelectedBrand)] = FormBrand;
                    SelectedBrand = DisplayStockBrands[DisplayStockBrands.IndexOf(SelectedBrand)] = FormBrand;
                    OnPropertyChanged(nameof(StockBrands));
                    SwitchContent("BrandsList");
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar4"));
                    break;
                case "AddCategory":
                    if (FormCategory.Name == null || FormCategory.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    if (FormCategory.Name.ToLower().Contains(FilterCategoryKeyword.ToLower()) || FormCategory.Code.ToLower().Contains(FilterCategoryKeyword.ToLower()))
                    {
                        DisplayStockCategories.Add(FormCategory);
                        SelectedCategory = FormCategory;
                    }
                    FormCategory.ID = await _categoryDataService.CreateEntry(FormCategory);
                    StockCategories.Add(FormCategory);
                    FilterStockCategories.Add(FormCategory);
                    SwitchContent("CategoriesList");
                    DataMessanger.BroadCast("add", FormCategory);
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar5"));
                    break;
                case "EditCategory":
                    if (FormCategory.Name == null || FormCategory.Name == "")
                    {
                        ErrorMessage = (string)Application.Current.FindResource("MiscError1");
                        break;
                    }
                    await _categoryDataService.UpdateEntry(FormCategory);
                    if (FilterCategory.ID == FormCategory.ID)
                    {
                        FilterCategory = FilterStockCategories[0];
                    }
                    StockCategories[StockCategories.IndexOf(SelectedCategory)] = FormCategory;
                    FilterStockCategories[FilterStockCategories.IndexOf(SelectedCategory)] = FormCategory;
                    SelectedCategory = DisplayStockCategories[DisplayStockCategories.IndexOf(SelectedCategory)] = FormCategory;
                    OnPropertyChanged(nameof(DisplayStockCategories));
                    SwitchContent("CategoriesList");
                    DataMessanger.BroadCast("edit", FormCategory);
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar6"));
                    break;
            }
            // Turns off Progress Bar to indicate command is complete.
            ProgressStatus = false;
        }

        /// <summary>
        /// Deletes an existing data entry (And Adjusts the View).
        /// </summary>
        private async Task DeleteEntryAsync()
        {
            switch (_selectedTab)
            {
                case "ItemsList":
                    await _itemDataService.DeleteEntry(SelectedItem);
                    DataMessanger.BroadCast("delete", SelectedItem);
                    DisplayStockItems.Remove(SelectedItem);
                    StockItems.Remove(SelectedItem);
                    DialogStatus = false;
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar7"));
                    break;
                case "BrandsList":
                    await _brandDataService.DeleteEntry(SelectedBrand);
                    if (FilterBrand.ID == SelectedBrand.ID)
                    {
                        FilterBrand = FilterStockBrands[0];
                    }
                    FilterStockBrands.Remove(SelectedBrand);
                    StockBrands.Remove(SelectedBrand);
                    DisplayStockBrands.Remove(SelectedBrand);
                    DialogStatus = false;
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar8"));
                    break;
                case "CategoriesList":
                    await _categoryDataService.DeleteEntry(SelectedCategory);
                    DataMessanger.BroadCast("delete", SelectedCategory);
                    if (FilterCategory.ID == SelectedCategory.ID)
                    {
                        FilterCategory = FilterStockCategories[0];
                    }
                    FilterStockCategories.Remove(SelectedCategory);
                    StockCategories.Remove(SelectedCategory);
                    DisplayStockCategories.Remove(SelectedCategory);
                    DialogStatus = false;
                    SnackbarMessage.Enqueue((string)Application.Current.FindResource("ItemsSnackBar9"));
                    break;
            }
        }

        /// <summary>
        /// Refreshs the datagrid entries from the database.
        /// </summary>
        public async Task LoadDataAsync()
        {
            // Gets all categories data from database.
            List<StockCategory> savedCategories = await _categoryDataService.GetAll();
            StockCategories = new ObservableCollection<StockCategory>(savedCategories);

            // Gets all brands data from database.
            List<StockBrand> savedBrands = await _brandDataService.GetAll();
            StockBrands = new ObservableCollection<StockBrand>(savedBrands);

            // Gets all stock items data from database.
            List<StockItem> savedItems = await _itemDataService.GetAll();
            StockItems = new ObservableCollection<StockItem>(savedItems);
            DataMessanger.BroadCast("LoadItems", StockItems);

            // Setups Up FilterStockCategories
            FilterStockCategories = new ObservableCollection<StockCategory>(StockCategories);
            FilterStockCategories.Insert(0, new StockCategory() { ID = 0, Name = (string)Application.Current.FindResource("ItemsSubTitle7") });
            FilterCategory = FilterStockCategories[0];

            // Setups Up FilterStockCategories
            FilterStockBrands = new ObservableCollection<StockBrand>(StockBrands);
            FilterStockBrands.Insert(0, new StockBrand() { ID = 0, Name = (string)Application.Current.FindResource("ItemsSubTitle8") });
            FilterBrand = FilterStockBrands[0];

            FilterEntries("Brands");
            FilterEntries("Categories");
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Main Class Constructor and Command Initializer
        /// </summary>
        public ItemsViewModel(IDataService<StockItem> itemDataService, IDataService<StockCategory> categoryDataService, IDataService<StockBrand> brandDataService)
        {
            _itemDataService = itemDataService;
            _categoryDataService = categoryDataService;
            _brandDataService = brandDataService;
            InitializeCommands();
            InitializeEvents();
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Initializes all Event Listensers for ItemsViewModel.
        /// </summary>
        private void InitializeEvents()
        {
            DataMessanger.OnUserSwitched += OnUserSwitched;
            DataMessanger.OnLanguageSwitched += OnLanguageSwitched;
            DataMessanger.OnMessageTransmitted += OnMessageTransmitted;
        }

        /// <summary>
        /// Sets CurrentUser Data on login event.
        /// </summary>
        private void OnUserSwitched(string message, SystemUser systemUser, SystemRole systemRole)
        {
            CurrentUser = systemUser;
            CurrentRole = systemRole;
            if (message == "Login")
            {
                if (CurrentRole.OpenItemsListTab) SwitchContent("ItemsList");
                else if (CurrentRole.OpenBrandsListTab) SwitchContent("BrandsList");
                else if (CurrentRole.OpenCategoriesListTab) SwitchContent("CategoriesList");
            }
        }

        /// <summary>
        /// Updates data depending on message from other classes.
        /// </summary>
        private void OnMessageTransmitted(string message, object payload)
        {
            switch (message)
            {
                case "itemSold":
                    StockItems[StockItems.IndexOf((StockItem)payload)] = (StockItem)payload;
                    var filteredIndex = DisplayStockItems.IndexOf((StockItem)payload);
                    if (filteredIndex != -1)
                    {
                        DisplayStockItems[filteredIndex] = (StockItem)payload;
                    }
                    OnPropertyChanged(nameof(DisplayStockItems));
                    OnPropertyChanged(nameof(SelectedItem));
                    break;
                case "itemBought":
                    StockItems[StockItems.IndexOf((StockItem)payload)] = (StockItem)payload;
                    var filteredIndex2 = DisplayStockItems.IndexOf((StockItem)payload);
                    if (filteredIndex2 != -1)
                    {
                        DisplayStockItems[filteredIndex2] = (StockItem)payload;
                    }
                    OnPropertyChanged(nameof(DisplayStockItems));
                    OnPropertyChanged(nameof(SelectedItem));
                    break;
                case "TabSwitched":
                    DialogStatus = false;
                    break;
            }
        }


        /// <summary>
        /// Refreshes Elements On Language Change.
        /// </summary>
        private void OnLanguageSwitched(string message)
        {
            SuppressFilterAction = true;
            if (FilterStockCategories != null)
            {
                FilterStockCategories.Remove(FilterStockCategories[0]);
                FilterStockCategories.Insert(0, new StockCategory() { ID = 0, Name = (string)Application.Current.FindResource("ItemsSubTitle7") });
                FilterCategory = FilterStockCategories[0];
            }
            if (FilterStockBrands != null)
            {
                FilterStockBrands.Remove(FilterStockBrands[0]);
                FilterStockBrands.Insert(0, new StockBrand() { ID = 0, Name = (string)Application.Current.FindResource("ItemsSubTitle8") });
                FilterBrand = FilterStockBrands[0];
            }
            SuppressFilterAction = false;
            SwitchContent(_selectedTab);
            OnPropertyChanged(nameof(DisplayStockItems));
        }
        #endregion
    }
}
