﻿using SaudiSoft.Core;
using LiveCharts;
using LiveCharts.Wpf;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SaudiSoft.WinApp
{
    public class DashboardViewModel : ViewModelBase
    {
        #region Private Services
        private readonly IDashboardService _dashboardService;
        #endregion

        #region Public Properties
        /// <summary>
        /// Holds the number of the items in the database.
        /// </summary>
        public DashboardServiceResult DashData { get; set; }

        /// <summary>
        /// Holds Sales Chart Values.
        /// </summary>
        public SeriesCollection SalesValues { get; set; }
        #endregion

        #region Private Methods
        /// <summary>
        /// Loads Data On Application Launch.
        /// </summary>
        public async Task LoadDataAsync()
        {
            DashData = await _dashboardService.PopulateAsync();

            var converter = new BrushConverter();
            var brush = (Brush)converter.ConvertFromString("#128C73");
            SalesValues = new SeriesCollection
            {
                new LineSeries
                {
                    Title = (string)Application.Current.FindResource("DBAttributeAmount"),
                    Values = new ChartValues<double>(DashData.SalesValues),
                    Stroke = brush,
                    LabelPoint = point => string.Format("{0} EGP", point.Y),
                },
            };
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Main Class Constructor and Command Initializer
        /// </summary>
        public DashboardViewModel(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
            InitializeEvents();
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Initializes all Event Listensers for DashboardViewModel.
        /// </summary>
        private void InitializeEvents()
        {
            DataMessanger.OnMessageTransmitted += OnMessageTransmitted;
        }

        /// <summary>
        /// Refreshes Elements On Refresh Request.
        /// </summary>
        private async void OnMessageTransmitted(string messsge, object payload)
        {
            if (messsge == "RefreshDashboard")
            {
                await LoadDataAsync();
            }
        }
        #endregion
    }
}
