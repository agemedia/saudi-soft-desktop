﻿using System.Windows.Controls;

namespace SaudiSoft.WinApp.Views
{
    /// <summary>
    /// Interaction logic for ItemsListControl.xaml
    /// </summary>
    public partial class ItemsListControl : UserControl
    {
        public ItemsListControl()
        {
            InitializeComponent();
        }

        private void FilterItemsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var VM = (ItemsViewModel)this.DataContext;
            VM.FilterEntries("Items");
        }
    }
}
