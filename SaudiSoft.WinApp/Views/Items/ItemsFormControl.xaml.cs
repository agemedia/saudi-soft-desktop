﻿using System.Windows.Controls;

namespace SaudiSoft.WinApp.Views
{
    /// <summary>
    /// Interaction logic for ItemsFormControl.xaml
    /// </summary>
    public partial class ItemsFormControl : UserControl
    {
        public ItemsFormControl()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox unitComboBox = (ComboBox)sender;
            if (unitComboBox.SelectedIndex == -1)
            {
                var VM = (ItemsViewModel)DataContext;
                if (VM.FormItem.ID == 0)
                {
                    unitComboBox.SelectedIndex = 0;
                }
                else
                {
                    unitComboBox.SelectedIndex = VM.SelectedItem.Unit;
                }
            }
        }
    }
}
