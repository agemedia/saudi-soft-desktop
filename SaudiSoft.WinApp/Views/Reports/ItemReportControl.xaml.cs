﻿using System.Windows.Controls;

namespace SaudiSoft.WinApp.Views
{
    /// <summary>
    /// Interaction logic for ItemReportControl.xaml
    /// </summary>
    public partial class ItemReportControl : UserControl
    {
        public ItemReportControl()
        {
            InitializeComponent();
        }

        private async void LoadReport_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var VM = (ReportsViewModel)this.DataContext;
            await VM.LoadItemReport();
        }
    }
}
