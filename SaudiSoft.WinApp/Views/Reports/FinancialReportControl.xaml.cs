﻿using System.Windows.Controls;

namespace SaudiSoft.WinApp.Views
{
    /// <summary>
    /// Interaction logic for FinancialReportControl.xaml
    /// </summary>
    public partial class FinancialReportControl : UserControl
    {
        public FinancialReportControl()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var VM = (ReportsViewModel)this.DataContext;
            VM.UpdateDateRange();
        }
    }
}
