﻿using SaudiSoft.Core;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace SaudiSoft.WinApp.Views
{
    /// <summary>
    /// Interaction logic for SalesListControl.xaml
    /// </summary>
    public partial class SalesListControl : UserControl
    {
        public SalesListControl()
        {
            InitializeComponent();
        }

        private void ComboBox_DropDownClosed(object sender, System.EventArgs e)
        {
            var viewModel = (SalesViewModel)this.DataContext;
            var comboBox = (ComboBox)sender;
            comboBox.ItemsSource = viewModel.StoreCustomers;
            if (comboBox.SelectedItem == null) comboBox.SelectedIndex = 0;
        }

        private void ComboBox_DropDownOpened(object sender, System.EventArgs e)
        {
            var comboBox = (ComboBox)sender;
            comboBox.SelectedItem = null;
        }

        private void ComboBox_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var comboBox = (ComboBox)sender;
            comboBox.IsDropDownOpen = true;
        }

        private void ComboBox2_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var viewModel = (SalesViewModel)this.DataContext;
            var comboBox = (ComboBox)sender;
            comboBox.IsDropDownOpen = true;
            if (!string.IsNullOrEmpty(comboBox.Text))
            {
                var filteredStoreCustomers = new ObservableCollection<StoreCustomer>();
                foreach (var customer in viewModel.FilterStoreCustomers)
                {
                    var searchTerm = comboBox.Text.ToLower();
                    if (customer.ID == 0) continue;
                    else if (customer.Name != null && customer.Name.ToLower().Contains(searchTerm)) filteredStoreCustomers.Add(customer);
                    else if (customer.Email != null && customer.Email.ToLower().Contains(searchTerm)) filteredStoreCustomers.Add(customer);
                    else if (customer.Phone != null && customer.Phone.ToLower().Contains(searchTerm)) filteredStoreCustomers.Add(customer);
                    else if (customer.Mobile != null && customer.Mobile.ToLower().Contains(searchTerm)) filteredStoreCustomers.Add(customer);
                }
                comboBox.ItemsSource = filteredStoreCustomers;
            }
            else
            {
                comboBox.ItemsSource = viewModel.FilterStoreCustomers;
            }
        }

        private void ComboBox2_DropDownClosed(object sender, System.EventArgs e)
        {
            var viewModel = (SalesViewModel)this.DataContext;
            var comboBox = (ComboBox)sender;
            comboBox.ItemsSource = viewModel.FilterStoreCustomers;
            if (comboBox.SelectedItem == null) comboBox.SelectedIndex = 0;
        }
    }
}
