﻿using System.Windows.Controls;

namespace SaudiSoft.WinApp.Views
{
    /// <summary>
    /// Interaction logic for SalesFormControl.xaml
    /// </summary>
    public partial class SalesFormControl : UserControl
    {
        public SalesFormControl()
        {
            InitializeComponent();
        }

        private void NumericUpDown_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double?> e)
        {
            var VM = (SalesViewModel)this.DataContext;
            VM.UpdateTicket();
        }
    }
}
