﻿using System.Windows.Controls;

namespace SaudiSoft.WinApp.Views
{
    /// <summary>
    /// Interaction logic for PurchasesFormControl.xaml
    /// </summary>
    public partial class PurchasesFormControl : UserControl
    {
        public PurchasesFormControl()
        {
            InitializeComponent();
        }

        private void NumericUpDown_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double?> e)
        {
            var VM = (PurchaseViewModel)this.DataContext;
            VM.UpdateTicket();
        }
    }
}
