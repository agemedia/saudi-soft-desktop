﻿using SaudiSoft.Core;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace SaudiSoft.WinApp.Views
{
    /// <summary>
    /// Interaction logic for PurchasesListControl.xaml
    /// </summary>
    public partial class PurchasesListControl : UserControl
    {
        public PurchasesListControl()
        {
            InitializeComponent();
        }

        private void ComboBox_DropDownOpened(object sender, System.EventArgs e)
        {
            var comboBox = (ComboBox)sender;
            comboBox.SelectedItem = null;
        }

        private void ComboBox_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var comboBox = (ComboBox)sender;
            comboBox.IsDropDownOpen = true;
        }

        private void ComboBox2_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var viewModel = (PurchaseViewModel)this.DataContext;
            var comboBox = (ComboBox)sender;
            comboBox.IsDropDownOpen = true;
            if (!string.IsNullOrEmpty(comboBox.Text))
            {
                var filteredStoreSuppliers = new ObservableCollection<StockSupplier>();
                foreach (var supplier in viewModel.FilterStockSuppliers)
                {
                    var searchTerm = comboBox.Text.ToLower();
                    if (supplier.ID == 0) continue;
                    else if (supplier.Name != null && supplier.Name.ToLower().Contains(searchTerm)) filteredStoreSuppliers.Add(supplier);
                    else if (supplier.Email != null && supplier.Email.ToLower().Contains(searchTerm)) filteredStoreSuppliers.Add(supplier);
                    else if (supplier.Phone != null && supplier.Phone.ToLower().Contains(searchTerm)) filteredStoreSuppliers.Add(supplier);
                    else if (supplier.Mobile != null && supplier.Mobile.ToLower().Contains(searchTerm)) filteredStoreSuppliers.Add(supplier);
                }
                comboBox.ItemsSource = filteredStoreSuppliers;
            }
            else
            {
                comboBox.ItemsSource = viewModel.FilterStockSuppliers;
            }
        }

        private void ComboBox2_DropDownClosed(object sender, System.EventArgs e)
        {
            var viewModel = (PurchaseViewModel)this.DataContext;
            var comboBox = (ComboBox)sender;
            comboBox.ItemsSource = viewModel.FilterStockSuppliers;
            if (comboBox.SelectedItem == null) comboBox.SelectedIndex = 0;
        }
    }
}
