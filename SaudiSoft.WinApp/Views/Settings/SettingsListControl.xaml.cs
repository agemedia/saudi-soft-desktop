﻿using System.Windows.Controls;

namespace SaudiSoft.WinApp.Views
{
    /// <summary>
    /// Interaction logic for SettingsListControl.xaml
    /// </summary>
    public partial class SettingsListControl : UserControl
    {
        public SettingsListControl()
        {
            InitializeComponent();
        }

        private void Language_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var VM = (SettingsViewModel)this.DataContext;
            VM.SetLanguage();
        }

        private void Currency_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var VM = (SettingsViewModel)this.DataContext;
            VM.SetCurrency();
        }

        private void ItemDisplayOption_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var VM = (SettingsViewModel)this.DataContext;
            VM.SetItemDisplayOption();
        }
    }
}
